#ifdef _WIN32
#define snprintf  _snprintf
#endif

#include "mtf/mtf.h"
#include "mtf/Test/Diagnostics.h"

#include "mtf/Config/parameters.h"
#include "mtf/Config//datasets.h"

#include "mtf/Tools/cvUtils.h"
#include "mtf/Tools/inputCV.h"
#ifndef DISABLE_XVISION
#include "mtf/Tools/inputXV.h"
#endif
// tools for preprocessing the image
#include "mtf/Tools/PreProc.h"

#include <time.h>
#include <string.h>
#include <numeric>
#include <vector>
#include <iomanip>

#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/path.hpp"

#ifndef DISABLE_XVISION
#define XVISION_PIPELINE 'x'
#endif
#define OPENCV_PIPELINE 'c'

using namespace std;
namespace fs = boost::filesystem;
using namespace mtf::params;

mtf::Diagnostics *diag;
VectorXd param_range;
VectorXd x_vec, y_vec;

typedef mtf::Diagnostics::AnalyticalDataType ADT;
typedef mtf::Diagnostics::NumericalDataType NDT;

const char* getDataTypeName(int data_id, int adt_len, int diag_len){
	if(data_id < adt_len){// analytical
		return diag->toString(static_cast<ADT>(data_id));
	} else if(data_id < diag_len - 1){// numerical
		return diag->toString(static_cast<NDT>(data_id - adt_len));
	} else if(data_id == diag_len - 1){// ssm
		return "ssm_param";
	} else{
		throw std::invalid_argument("getDataTypeName:: Invalid datay type provided");
	}

}

void generateData(int data_id, int adt_len, int diag_len){
	char *out_fname = nullptr;
	if(data_id < adt_len){// analytical
		if(diag_verbose){
			printf("Generating %s data\t",
				diag->toString(static_cast<ADT>(data_id)));
		}
		if(diag_3d){
			Vector2d state_ids(diag_3d_ids[0], diag_3d_ids[1]);
			diag->generateAnalyticalData3D(x_vec, y_vec, param_range, diag_res, state_ids,
				static_cast<ADT>(data_id), out_fname);
		} else{
			diag->generateAnalyticalData(param_range, diag_res, static_cast<ADT>(data_id), out_fname);
		}
	} else if(data_id < diag_len - 1){// numerical
		int num_data_id = data_id - adt_len;
		//printf("data_id: %d\n", data_id);
		//printf("adt_len: %d\n", adt_len);
		//printf("num_data_id: %d\n", num_data_id);
		if(diag_verbose){
			printf("Generating numerical %s data\t",
				diag->toString(static_cast<NDT>(num_data_id)));
		}
		diag->generateNumericalData(param_range, diag_res,
			static_cast<NDT>(num_data_id), out_fname, diag_grad_diff);
	} else if(data_id == diag_len - 1){// ssm
		if(diag_verbose){ printf("Generating SSMParam data\t"); }
		diag->generateSSMParamData(param_range, diag_res, out_fname);
	} else{
		printf("Data type: %d\n", data_id);
		throw std::invalid_argument("generateData:: Invalid data type provided");
	}
}

void generateInverseData(int data_id, int adt_len, int diag_len){
	char *out_fname = nullptr;
	if(data_id < adt_len){// analytical
		if(diag_verbose){ printf("Generating inverse %s data\t", diag->toString(static_cast<ADT>(data_id))); }
		diag->generateInverseAnalyticalData(param_range, diag_res, static_cast<ADT>(data_id), out_fname);
	} else if(data_id < diag_len - 1){// numerical
		if(diag_verbose){
			printf("Generating numerical inverse %s data\t",
				diag->toString(static_cast<NDT>(data_id - adt_len)));
		}
		diag->generateInverseNumericalData(param_range, diag_res, static_cast<NDT>(data_id - adt_len), out_fname, diag_grad_diff);
	} else if(data_id == diag_len - 1){// ssm
		if(diag_verbose){ printf("Generating SSMParam data\t"); }
		diag->generateSSMParamData(param_range, diag_res, out_fname);
	} else{
		printf("Data type: %d\n", data_id);
		throw std::invalid_argument("generateInverseData:: Invalid data type provided");
	}
}

int main(int argc, char * argv[]) {

	if(!readParams(argc, argv)){ return EXIT_FAILURE; }

	cout << "*******************************\n";
	cout << "Using parameters:\n";
	cout << "source_id: " << source_id << "\n";
	cout << "source_name: " << source_name << "\n";
	cout << "actor: " << actor << "\n";
	cout << "steps_per_frame: " << steps_per_frame << "\n";
	cout << "pipeline: " << pipeline << "\n";
	cout << "img_source: " << img_source << "\n";
	cout << "show_cv_window: " << show_cv_window << "\n";
	cout << "read_obj_from_file: " << read_obj_from_file << "\n";
	cout << "record_frames: " << record_frames << "\n";
	cout << "patch_size: " << patch_size << "\n";
	cout << "read_obj_fname: " << read_obj_fname << "\n";
	cout << "read_obj_from_gt: " << read_obj_from_gt << "\n";
	cout << "show_warped_img: " << show_warped_img << "\n";
	cout << "pause_after_frame: " << pause_after_frame << "\n";
	cout << "write_tracker_states: " << write_tracker_states << "\n";
	cout << "*******************************\n";

	InputBase *input = nullptr;

	cv::Point fps_origin(10, 20);
	double fps_font_size = 0.50;
	cv::Scalar fps_color(0, 255, 0);
	char fps_text[100];

	cv::Point err_origin(10, 40);
	double err_font_size = 0.50;
	cv::Scalar err_color(0, 255, 0);
	char err_text[100];

	/* initialize pipeline*/
	if(pipeline == OPENCV_PIPELINE) {
		input = new InputCV(img_source, source_name, source_fmt, source_path, buffer_count);
	}
#ifndef DISABLE_XVISION
	else if(pipeline == XVISION_PIPELINE) {
		input = new InputXV(img_source, source_name, source_fmt, source_path, buffer_count);
	}
#endif
	else {
		printf("Invalid video pipeline provided\n");
		return EXIT_FAILURE;
	}
	if(!input->initialize()){
		printf("Pipeline could not be initialized successfully\n");
		return EXIT_FAILURE;
	}
	/* initialize frame pre processor*/
	PreProc *pre_proc = getPreProcObj(CV_32FC1);
	pre_proc->initialize(input->getFrame());


	//printf("done getting no. of frames\n");
	printf("n_frames=%d\n", input->n_frames);

	for(int i = 0; i < init_frame_id; i++){
		if(!input->update()){
			printf("Frame %d could not be read from the input pipeline", input->getFrameID() + 1);
			return EXIT_FAILURE;
		}
	}
	pre_proc->update(input->getFrame(), input->getFrameID());

	bool init_obj_read = false;
	/*get objects to be tracked*/
	CVUtils *util_obj = new CVUtils;
	vector<obj_struct*> init_objects;
	if(read_obj_from_gt){
		obj_struct* init_object = util_obj->readObjectFromGT(source_name, source_path, input->n_frames, init_frame_id, debug_mode);
		if(init_object){
			init_obj_read = true;
			for(int i = 0; i < n_trackers; i++) {
				init_objects.push_back(init_object);
			}
		} else{
			printf("Failed to read initial object location from ground truth; using manual selection...\n");
		}

	}
	if(!init_obj_read && read_obj_from_file) {
		init_objects = util_obj->readObjectsFromFile(n_trackers, read_obj_fname.c_str(), debug_mode);
		if(init_objects[0]){
			init_obj_read = true;
		} else{
			printf("Failed to read initial object location from file; using manual selection...\n");
		}
	}
	if(!init_obj_read){
		//printf("calling getMultipleObjects\n");
		init_objects = util_obj->getMultipleObjects(input->getFrame(), n_trackers,
			patch_size, line_thickness, write_objs, write_obj_fname.c_str());
		//printf("done calling getMultipleObjects\n");
	}


	/*********************************** initialize trackers ***********************************/

	printf("Using diagnostics with object of size %f x %f\n",
		init_objects[0]->size_x, init_objects[0]->size_y);
	if(res_from_size){
		resx = init_objects[0]->size_x;
		resy = init_objects[0]->size_y;
	}
	mtf::AppearanceModel *am = mtf::getAM(diag_am);
	mtf::StateSpaceModel *ssm = mtf::getSSM(diag_ssm);
	if(!am || !ssm){
		printf("Diagnostics could not be initialized successfully\n");
		return EXIT_FAILURE;
	}
	mtf::DiagnosticsParams diag_params(
		static_cast<mtf::DiagnosticsParams::UpdateType>(diag_update),
		diag_show_corners, diag_show_patches,
		diag_enable_validation, diag_validation_prec);
	diag = new  mtf::Diagnostics(am, ssm, &diag_params);

	//char feat_norm_fname[100], norm_fname[100], jac_fname[100], hess_fname[100], hess2_fname[100];
	//char jac_num_fname[100], hess_num_fname[100], nhess_num_fname[100];
	//char ssm_fname[100];
	std::string diag_data_dir = cv::format("log/diagnostics/%s", source_name.c_str());
	if(diag_3d){
		diag_data_dir = diag_data_dir + "/3D";
	}
	if(!fs::exists(diag_data_dir)){
		printf("Diagnostic data directory: %s does not exist. Creating it...\n", diag_data_dir.c_str());
		fs::create_directories(diag_data_dir);
	}
	diag->setImage(pre_proc->getFrame());
	diag->initialize(init_objects[0]->corners);

	param_range.resize(diag->ssm_state_size);
	if(diag_range){
		param_range.fill(diag_range);
	} else{
		if(diag_ssm_range.size() < diag->ssm_state_size){
			throw std::invalid_argument("testMTF:: Insufficient number of SSM range parameters provided");
		}

		for(int state_id = 0; state_id < diag->ssm_state_size; ++state_id){
			param_range[state_id] = diag_ssm_range[state_id];
		}
	}
	if(res_from_size){
		printf("Getting sampling resolution from object size...\n");
	}

	std::string diag_gen = diag_gen_norm + diag_gen_jac + diag_gen_hess +
		diag_gen_hess2 + diag_gen_hess_sum + diag_gen_num + diag_gen_ssm;

	int diag_len = diag_gen.length();
	int adt_len = diag_gen_norm.length() + diag_gen_jac.length() +
		diag_gen_hess.length() + diag_gen_hess2.length() + diag_gen_hess_sum.length();

	printf("diag_gen_norm: %s\n", diag_gen_norm.c_str());
	printf("diag_gen_jac: %s\n", diag_gen_jac.c_str());
	printf("diag_gen_hess: %s\n", diag_gen_hess.c_str());
	printf("diag_gen_hess2: %s\n", diag_gen_hess2.c_str());
	printf("diag_gen_hess_sum: %s\n", diag_gen_hess_sum.c_str());
	printf("diag_gen_num: %s\n", diag_gen_num.c_str());
	printf("diag_gen_ssm: %s\n", diag_gen_ssm.c_str());
	printf("diag_gen: %s\n", diag_gen.c_str());
	printf("diag_len: %d\n", diag_len);
	printf("adt_len: %d\n", adt_len);
	printf("diag_frame_gap: %d\n", diag_frame_gap);

	int start_id = init_frame_id >= diag_frame_gap ? init_frame_id : diag_frame_gap;
	int end_id = end_frame_id >= init_frame_id ? end_frame_id : input->n_frames - 1;
	if(end_id > start_id){
		printf("Generating diagnostics data for frames %d to %d\n", start_id, end_id);
	} else {
		end_id = start_id;
		printf("Generating diagnostics data for frame %d\n", start_id);
	}

	std::string bin_out_fname;
	ofstream out_files[diag_len];
	if(diag_bin){
		for(int data_id = 0; data_id < diag_len; data_id++){
			if(diag_gen[data_id] - '0'){
				const char* data_name = getDataTypeName(data_id, adt_len, diag_len);
				if(diag_inv){
					bin_out_fname = cv::format("%s/%s_%s_%d_inv_%s_%d_%d_%d.bin",
						diag_data_dir.c_str(), diag_am, diag_ssm, diag_update, data_name,
						diag_frame_gap, start_id, end_id);					
				} else{
					bin_out_fname = cv::format("%s/%s_%s_%d_%s_%d_%d_%d.bin",
						diag_data_dir.c_str(), diag_am, diag_ssm, diag_update, data_name,
						diag_frame_gap, start_id, end_id);					
				}

				if(diag_inv){
					printf("Writing inv_%s data to %s\n", data_name, bin_out_fname.c_str());
				} else{
					printf("Writing %s data to %s\n", data_name, bin_out_fname.c_str());
				}
				out_files[data_id].open(bin_out_fname, ios::out | ios::binary);
				if(diag_3d){
					Vector4i header(start_id - 1, start_id, diag_res, diag_res);
					out_files[data_id].write((char*)(header.data()), sizeof(int) * 4);
				} else{
					out_files[data_id].write((char*)(&diag_res), sizeof(int));
					out_files[data_id].write((char*)(&(diag->ssm_state_size)), sizeof(int));
				}
			}
		}
	}

	for(int i = init_frame_id; i < start_id; i++){
		if(!input->update()){
			printf("Frame %d could not be read from the input pipeline", input->getFrameID() + 1);
			return EXIT_FAILURE;
		}
	}
	pre_proc->update(input->getFrame(), input->getFrameID());

	printf("frame_id: %d\n", input->getFrameID());
	vector<double> proc_times;
	for(int frame_id = start_id; frame_id <= end_id; frame_id++){
		printf("Processing frame: %d\n", frame_id);
		//update diagnostics module
		diag->update(util_obj->ground_truth[frame_id - diag_frame_gap]);

		mtf_clock_get(frame_start_time);
		for(int data_id = 0; data_id < diag_len; data_id++){
			if(diag_gen[data_id] - '0'){
				clock_t start_time = clock();
				if(diag_inv){
					generateInverseData(data_id, adt_len, diag_len);
				} else{
					generateData(data_id, adt_len, diag_len);
				}
				clock_t end_time = clock();
				double time_taken = static_cast<double>(end_time - start_time) / CLOCKS_PER_SEC;
				if(diag_verbose){
					printf("Time taken:\t %f\n", time_taken);
				}
				if(diag_3d){
					if(frame_id==start_id){
						out_files[data_id].write((char*)(y_vec.data()), sizeof(double)*y_vec.size());
						out_files[data_id].write((char*)(x_vec.data()), sizeof(double)*x_vec.size());
					}
					out_files[data_id].write((char*)(diag->getData().data()),
						sizeof(double)*diag->getData().size());

					long curr_pos = out_files[data_id].tellp();
					out_files[data_id].seekp(0, ios_base::beg);
					out_files[data_id].write((char*)(&frame_id), sizeof(int));
					out_files[data_id].seekp(curr_pos, ios_base::beg);
				}else{
					out_files[data_id].write((char*)(diag->getData().data()),
						sizeof(double)*diag->getData().size());
				}

			}
		}
		mtf_clock_get(frame_end_time);
		double time_taken;
		mtf_clock_measure(frame_start_time, frame_end_time, time_taken);
		proc_times.push_back(time_taken);
		if(diag_verbose){
			printf("Done frame %d. Time taken:\t %f\n", frame_id, time_taken);
		}
		if(diag_verbose){
			printf("***********************************\n");
		}

		if(input->getFrameID() == end_id){break;}

		// update frame
		if(!input->update()){
			printf("Frame %d could not be read from the input pipeline", input->getFrameID() + 1);
			break;
		}
		pre_proc->update(input->getFrame(), input->getFrameID());
	}
	printf("Closing files...\n");
	if(diag_bin){
		for(int data_id = 0; data_id < diag_len; data_id++){
			if(diag_gen[data_id] - '0'){
				out_files[data_id].write((char*)(proc_times.data()), sizeof(double)*proc_times.size());
				out_files[data_id].close();
			}
		}
	}
	printf("Done closing files\n");
	return EXIT_SUCCESS;
}
