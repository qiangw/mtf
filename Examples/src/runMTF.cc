// main header that provides functions for creating trackers
#include "mtf/mtf.h"

// tools for reading in images from various sources like image sequences, 
// videos and cameras as well as for pre processing them
#include "mtf/Tools/pipeline.h"
// general OpenCV tools for selecting objects, reading ground truth, etc.
#include "mtf/Tools/cvUtils.h"
// parameters for different modules
#include "mtf/Config/parameters.h"
// general utilities for image drawing, etc.
#include "mtf/Utilities/miscUtils.h"

#include <string.h>
#include <numeric>
#include <vector>
#include <iomanip>

#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"

#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/path.hpp"

#define MAX_FPS 1e6

using namespace std;
using namespace mtf::params;
namespace fs = boost::filesystem;

int main(int argc, char * argv[]) {
    printf("Starting MTF...\n");

    // *************************************************************************************************** //
    // ********************************** read configuration parameters ********************************** //
    // *************************************************************************************************** //

	if(!readParams(argc, argv)){return EXIT_FAILURE;}

#ifdef ENABLE_PARALLEL
    Eigen::initParallel();
#endif
    printf("*******************************\n");
    printf("Using parameters:\n");
    printf("n_trackers: %d\n", n_trackers);
    printf("actor_id: %d\n", actor_id);
    printf("source_id: %d\n", source_id);
    printf("source_name: %s\n", source_name.c_str());
    printf("actor: %s\n", actor.c_str());
    printf("pipeline: %c\n", pipeline);
    printf("img_source: %c\n", img_source);
    printf("show_cv_window: %d\n", show_cv_window);
    printf("read_obj_from_gt: %d\n", read_obj_from_gt);
    printf("write_tracking_data: %d\n", write_tracking_data);
    printf("mtf_sm: %s\n", mtf_sm);
    printf("mtf_am: %s\n", mtf_am);
    printf("mtf_ssm: %s\n", mtf_ssm);
    printf("*******************************\n");

    // *********************************************************************************************** //
    // ********************************** initialize input pipeline ********************************** //
    // *********************************************************************************************** //

	InputBase *input = getInputObj();
    if(!input->initialize()){
        printf("Pipeline could not be initialized successfully. Exiting...\n");
        return EXIT_FAILURE;
    }
    //printf("done getting no. of frames\n");
    printf("n_frames=%d\n", input->n_frames);

    if(init_frame_id > 0){
        if(input->n_frames > 0 && init_frame_id >= input->n_frames){
            printf("init_frame_id: %d is larger than the maximum frame ID in the sequence: %d\n",
                init_frame_id, input->n_frames - 1);
            return EXIT_FAILURE;
        }
        printf("Skipping to frame %d before initializing trackers...\n", init_frame_id + 1);
        for(int frame_id = 0; frame_id < init_frame_id; frame_id++){
            if(!input->update()){
                printf("Frame %d could not be read from the input pipeline\n", input->getFrameID() + 1);
                return EXIT_FAILURE;
            }
        }
    }

    // **************************************************************************************************** //
    // ******************* get objects to be tracked and read ground truth if available ******************* //
    // **************************************************************************************************** //

    bool init_obj_read = false;
    CVUtils cv_utils;
	if(reinit_from_gt){
		if(!cv_utils.readReinitGT(source_name, source_path, input->n_frames,
			debug_mode, use_opt_gt, reinit_gt_from_bin, opt_gt_ssm)){
			printf("Reinitialization ground truth could not be read.\n");
			return EXIT_FAILURE;
		}
		printf("Tracker reinitialization on failure is enabled with error threshold: %f and skipped frames: %d\n",
			reinit_err_thresh, reinit_frame_skip);
	}
    vector<obj_struct*> init_objects;
    if(read_obj_from_gt){
        obj_struct* init_object = cv_utils.readObjectFromGT(source_name, source_path, input->n_frames,
            init_frame_id, debug_mode, use_opt_gt, opt_gt_ssm);
        if(init_object){
            init_obj_read = true;
            for(int tracker_id = 0; tracker_id < n_trackers; tracker_id++) {
                init_objects.push_back(init_object);
            }
        } else{
            printf("Failed to read initial object from ground truth; using manual selection...\n");
        }
    }
    if(!init_obj_read && read_obj_from_file) {
        init_objects = cv_utils.readObjectsFromFile(n_trackers, read_obj_fname.c_str(), debug_mode);
        if(init_objects[0]){
            init_obj_read = true;
        } else{
            printf("Failed to read initial object location from file; using manual selection...\n");
        }
    }
    if(!init_obj_read){
        if(img_source == SRC_IMG || img_source == SRC_DISK || img_source == SRC_VID){
            //printf("calling getMultipleObjects with sel_quad_obj=%d\n", sel_quad_obj);
            init_objects = cv_utils.getMultipleObjects(input->getFrame(), n_trackers,
                patch_size, line_thickness, write_objs, sel_quad_obj, write_obj_fname.c_str());
            //printf("done calling getMultipleObjects\n");
        } else{
            //printf("calling getMultipleObjects with sel_quad_obj=%d\n", sel_quad_obj);
            init_objects = cv_utils.getMultipleObjects(input, n_trackers,
                patch_size, line_thickness, write_objs, sel_quad_obj, write_obj_fname.c_str());
            //printf("done calling getMultipleObjects\n");
        }
    }

    // ************************************************************************************************** //
    // ************************** initialize trackers and image pre processors ************************** //
    // ************************************************************************************************** //

    if(n_trackers > 1){
        printf("Multi tracker setup enabled\n");
        write_tracking_data = 0;
        reinit_from_gt = 0;
        show_ground_truth = 0;
    }
    if(res_from_size){
        printf("Getting sampling resolution from object size...\n");
    }

    FILE *multi_fid = nullptr;
	vector<mtf::TrackerBase*> trackers;
	vector<PreProc*> pre_proc;
	for(int tracker_id = 0; tracker_id < n_trackers; tracker_id++) {
		if(n_trackers > 1){ multi_fid = readTrackerParams(multi_fid); }
		printf("Using tracker %d with object of size %f x %f\n", tracker_id,
			init_objects[tracker_id]->size_x, init_objects[tracker_id]->size_y);
		if(res_from_size){
			resx = init_objects[tracker_id]->size_x;
			resy = init_objects[tracker_id]->size_y;
		}
		mtf::TrackerBase *new_tracker = mtf::getTrackerObj(mtf_sm, mtf_am, mtf_ssm);
		if(!new_tracker){
            printf("Tracker could not be created successfully\n");
            return EXIT_FAILURE;
        }
		PreProc* new_pre_proc = getPreProcObj(pre_proc, new_tracker->inputType());		
		new_pre_proc->initialize(input->getFrame(), input->getFrameID());
		for(PreProc* curr_obj = new_pre_proc; curr_obj; curr_obj = curr_obj->next){
			new_tracker->setImage(curr_obj->getFrame());
		}
        new_tracker->initialize(init_objects[tracker_id]->corners);
        trackers.push_back(new_tracker);
		pre_proc.push_back(new_pre_proc);
    }
    if(input->n_frames == 0){
        start_frame_id = init_frame_id;
    } else if(start_frame_id >= cv_utils.ground_truth.size()){
        printf("Ground truth is only available for %d frames so starting tracking there instead",
            cv_utils.ground_truth.size());
        start_frame_id = cv_utils.ground_truth.size() - 1;
    }
    if(start_frame_id > init_frame_id){
        if(input->n_frames > 0 && start_frame_id >= input->n_frames){
            printf("start_frame_id: %d is larger than the maximum frame ID in the sequence: %d\n",
                start_frame_id, input->n_frames - 1);
            return EXIT_FAILURE;
        }

        printf("Skipping to frame %d before starting tracking...\n", start_frame_id + 1);
        while(input->getFrameID() < start_frame_id){
            if(!input->update()){
                printf("Frame %d could not be read from the input pipeline\n", input->getFrameID() + 1);
                return EXIT_FAILURE;
            }
        }
        
        for(int tracker_id = 0; tracker_id < n_trackers; ++tracker_id) {
			pre_proc[tracker_id]->update(input->getFrame(), input->getFrameID());
            trackers[tracker_id]->setRegion(cv_utils.ground_truth[input->getFrameID()]);
        }
    }


    // ******************************************************************************************** //
    // *************************************** setup output *************************************** //
    // ******************************************************************************************** //

    string cv_win_name = "MTF :: " + source_name;
    string proc_win_name = "Processed Image :: " + source_name;
    if(show_cv_window) {
        cv::namedWindow(cv_win_name, cv::WINDOW_AUTOSIZE);
        if(show_proc_img) {
            cv::namedWindow(proc_win_name, cv::WINDOW_AUTOSIZE);
        }
    }
    // if reading object from ground truth did not succeed then the 
    // ground truth is invalid and computing tracking error is not possible 
    // nor is it possible to reinitialize the object from ground truth
    show_tracking_error = show_tracking_error && read_obj_from_gt;
    reinit_from_gt = reinit_from_gt && read_obj_from_gt;
    show_ground_truth = show_ground_truth && read_obj_from_gt;

    if(show_tracking_error || reinit_from_gt){
        int valid_gt_frames = cv_utils.ground_truth.size();
        if(input->n_frames <= 0 || valid_gt_frames < input->n_frames){
            if(input->n_frames <= 0){
                printf("Disabling tracking error computation\n");
            } else if(valid_gt_frames > 0){
                printf("Disabling tracking error computation since ground truth is only available for %d out of %d frames\n",
                    valid_gt_frames, input->n_frames);
            } else{
                printf("Disabling tracking error computation since ground truth is not available\n");
            }
            show_tracking_error = 0;
            reinit_from_gt = 0;
            show_ground_truth = 0;
        }
    }
    cv::VideoWriter output;
    if(record_frames){
        if(record_frames_fname.empty()){
            record_frames_fname = cv::format("log/%s_%s_%s_%d.avi", mtf_sm, mtf_am, mtf_ssm, 1 - hom_normalized_init);
        }
        output.open(record_frames_fname, CV_FOURCC('M', 'J', 'P', 'G'), 24, input->getFrame().size());
    }
    FILE *tracking_data_fid = nullptr;
    if(write_tracking_data){
        string tracking_data_dir, tracking_data_path;
        if(reinit_from_gt){
            tracking_data_dir = cv::format("log/tracking_data/reinit_%d_%d/%s/%s",
                static_cast<int>(reinit_err_thresh), reinit_frame_skip, actor.c_str(), source_name.c_str());
        } else{
            tracking_data_dir=cv::format("log/tracking_data/%s/%s", actor.c_str(), source_name.c_str());
        }

        if(!fs::exists(tracking_data_dir)){
            printf("Tracking data directory: %s does not exist. Creating it...\n", tracking_data_dir.c_str());
            fs::create_directories(tracking_data_dir);
        }
        if(tracking_data_fname.empty()){
            tracking_data_fname = cv::format("%s_%s_%s_%d", mtf_sm, mtf_am, mtf_ssm, 1 - hom_normalized_init);
        }
        tracking_data_path = cv::format("%s/%s.txt", tracking_data_dir.c_str(), tracking_data_fname.c_str());
        printf("Writing tracking data to: %s\n", tracking_data_path.c_str());
        tracking_data_fid = fopen(tracking_data_path.c_str(), "w");
        fprintf(tracking_data_fid, "frame ulx uly urx ury lrx lry llx lly\n");
    }

    double fps = 0, fps_win = 0;
	double tracking_time, tracking_time_with_input;
    double avg_fps = 0, avg_fps_win = 0;
    int fps_count = 0;
    double avg_err = 0;

    cv::Point fps_origin(10, 20);
    double fps_font_size = 0.50;
    cv::Scalar fps_color(0, 255, 0);
    cv::Point err_origin(10, 40);
    double err_font_size = 0.50;
    cv::Scalar err_color(0, 255, 0);
    cv::Scalar gt_color(0, 255, 0);

	if(reset_template){ printf("Template resetting is enabled\n"); }

	double tracking_err = 0;
	int failure_count = 0;
	bool is_initialized = true;
    int valid_frame_count = 0;
    int reinit_frame_id = 0;
	//! store the IDs of the frames where tracking failure is detected if reinit_from_gt is enabled;
	std::vector<int> failure_frame_ids;

	printf("Using %s error to measure tracking accuracy\n", toString(tracking_err_type));

    // ********************************************************************************************** //
    // *************************************** start tracking ! ************************************* //
    // ********************************************************************************************** //
    while(true) {
        // *********************** compute tracking error and reinitialize if needed *********************** //
		cv::Mat gt_corners;
		if(show_tracking_error || reinit_from_gt || show_ground_truth){
			if(reinit_from_gt){
				gt_corners = cv_utils.reinit_ground_truth[reinit_frame_id][input->getFrameID() - reinit_frame_id];
			} else{
				gt_corners = cv_utils.ground_truth[input->getFrameID()];
			}
		}
		cv::Mat tracker_corners = trackers[0]->getRegion();
		if(print_corners){
			cout << tracker_corners << "\n";
		}
        if(show_tracking_error || reinit_from_gt){
			// check the type of error needed
			switch(tracking_err_type){
			case TrackingErrorType::MCD:
				tracking_err = mtf::utils::getMeanCornerDistanceError(gt_corners, tracker_corners);
				break;
			case TrackingErrorType::CLE:
				tracking_err = mtf::utils::getCenterLocationError(gt_corners, tracker_corners);
				break;
			case TrackingErrorType::Jaccard:
				tracking_err = mtf::utils::getJaccardError(gt_corners, tracker_corners,
					input->getFrame().cols, input->getFrame().rows);
				break;
			default:
				throw invalid_argument(
					cv::format("Invalid tracking error type provided: %d\n", 
					tracking_err_type));
			}          
            //printf("tracking_err: %f\n", tracking_err);
            //printf("done computing error\n");
			if(reinit_from_gt && (std::isnan(tracking_err) || tracking_err > reinit_err_thresh)){
                ++failure_count;
				failure_frame_ids.push_back(input->getFrameID() + 1);
                printf("Tracking failure %4d detected in frame %5d with error: %10.6f. ",
                    failure_count, input->getFrameID() + 1, tracking_err);
                if(write_tracking_data){
                    fprintf(tracking_data_fid, "frame%05d.jpg tracker_failed\n", input->getFrameID() + 1);
                }
                if(input->getFrameID() + reinit_frame_skip >= input->n_frames){
                    printf("Reinitialization is not possible as insufficient frames (%d) are left to skip. Exiting...\n",
                        input->n_frames - input->getFrameID() - 1);
                    break;
                }
                bool skip_success = true;
                for(int skip_id = 0; skip_id < reinit_frame_skip; ++skip_id) {
                    if(!input->update()){
                        printf("Frame %d could not be read from the input pipeline\n", input->getFrameID() + 1);
                        skip_success = false;
                        break;
                    }
                }
                if(!skip_success){ break; }
                printf("Reinitializing in frame %5d...\n", input->getFrameID() + 1);
                //cout << "Using ground truth:\n" << cv_utils.ground_truth[input->getFrameID()] << "\n";
				pre_proc[0]->update(input->getFrame(), input->getFrameID());			
                reinit_frame_id = input->getFrameID();
				if(reinit_with_new_obj){
					// delete the old tracker instance and create a new one before reinitializing
					if(trackers[0]){ delete(trackers[0]); }
					trackers[0] = mtf::getTrackerObj(mtf_sm, mtf_am, mtf_ssm);
					if(!trackers[0]){
						printf("Tracker could not be created successfully\n");
						return EXIT_FAILURE;
					}
					for(PreProc* curr_obj = pre_proc[0]; curr_obj; curr_obj = curr_obj->next){
						trackers[0]->setImage(curr_obj->getFrame());
					}
				}
                trackers[0]->initialize(cv_utils.reinit_ground_truth[reinit_frame_id][0]);
                is_initialized = true;
            }
            if(is_initialized){
                is_initialized = false;
            } else{// exclude initialization frames for computing the average error                
                if(!std::isinf(fps)){
                    ++valid_frame_count;
                    avg_err += (tracking_err - avg_err) / valid_frame_count;
                    //fps_vector.push_back(fps);    
                }
            }
        }

        // *************************** display/save tracking output *************************** //

        if(record_frames || show_cv_window) {
            /* draw tracker positions to OpenCV window */
            for(int tracker_id = 0; tracker_id < n_trackers; ++tracker_id) {
                int col_id = tracker_id % cv_utils.no_of_cols;
				mtf::utils::drawCorners(input->getFrameMutable(), trackers[tracker_id]->getRegion(),
					cv_utils.obj_cols[col_id], line_thickness, trackers[tracker_id]->name.c_str(), 
					fps_font_size, show_corner_ids, 1 - show_corner_ids);
			}
            if(show_ground_truth){
				mtf::utils::drawCorners(input->getFrameMutable(), gt_corners,
					gt_color, line_thickness, "ground_truth", fps_font_size,
					show_corner_ids, 1 - show_corner_ids);
            }
            std::string fps_text=cv::format("frame: %d c: %9.3f a: %9.3f cw: %9.3f aw: %9.3f fps",
                input->getFrameID() + 1, fps, avg_fps, fps_win, avg_fps_win);
            putText(input->getFrameMutable(), fps_text, fps_origin, cv::FONT_HERSHEY_SIMPLEX, fps_font_size, fps_color);

            if(show_tracking_error){
				std::string err_text = cv::format("ce: %12.8f ae: %12.8f", tracking_err, avg_err);
				if(show_jaccard_error){
					double jaccard_error = tracking_err_type == TrackingErrorType::Jaccard ? tracking_err :
						mtf::utils::getJaccardError(gt_corners, trackers[0]->getRegion(),
						input->getFrame().cols, input->getFrame().rows);
					err_text = err_text + cv::format(" je: %12.8f", jaccard_error);
				}
				putText(input->getFrameMutable(), err_text, err_origin, cv::FONT_HERSHEY_SIMPLEX, err_font_size, err_color);
			}

            if(record_frames){
                output.write(input->getFrame());
            }
            if(show_cv_window){
                imshow(cv_win_name, input->getFrame());
                if(show_proc_img){
					pre_proc[0]->showFrame(proc_win_name);
                }
                int pressed_key = cv::waitKey(1 - pause_after_frame) % 256;
                if(pressed_key %256 == 27){
                    break;
                }
                if(pressed_key % 256 == 32){
                    pause_after_frame = 1 - pause_after_frame;
                }
            }
        }
        if(!show_cv_window && (input->getFrameID() + 1) % 50 == 0){
            printf("frame_id: %5d avg_fps: %15.9f avg_fps_win: %15.9f avg_err: %15.9f\n",
                input->getFrameID() + 1, avg_fps, avg_fps_win, avg_err);
        }
        if(write_tracking_data){
            fprintf(tracking_data_fid, "frame%05d.jpg %15.6f %15.6f %15.6f %15.6f %15.6f %15.6f %15.6f %15.6f\n", input->getFrameID() + 1,
                trackers[0]->getRegion().at<double>(0, 0), trackers[0]->getRegion().at<double>(1, 0),
                trackers[0]->getRegion().at<double>(0, 1), trackers[0]->getRegion().at<double>(1, 1),
                trackers[0]->getRegion().at<double>(0, 2), trackers[0]->getRegion().at<double>(1, 2),
                trackers[0]->getRegion().at<double>(0, 3), trackers[0]->getRegion().at<double>(1, 3));
        }

        if(input->n_frames > 0 && input->getFrameID() >= input->n_frames - 1){
            printf("==========End of input stream reached==========\n");
            break;
        }

        // ******************************* update pipeline and trackers ******************************* //

		mtf_clock_get(start_time_with_input);
        // update pipeline
        if(!input->update()){
            printf("Frame %d could not be read from the input pipeline\n", input->getFrameID() + 1);
            break;
        }
		
        //update trackers       
        for(int tracker_id = 0; tracker_id < n_trackers; ++tracker_id) {	
			pre_proc[tracker_id]->update(input->getFrame(), input->getFrameID());

			mtf_clock_get(start_time);
            trackers[tracker_id]->update();// this is equivalent to :             
            // trackers[tracker_id]->update(pre_proc->getFrame());        
            // as the image has been passed at the time of initialization 
            // and does not need to be passed again as long as the new 
            // image is read into the same locatioon
			mtf_clock_get(end_time);
			mtf_clock_measure(start_time, end_time, tracking_time);
			mtf_clock_measure(start_time_with_input, end_time,
				tracking_time_with_input);
			fps = 1.0 / tracking_time;
			fps_win = 1.0 / tracking_time_with_input;
            if(reset_template){
                trackers[tracker_id]->initialize(trackers[tracker_id]->getRegion());
            }
        }
        if(!std::isinf(fps) && fps < MAX_FPS){
            ++fps_count;
            avg_fps += (fps - avg_fps) / fps_count;
            // if fps is not inf then fps_win too must be non inf
            avg_fps_win += (fps_win - avg_fps_win) / fps_count;
        }
    }
    cv::destroyAllWindows();

    //double avg_fps = accumulate( fps_vector.begin(), fps_vector.end(), 0.0 )/ fps_vector.size();
    //double avg_fps_win = accumulate( fps_win_vector.begin(), fps_win_vector.end(), 0.0 )/ fps_win_vector.size();
    printf("Average FPS: %15.10f\n", avg_fps);
    printf("Average FPS with Input: %15.10f\n", avg_fps_win);
    if(show_tracking_error){
        printf("Average Tracking Error: %15.10f\n", avg_err);
        printf("Frames used for computing the average: %d\n", valid_frame_count);
    }
    if(reinit_from_gt){
        printf("Number of failures: %d\n", failure_count);
		if(failure_count>0){
			printf("Failures detected in frame(s): %d", failure_frame_ids[0]);
			for(int i = 1; i < failure_frame_ids.size(); ++i){
				printf(", %d", failure_frame_ids[i]);
			}
			printf("\n");
		}

    }
    if(write_tracking_data){
        fclose(tracking_data_fid);
        FILE *tracking_stats_fid = fopen("log/tracking_stats.txt", "a");
        fprintf(tracking_stats_fid, "%s\t %s\t %s\t %s\t %d\t %s\t %15.9f\t %15.9f",
            source_name.c_str(), mtf_sm, mtf_am, mtf_ssm, hom_normalized_init, tracking_data_fname.c_str(), avg_fps, avg_fps_win);
        if(show_tracking_error){
            fprintf(tracking_stats_fid, "\t %15.9f", avg_err);
        }
        if(reinit_from_gt){
            fprintf(tracking_stats_fid, "\t %d", failure_count);
        }
        fprintf(tracking_stats_fid, "\n");
        fclose(tracking_stats_fid);
    }
    if(record_frames){
        output.release();
    }
    //delete(input);
    //pre_proc.clear();
    //trackers.clear();
    return EXIT_SUCCESS;
}

