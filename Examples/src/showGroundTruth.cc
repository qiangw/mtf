#include "mtf/mtf.h"
// tools for reading in images from various sources like image sequences, 
// videos and cameras as well as for pre processing them
#include "mtf/Tools/pipeline.h"
// general OpenCV utilities for selecting objects, reading ground truth, etc.
#include "mtf/Tools/cvUtils.h"
// parameters for different modules
#include "mtf/Config/parameters.h"
// general utilities for image drawing, etc.
#include "mtf/Utilities/miscUtils.h"

#ifdef _WIN32
#define snprintf  _snprintf
#include <time.h>
#else
#include <ctime> 
#endif
#include <string.h>
#include <numeric>
#include <vector>
#include <iomanip>

#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"

#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/path.hpp"

#define MAX_FPS 1e6

using namespace std;
using namespace mtf::params;
namespace fs = boost::filesystem;

int main(int argc, char * argv[]) {
	// *************************************************************************************************** //
	// ********************************** read configuration parameters ********************************** //
	// *************************************************************************************************** //

	if(!readParams(argc, argv)){ return EXIT_FAILURE; }

	printf("*******************************\n");
	printf("Using parameters:\n");
	printf("actor_id: %d\n", actor_id);
	printf("source_id: %d\n", source_id);
	printf("source_name: %s\n", source_name.c_str());
	printf("actor: %s\n", actor.c_str());
	printf("pipeline: %c\n", pipeline);
	printf("img_source: %c\n", img_source);
	printf("*******************************\n");

	// *********************************************************************************************** //
	// ********************************** initialize input pipeline ********************************** //
	// *********************************************************************************************** //

	InputBase *input = getInputObj();
	if(!input->initialize()){
		printf("Pipeline could not be initialized successfully. Exiting...\n");
		return EXIT_FAILURE;
	}
	//printf("done getting no. of frames\n");
	printf("n_frames=%d\n", input->n_frames);

	if(init_frame_id > 0){
		printf("Skipping %d frames...\n", init_frame_id);
	}
	for(int frame_id = 0; frame_id < init_frame_id; frame_id++){
		if(!input->update()){
			printf("Frame %d could not be read from the input pipeline", input->getFrameID() + 1);
			return EXIT_FAILURE;
		}
	}
	// ************************************************************************************* //
	// ********************************* read ground truth ********************************* //
	// ************************************************************************************* //

	CVUtils cv_utils;
	if(!cv_utils.readGT(source_name, source_path, input->n_frames,
		init_frame_id, debug_mode)){
		printf("Ground truth could not be read.\n");
		return EXIT_FAILURE;
	}

	if(reinit_from_gt){
		write_gt_ssm_params = false;
		if(!cv_utils.readReinitGT(source_name, source_path, input->n_frames,
			debug_mode, use_opt_gt, reinit_gt_from_bin, opt_gt_ssm)){
			printf("Reinitialization ground truth could not be read.\n");
			return EXIT_FAILURE;
		}
	} 

	// ******************************************************************************************** //
	// *************************************** setup output *************************************** //
	// ******************************************************************************************** //

	string cv_win_name = "showGroundTruth :: " + source_name;
	if(show_cv_window) {
		cv::namedWindow(cv_win_name, cv::WINDOW_AUTOSIZE);
	}
	int valid_gt_frames = cv_utils.ground_truth.size();
	if(input->n_frames <= 0 || valid_gt_frames < input->n_frames){
		if(input->n_frames <= 0){
			printf("Ground truth is unavailable\n");
		} else if(valid_gt_frames > 0){
			printf("Ground truth is only available for %d out of %d frames\n",
				valid_gt_frames, input->n_frames);			
		} 
		return EXIT_FAILURE;
	}
	cv::VideoWriter output;
	if(record_frames){
		output.open("ground_truth.avi", CV_FOURCC('M', 'J', 'P', 'G'), 24, input->getFrame().size());
	}
	cv::Scalar gt_color(0, 255, 0), reinit_gt_color(0, 0, 255);
	cv::Point2d corners[4];

	// *********************************************************************************************** //
	// ************************************** start visualization ************************************ //
	// *********************************************************************************************** //
	cv::Point text_origin(10, 20);
	double text_font_size = 0.50;
	cv::Scalar text_color(0, 255, 0);
	mtf::StateSpaceModel *ssm=nullptr;
	std::string out_fname;
	cv::Mat curr_corners, next_corners;
	VectorXd ssm_params, curr_state, next_state, curr_state_inv;
	if(write_gt_ssm_params){
		std::string out_dir = db_root_path + "/" + actor + "/" + "SSM";
		if(!fs::exists(out_dir)){
			printf("SSM param directory: %s does not exist. Creating it...\n", out_dir.c_str());
			fs::create_directories(out_dir);
		}
		ssm = mtf::getSSM(mtf_ssm);
		out_fname = out_dir + "/" + source_name + "." + ssm->name;
		printf("Writing GT SSM parameters to: %s\n", out_fname.c_str());
		fclose(fopen(out_fname.c_str(), "w"));
		ssm_params.resize(ssm->getStateSize());
		curr_state.resize(ssm->getStateSize());
		next_state.resize(ssm->getStateSize());
		curr_state_inv.resize(ssm->getStateSize());
	}


	while(true) {
		// *************************** display/save ground truth annotated frames *************************** //
		curr_corners = cv_utils.ground_truth[input->getFrameID()];
		if(write_gt_ssm_params){
			ssm->setCorners(curr_corners);
			curr_state = ssm->getState();
			ssm->invertState(curr_state_inv, curr_state);
		}
		mtf::utils::drawCorners(input->getFrameMutable(), curr_corners,
			gt_color, line_thickness);
		if(reinit_from_gt){
			cv::Mat reinit_gt_corners = cv_utils.reinit_ground_truth[init_frame_id][input->getFrameID() - init_frame_id];
			mtf::utils::drawCorners(input->getFrameMutable(), reinit_gt_corners,
				reinit_gt_color, line_thickness);
			//printf("frame_id: %d : \n", input->getFrameID());
			//cout << reinit_gt_corners << "\n";
		}
		putText(input->getFrameMutable(), cv::format("frame: %d", input->getFrameID() + 1),
			text_origin, cv::FONT_HERSHEY_SIMPLEX, text_font_size, text_color);
		if(show_cv_window){
			imshow(cv_win_name, input->getFrame());
			int pressed_key = cv::waitKey(1 - pause_after_frame);

			if(pressed_key == 27){
				break;
			}
			if(pressed_key == 32){
				pause_after_frame = 1 - pause_after_frame;
			}
		}

		if(input->n_frames > 0 && input->getFrameID() >= input->n_frames - 1){
			printf("==========End of input stream reached==========\n");
			break;
		}
		if(record_frames){
			output.write(input->getFrame());
		}
		// ******************************* update pipeline ******************************* //

		// update pipeline
		if(!input->update()){
			printf("Frame %d could not be read from the input pipeline", input->getFrameID() + 1);
			break;
		}
		if(write_gt_ssm_params){
			next_corners = cv_utils.ground_truth[input->getFrameID()];
			ssm->setCorners(next_corners);
			next_state = ssm->getState();			
			ssm->composeWarps(ssm_params, next_state, curr_state_inv);
			mtf::utils::printScalarToFile(input->getFrameID(), nullptr, out_fname.c_str(),
				"%d", "a", "\t", "\t");
			mtf::utils::printMatrixToFile(ssm_params.transpose(), nullptr, out_fname.c_str());
		}
	}
	cv::destroyAllWindows();
	if(record_frames){
		output.release();
	}
	delete(input);
	return EXIT_SUCCESS;
}
