#ifndef MTF_INPUT_BASE
#define MTF_INPUT_BASE

#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "mtf/Config/parameters.h"

#ifdef _WIN32
#define snprintf  _snprintf
#endif

#define NCHANNELS 3

#define SRC_VID 'm'
#define SRC_USB_CAM 'u'
#define SRC_FW_CAM 'f'
#define SRC_IMG 'j'

#define MAX_PATH_SIZE 500

using namespace std;

class InputBase {
protected:
	int img_width;
	int img_height;
	int n_buffers;
	int n_channels;
	int buffer_id;


	char img_source;
	string file_path;
	string dev_name;
	string dev_fmt;
	string dev_path;

public:
	int n_frames;

	InputBase() : img_width(0), img_height(0), n_buffers(0), 
		n_channels(0), buffer_id(0), n_frames(0), img_source('j'){}
	InputBase(char _img_source, string _dev_name, string _dev_fmt,
		string _dev_path, int _n_buffers = 1) :
		img_source(_img_source),
		dev_name(_dev_name),
		dev_fmt(_dev_fmt),
		dev_path(_dev_path),
		n_buffers(_n_buffers) {

		printf("Starting InputBase constructor\n");
		printf("img_source=%c\n", img_source);
		/*fprintf(stdout, "before: dev_name=%d\n", dev_name);
		fprintf(stdout, "before: dev_fmt=%d\n", dev_fmt);*/

		n_frames = 0;

		if(img_source == SRC_VID){
			file_path = dev_path + "/" + dev_name + "." + dev_fmt;
			n_frames = getNumberOfVideoFrames(file_path.c_str());
		} else if(img_source == SRC_IMG || img_source == SRC_DISK){
			file_path = dev_path + "/" + dev_name + "/frame%05d." + dev_fmt;
			n_frames = getNumberOfFrames(file_path.c_str());
		}
		fprintf(stdout, "dev_name=%s\n", dev_name.c_str());
		fprintf(stdout, "dev_path=%s\n", dev_path.c_str());
		fprintf(stdout, "dev_fmt=%s\n", dev_fmt.c_str());
	}

	virtual ~InputBase(){}
	virtual bool initialize() = 0;
	virtual bool update() = 0;

	virtual void remapBuffer(uchar **new_addr) = 0;
	virtual int getFrameID() = 0;
	virtual const cv::Mat& getFrame() = 0;
	virtual cv::Mat& getFrameMutable() = 0;

	int getNumberOfFrames(const char *file_template){
		FILE* fid;
		int frame_id = 0;
		while(fid = fopen(cv::format(file_template, ++frame_id).c_str(), "r")){
			fclose(fid);
		}
		return frame_id - 1;
	}

	int getNumberOfVideoFrames(const char *file_name){
		cv::VideoCapture cap_obj(file_name);
		cv::Mat img;
		int frame_id = 0;
		while(cap_obj.read(img)){ ++frame_id; }
		cap_obj.release();
		return frame_id;
	}
};
#endif
