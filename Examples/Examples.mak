# include Examples/Tools/modules.mak

utt ?= 0
uavold ?= 0
am ? = ssd
am ? = 4

EXAMPLES_ROOT_DIR = Examples
EXAMPLES_SRC_DIR = ${EXAMPLES_ROOT_DIR}/src
EXAMPLES_INCLUDE_DIR = ${EXAMPLES_ROOT_DIR}/include
EXAMPLES_HEADER_DIR = ${EXAMPLES_INCLUDE_DIR}/mtf/Tools
EXAMPLES_INCLUDE_FLAGS = -I${EXAMPLES_INCLUDE_DIR}
EXAMPLES_TOOLS = PreProc cvUtils inputBase inputCV pipeline

EXAMPLES_TOOLS_HEADERS = $(addprefix ${EXAMPLES_HEADER_DIR}/,$(addsuffix .h, ${EXAMPLES_TOOLS}))
MTF_HEADERS += ${EXAMPLES_TOOLS_HEADERS}
MTF_INCLUDE_DIRS += ${EXAMPLES_INCLUDE_DIR}

MTF_EXEC_INSTALL_DIR ?= /usr/local/bin
MTF_PY_INSTALL_DIR ?= ../../CModules/
PYTHON_INCLUDE_DIR ?= /usr/include/python2.7
MTF_PY_LIB_NAME ?= pyMTF.so
MTF_TEST_INSTALL_DIR ?= /usr/local/bin
MTF_RUNTIME_FLAGS += -std=c++11
LIBS_BOOST +=  -lboost_random -lboost_filesystem -lboost_system
FLAGS_TBB +=  -L/opt/intel/composer_xe_2015/tbb/lib/intel64/gcc4.4

UAV_FLAGS =
MTF_AM= SSD
MTF_SSM = Similitude

ifeq (${use_caffe}, 1)
MTF_LIBS += -lstdc++ -lglog -L${CAFFE_LIBRARY_PATH} -lcaffe
endif
ifeq (${utt}, 1)
UAV_FLAGS += -DUSE_TEMPLATED_SM -DMTF_AM=${MTF_AM} -DMTF_SSM=${MTF_SSM}
endif
ifeq (${uavold}, 1)
UAV_FLAGS += -DUSE_OLD_METHOD
endif
ifeq (${am}, ssd)
MTF_AM= SSD
else ifeq (${am}, mi)
MTF_AM= MI
else ifeq (${am}, ncc)
MTF_AM= NCC
endif

ifeq (${ssm}, 2)
MTF_SSM = Translation
else ifeq (${ssm}, 4)
MTF_SSM = Similitude
else ifeq (${ssm}, 6)
MTF_SSM = Affine
else ifeq (${ssm}, 8)
MTF_SSM = Homography
endif

ifeq (${o}, 1)
# LIBS_PARALLEL += -ltbb
MTF_RUNTIME_FLAGS += -O3 -D NDEBUG -D EIGEN_NO_DEBUG
_MTF_EXE_NAME = runMTF
_MTF_TEST_EXE_NAME = testMTF
_MTF_PATCH_EXE_NAME = extractPatch
_MTF_UAV_EXE_NAME = trackUAVTrajectory
_MTF_GT_EXE_NAME = showGroundTruth
else
# LIBS_PARALLEL += -ltbb_debug
MTF_RUNTIME_FLAGS += -g -O0
_MTF_EXE_NAME = runMTFd
_MTF_TEST_EXE_NAME = testMTFd
_MTF_PATCH_EXE_NAME = extractPatchd
_MTF_UAV_EXE_NAME = trackUAVTrajectoryd
_MTF_GT_EXE_NAME = showGroundTruthd
endif

MTF_EXE_NAME = $(addsuffix ${LIB_POST_FIX}, ${_MTF_EXE_NAME})
MTF_TEST_EXE_NAME = $(addsuffix ${LIB_POST_FIX}, ${_MTF_TEST_EXE_NAME})
MTF_PATCH_EXE_NAME = $(addsuffix ${LIB_POST_FIX}, ${_MTF_PATCH_EXE_NAME})
MTF_UAV_EXE_NAME = $(addsuffix ${LIB_POST_FIX}, ${_MTF_UAV_EXE_NAME})
MTF_GT_EXE_NAME = $(addsuffix ${LIB_POST_FIX}, ${_MTF_GT_EXE_NAME})

ifeq (${vp}, 1)
EXAMPLES_TOOLS += inputVP
MTF_RUNTIME_FLAGS += -lvisp_io -lvisp_sensor
endif

.PHONY: mtfe mtfi mtfp mtfc mtfu mtft
.PHONY: install_exec install_uav install_patch install_py install_test
.PHONY: run

mtfe: ${BUILD_DIR}/${MTF_EXE_NAME}
mtfen: ${BUILD_DIR}/${MTF_NT_EXE_NAME}

mtfi: install install_exec
mtfpa: install install_patch
mtfp: install install_py
mtfu: install install_uav
mtfg: install install_gt
mtft: install install_test_lib install_test

mtfc: clean
	rm -f ${BUILD_DIR}/${MTF_EXE_NAME}

install_exec: ${MTF_EXEC_INSTALL_DIR}/${MTF_EXE_NAME}
install_gt: ${MTF_EXEC_INSTALL_DIR}/${MTF_GT_EXE_NAME}
install_uav: ${MTF_EXEC_INSTALL_DIR}/${MTF_UAV_EXE_NAME}
install_patch: ${MTF_EXEC_INSTALL_DIR}/${MTF_PATCH_EXE_NAME}
install_py: ${MTF_PY_INSTALL_DIR}/${MTF_PY_LIB_NAME}
install_test: ${MTF_TEST_INSTALL_DIR}/${MTF_TEST_EXE_NAME}

run: install_exec
	${MTF_EXE_NAME}
${MTF_EXEC_INSTALL_DIR}/${MTF_EXE_NAME}: ${BUILD_DIR}/${MTF_EXE_NAME}
	sudo cp -f $< $@	
${MTF_EXEC_INSTALL_DIR}/${MTF_UAV_EXE_NAME}: ${BUILD_DIR}/${MTF_UAV_EXE_NAME}
	sudo cp -f $< $@
${MTF_EXEC_INSTALL_DIR}/${MTF_PATCH_EXE_NAME}: ${BUILD_DIR}/${MTF_PATCH_EXE_NAME}
	sudo cp -f $< $@
${MTF_EXEC_INSTALL_DIR}/${MTF_GT_EXE_NAME}: ${BUILD_DIR}/${MTF_GT_EXE_NAME}
	sudo cp -f $< $@
${MTF_PY_INSTALL_DIR}/${MTF_PY_LIB_NAME}: ${BUILD_DIR}/${MTF_PY_LIB_NAME}
	sudo cp -f $< $@
${MTF_TEST_INSTALL_DIR}/${MTF_TEST_EXE_NAME}: ${BUILD_DIR}/${MTF_TEST_EXE_NAME}
	sudo cp -f $< $@

${BUILD_DIR}/${MTF_EXE_NAME}: | ${BUILD_DIR}	
${BUILD_DIR}/${MTF_PY_LIB_NAME}: | ${BUILD_DIR}	
${BUILD_DIR}/${MTF_TEST_EXE_NAME}: | ${BUILD_DIR}
${BUILD_DIR}/${MTF_UAV_EXE_NAME}: | ${BUILD_DIR}	
${BUILD_DIR}/${MTF_GT_EXE_NAME}: | ${BUILD_DIR}	
${BUILD_DIR}/${MTF_PATCH_EXE_NAME}: | ${BUILD_DIR}	

${BUILD_DIR}/${MTF_EXE_NAME}: ${EXAMPLES_SRC_DIR}/runMTF.cc ${MTF_HEADERS} ${ROOT_HEADER_DIR}/mtf.h
	${CXX}  $< -o $@ -w ${WARNING_FLAGS} ${MTF_RUNTIME_FLAGS} ${MTF_INCLUDE_FLAGS} ${EXAMPLES_INCLUDE_FLAGS} ${FLAGS64} ${FLAGSCV} ${MTF_LIB_LINK} ${LIBS} ${LIBSCV} ${LIBS_BOOST} ${LIBS_PARALLEL} ${MTF_LIBS} 
	
${BUILD_DIR}/${MTF_UAV_EXE_NAME}: ${EXAMPLES_SRC_DIR}/trackUAVTrajectory.cc ${MTF_HEADERS} ${ROOT_HEADER_DIR}/mtf.h
	${CXX} $< -o $@ -w ${UAV_FLAGS} ${WARNING_FLAGS} ${MTF_RUNTIME_FLAGS} ${MTF_INCLUDE_FLAGS} ${EXAMPLES_INCLUDE_FLAGS} ${FLAGS64} ${FLAGSCV} ${MTF_LIB_LINK} ${LIBS} ${LIBSCV} ${LIBS_BOOST} ${LIBS_PARALLEL} ${MTF_LIBS}
	
${BUILD_DIR}/${MTF_PATCH_EXE_NAME}: ${EXAMPLES_SRC_DIR}/extractPatch.cc ${MTF_HEADERS} ${ROOT_HEADER_DIR}/mtf.h
	${CXX} $< -o $@ -w ${WARNING_FLAGS} ${MTF_RUNTIME_FLAGS} ${MTF_INCLUDE_FLAGS} ${EXAMPLES_INCLUDE_FLAGS} ${FLAGS64} ${FLAGSCV} ${MTF_LIB_LINK} ${LIBS} ${LIBSCV} ${LIBS_BOOST} ${LIBS_PARALLEL} ${MTF_LIBS}
	
${BUILD_DIR}/${MTF_PY_LIB_NAME}: ${BUILD_DIR}/pyMTF.o 
	${CXX} -shared $< -o $@  ${LIBS_BOOST} ${LIBS_PARALLEL} ${MTF_LIBS} ${LIBSCV} -lpython2.7
	
${BUILD_DIR}/pyMTF.o: ${EXAMPLES_SRC_DIR}/pyMTF.cc ${MTF_HEADERS} ${ROOT_HEADER_DIR}/mtf.h
	${CXX} -w -c -fPIC $< ${WARNING_FLAGS} ${FLAGS64} ${FLAGSCV} ${MTF_RUNTIME_FLAGS} ${MTF_INCLUDE_FLAGS} ${EXAMPLES_INCLUDE_FLAGS} -I${PYTHON_INCLUDE_DIR} -I${PYTHON_INCLUDE_DIR}/numpy -o $@

${BUILD_DIR}/${MTF_GT_EXE_NAME}: ${EXAMPLES_SRC_DIR}/showGroundTruth.cc ${EXAMPLES_TOOLS_HEADERS} ${UTILITIES_HEADER_DIR}/miscUtils.h
	${CXX}  $< -o $@ -w ${WARNING_FLAGS} ${MTF_RUNTIME_FLAGS} ${EXAMPLES_INCLUDE_FLAGS} ${EXAMPLES_INCLUDE_FLAGS} ${FLAGS64} ${FLAGSCV} ${LIBS} ${MTF_LIB_LINK} ${LIBSCV} ${LIBS_BOOST} ${LIBS_PARALLEL} ${MTF_LIBS} 
		
${BUILD_DIR}/${MTF_TEST_EXE_NAME}: ${EXAMPLES_SRC_DIR}/testMTF.cc ${TEST_HEADER_DIR}/mtf_test.h ${TEST_HEADERS} ${APPEARANCE_HEADERS} ${STATE_SPACE_HEADERS} ${CONFIG_HEADERS}
	${CXX} $< -o $@ -w ${WARNING_FLAGS} ${MTF_RUNTIME_FLAGS} ${MTF_INCLUDE_FLAGS} ${EXAMPLES_INCLUDE_FLAGS} ${FLAGS64} ${FLAGSCV} ${MTF_LIB_LINK} ${LIBS} ${LIBSCV} ${LIBS_PARALLEL} ${LIBS_BOOST} ${MTF_TEST_LIBS} ${MTF_LIBS}
	
