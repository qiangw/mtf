#include "mtf/Test/Diagnostics.h"
#include "mtf/Utilities/miscUtils.h"

_MTF_BEGIN_NAMESPACE


void Diagnostics::generateInverseAnalyticalData(VectorXd &param_range_vec,
	int n_pts, ADT data_type, const char* fname){
	assert(param_range_vec.size() == ssm_state_size);

	am->updatePixVals(ssm->getPts());
	updateCurrPixJacobian();
	updateCurrPixHessian();
	if(data_type==ADT::CurrSelf)
		am->cmptSelfHessian(curr_self_hessian, curr_pix_jacobian);
	else if(data_type == ADT::CurrSelf2)
		am->cmptSelfHessian(curr_self_hessian2, curr_pix_jacobian, curr_pix_hessian);
	else if(data_type == ADT::FeatNorm)	
		am->updateDistFeat(curr_dist_vec.data());

	if(params.show_patches){
		curr_patch.convertTo(curr_patch_uchar, curr_patch_uchar.type());
		//utils::printMatrix<double>(curr_patch, "curr_patch");
		//utils::printMatrix<uchar>(curr_patch_uchar, "curr_patch_uchar", "%d");
		imshow(curr_patch_win_name, curr_patch_uchar);
	}

	ssm->initialize(init_corners);
	VectorXd param_range_vec_norm(ssm_state_size);
	for(int i = 0; i < ssm_state_size; i++){
		param_range_vec_norm(i) = param_range_vec(i) / ssm_grad_norm_mean(i);
	}
	//VectorXd base_state = ssm->getInitState();
	VectorXd min_state = -param_range_vec_norm;
	VectorXd max_state = param_range_vec_norm;

	VectorXd state_update(ssm_state_size);
	diagnostics_data.resize(n_pts, 2 * ssm_state_size);

	// all further updates need to be done using the initial image;
	// as the current image  is not needed anymore, there is no need to back it up;
	am->setCurrImg(init_img_cv);

	double data_val;
	for(int state_id = 0; state_id < ssm_state_size; state_id++){
		//printf("Processing state parameter %d....\n", state_id);

		state_update.setZero();
		diagnostics_data.col(2 * state_id) = VectorXd::LinSpaced(n_pts, min_state(state_id), max_state(state_id));
		for(int pt_id = 0; pt_id < n_pts; pt_id++){

			state_update(state_id) = diagnostics_data(pt_id, 2 * state_id);

			//utils::printMatrix(ssm->getCorners(), "init_corners");
			updateSSM(state_update);
			//utils::printMatrix(state_update, "state_update");
			//utils::printMatrix(ssm->getCorners(), "curr_corners");	
			
			switch(data_type){
			case  ADT::Norm:
				updateInitSimilarity();
				data_val = am->getSimilarity();
				break;
			case  ADT::FeatNorm:
				// don't need curr_pix_vals anymore so can safely overwrite with the values extracted from init_img
				am->updatePixVals(ssm->getPts());
				am->updateDistFeat();
				data_val = (*am)(am->getDistFeat(), curr_dist_vec.data(), am_dist_size);
				break;
			case  ADT::StdJac:
				updateInitGrad();
				updateInitPixJacobian();
				am->cmptInitJacobian(similarity_jacobian, init_pix_jacobian);
				data_val = similarity_jacobian(state_id);
				break;
			case  ADT::ESMJac:
				updateInitGrad();
				updateInitPixJacobian();
				mean_pix_jacobian = (init_pix_jacobian + curr_pix_jacobian) / 2.0;
				am->cmptInitJacobian(similarity_jacobian, mean_pix_jacobian);
				data_val = similarity_jacobian(state_id);
				break;
			case  ADT::DiffOfJacs:
				updateInitGrad();
				updateInitPixJacobian();
				am->updateCurrGrad();
				am->cmptDifferenceOfJacobians(similarity_jacobian, init_pix_jacobian, curr_pix_jacobian);
				data_val = similarity_jacobian(state_id)*0.5;
				break;
			case  ADT::Std:
				updateInitHess();
				updateInitPixJacobian();
				am->cmptInitHessian(hessian, init_pix_jacobian);
				data_val = hessian(state_id, state_id);
				break;
			case  ADT::Std2:
				updateInitHess();
				updateInitPixJacobian();
				updateInitPixHessian();
				am->cmptInitHessian(hessian, init_pix_jacobian,
					init_pix_hessian);
				data_val = hessian(state_id, state_id);
				break;
			case  ADT::ESM:
				updateInitHess();
				updateInitPixJacobian();
				mean_pix_jacobian = (init_pix_jacobian + curr_pix_jacobian) / 2.0;
				am->cmptInitHessian(hessian, mean_pix_jacobian);
				data_val = hessian(state_id, state_id);
				break;
			case  ADT::ESM2:
				updateInitHess();
				updateInitPixJacobian();
				updateInitPixHessian();
				mean_pix_jacobian = (init_pix_jacobian + curr_pix_jacobian) / 2.0;
				mean_pix_hessian = (init_pix_hessian + curr_pix_hessian) / 2.0;
				am->cmptCurrHessian(hessian, mean_pix_jacobian,
					mean_pix_hessian);
				data_val = hessian(state_id, state_id);
				break;
			case  ADT::InitSelf:
				updateInitSelfHess();
				updateInitPixJacobian();
				am->cmptSelfHessian(hessian, init_pix_jacobian);
				data_val = hessian(state_id, state_id);
				break;
			case  ADT::InitSelf2:
				updateInitSelfHess();
				updateInitPixJacobian();
				updateInitPixHessian();
				am->cmptSelfHessian(hessian, init_pix_jacobian, init_pix_hessian);
				data_val = hessian(state_id, state_id);
				break;
			case  ADT::CurrSelf:
				data_val = curr_self_hessian(state_id, state_id);
				break;
			case  ADT::CurrSelf2:
				data_val = curr_self_hessian2(state_id, state_id);
				break;
			case  ADT::SumOfStd:
				updateInitHess();
				updateInitPixJacobian();
				am->updateCurrGrad();
				am->cmptSumOfHessians(hessian, init_pix_jacobian, curr_pix_jacobian);
				data_val = hessian(state_id, state_id) / 2.0;
				break;
			case  ADT::SumOfStd2:
				updateInitHess();
				updateInitPixJacobian();
				updateInitPixHessian();
				am->updateCurrGrad();
				am->cmptSumOfHessians(hessian, init_pix_jacobian, curr_pix_jacobian,
					init_pix_hessian, curr_pix_hessian);
				data_val = hessian(state_id, state_id) / 2.0;
				break;
			case  ADT::SumOfSelf:
				updateInitSelfHess();
				updateInitPixJacobian();
				am->cmptSelfHessian(hessian, init_pix_jacobian);
				hessian += curr_self_hessian;
				data_val = hessian(state_id, state_id) / 2.0;
				break;
			case  ADT::SumOfSelf2:
				updateInitSelfHess();
				updateInitPixJacobian();
				updateInitPixHessian();
				am->cmptSelfHessian(hessian, init_pix_jacobian, init_pix_hessian);
				hessian += curr_self_hessian2;
				data_val = hessian(state_id, state_id) / 2.0;
				break;
			default:
				throw std::invalid_argument("Diagnostics :: Invalid data type specified");
			}
			diagnostics_data(pt_id, 2 * state_id + 1) = data_val;
			resetSSM(state_update);
			if(params.show_patches){
				init_patch.convertTo(init_patch_uchar, init_patch_uchar.type());
			}
			//if((pt_id + 1) % 50 == 0){ printf("Done %d points\n", pt_id + 1); }
		}
	}
	if(fname){
		printf("Writing diagnostics data to: %s\n", fname);
		utils::printMatrixToFile(diagnostics_data, "diagnostics_data", fname, "%15.9f", "w");
	}
}

_MTF_END_NAMESPACE
