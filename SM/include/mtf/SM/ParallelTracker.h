#ifndef MTF_PARALLEL_TRACKER_H
#define MTF_PARALLEL_TRACKER_H

#include "CompositeBase.h"


#define PARL_ESTIMATION_METHOD 0
#define PARL_RESET_TO_MEAN 0
#define PARL_AUTO_REINIT 0
#define PARL_REINIT_ERR_THRESH 1
#define PRL_SM_REINIT_FRAME_GAP 1

_MTF_BEGIN_NAMESPACE

struct ParallelParams {
	enum class EstimationMethod {
		MeanOfCorners,
		MeanOfState
	};
	EstimationMethod estimation_method;
	bool reset_to_mean;
	bool auto_reinit;
	double reinit_err_thresh;
	int reinit_frame_gap;
	static const char* toString(EstimationMethod _estimation_method);
	ParallelParams(EstimationMethod _estimation_method, bool _reset_to_mean,
		bool _auto_reinit, double _reinit_err_thresh, int _reinit_frame_gap);
	ParallelParams(const ParallelParams *params = nullptr);
};

// run multiple trackers in parallel
class ParallelTracker : public CompositeBase {

public:
	typedef ParallelParams ParamType;
	ParamType params;

	bool failure_detected;
	vector<cv::Mat> img_buffer, corners_buffer;
	int buffer_id;
	bool buffer_filled;
	cv::Mat curr_img;

	typedef ParamType::EstimationMethod EstimationMethod;

	cv::Mat mean_corners_cv;

	ParallelTracker(const vector<TrackerBase*> _trackers, const ParamType *parl_params);
	void setImage(const cv::Mat &img) override;
	void initialize(const cv::Mat &corners) override;
	void update() override;
	const cv::Mat& getRegion() override {
		return mean_corners_cv;
	}
	void setRegion(const cv::Mat& corners)  override;
};

_MTF_END_NAMESPACE

#endif

