#ifndef MTF_CASCADE_TRACKER_H
#define MTF_CASCADE_TRACKER_H

#include "mtf/SM/CompositeBase.h"

#define CASC_ENABLE_FEEDBACK true

_MTF_BEGIN_NAMESPACE

struct CascadeParams{
	bool enable_feedback;
	CascadeParams(bool _enable_feedback);
	CascadeParams(const CascadeParams *params = nullptr);
};

class CascadeTracker : public CompositeBase{

public:
	typedef CascadeParams ParamType;
	const ParamType params;
	
	CascadeTracker(const vector<TrackerBase*> _trackers, const ParamType *casc_params);
	void initialize(const cv::Mat &corners) override;
	void update() override;
	void setRegion(const cv::Mat& corners)  override;
	const cv::Mat& getRegion()  override{ return trackers[n_trackers - 1]->getRegion(); }
};
_MTF_END_NAMESPACE

#endif

