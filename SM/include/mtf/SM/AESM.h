#ifndef MTF_AESM_H
#define MTF_AESM_H

#include "ESM.h"

_MTF_BEGIN_NAMESPACE

// Additive formulation of ESM
template<class AM, class SSM>
class AESM : public ESM < AM, SSM > {

	typedef ESM<AM, SSM> ESMBase;
	inherit_profiling(ESMBase);

public:
	using ESM<AM, SSM> ::am;
	using ESM<AM, SSM> ::ssm;
	using typename ESM<AM, SSM> ::AMParams;
	using typename ESM<AM, SSM> ::SSMParams;
	using typename ESM<AM, SSM> ::ParamType;
	using ESM<AM, SSM> ::cv_corners_mat;
	using ESM<AM, SSM> ::name;
	using ESM<AM, SSM> ::initialize;
	using ESM<AM, SSM> ::update;

	using ESM<AM, SSM> ::params;
	using ESM<AM, SSM> ::log_fname;
	using ESM<AM, SSM> ::time_fname;
	using ESM<AM, SSM> ::init_pix_jacobian;
	using ESM<AM, SSM> ::curr_pix_jacobian;
	using ESM<AM, SSM> ::init_pix_hessian;
	using ESM<AM, SSM> ::curr_pix_hessian;
	using ESM<AM, SSM> ::ssm_update;

	//using typename ESM<AM, SSM> ::HessType;
	//using ESM<AM, SSM> ::mean_pix_hessian;
	//using ESM<AM, SSM> ::hessian;
	//using ESM<AM, SSM> ::mean_pix_jacobian;
	//using ESM<AM, SSM> ::mean_pix_hessian;
	//using ESM<AM, SSM> ::init_self_hessian;

	void initializePixJacobian() override;
	void updatePixJacobian() override;
	void initializePixHessian() override;
	void updatePixHessian() override;
	void updateSSM() override;

	AESM(const ParamType *aesm_params = nullptr,
		const AMParams *am_params = nullptr, const SSMParams *ssm_params = nullptr);
};
_MTF_END_NAMESPACE

#endif

