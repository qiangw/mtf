#ifndef MTF_ICLK_PARAMS_H
#define MTF_ICLK_PARAMS_H

#include "mtf/Macros/common.h"

#define ICLK_MAX_ITERS 10
#define ICLK_EPSILON 0.01
#define ICLK_HESS_TYPE 0
#define ICLK_SEC_ORD_HESS false
#define ICLK_UPDATE_SSM false
#define ICLK_CHAINED_WARP false
#define ICLK_DEBUG_MODE false

_MTF_BEGIN_NAMESPACE

struct ICLKParams{
	enum class HessType{ InitialSelf, CurrentSelf, Std };
	int max_iters; //! maximum iterations of the ICLK algorithm to run for each frame
	double epsilon; //! maximum L2 norm of the state update vector at which to stop the iterations
	HessType hess_type;
	bool sec_ord_hess;
	bool update_ssm;
	bool chained_warp;
	bool debug_mode; //! decides whether logging data will be printed for debugging purposes; 
	//! only matters if logging is enabled at compile time

	ICLKParams(int _max_iters, double _epsilon,
		HessType _hess_type, bool _sec_ord_hess,
		bool _update_ssm, bool _chained_warp,
		bool _debug_mode);
	ICLKParams(const ICLKParams *params = nullptr);
};

_MTF_END_NAMESPACE

#endif

