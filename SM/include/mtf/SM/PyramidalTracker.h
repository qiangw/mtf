#ifndef MTF_PYRAMIDAL_TRACKER_H
#define MTF_PYRAMIDAL_TRACKER_H

#include "CompositeBase.h"

#define PYRL_NO_OF_LEVELS 3
#define PYRL_SCALE_FACTOR 0
#define PYRL_AUTO_REINIT 0
#define PYRL_REINIT_ERR_THRESH 1
#define PYRL_SM_REINIT_FRAME_GAP 1

_MTF_BEGIN_NAMESPACE

struct PyramidalParams {
	int no_of_levels;
	double scale_factor;
	bool show_levels;
	PyramidalParams(int no_of_levels, double scale_factor, 
		bool show_levels);
	PyramidalParams(const PyramidalParams *params = nullptr);
};

// run multiple trackers in parallel
class PyramidalTracker : public CompositeBase {

public:
	typedef PyramidalParams ParamType;
	const ParamType params;

	vector<cv::Size> img_sizes;
	vector<cv::Mat> img_pyramid;
	double overall_scale_factor;
	bool external_img_pyramid;

	PyramidalTracker(const vector<TrackerBase*> _trackers, const ParamType *parl_params);
	void setImage(const cv::Mat &img) override;
	void initialize(const cv::Mat &corners) override;
	void update() override;
	const cv::Mat& getRegion() override { 
		return trackers[0]->getRegion();
	}
	void setRegion(const cv::Mat& corners)  override;
	int inputType() const override{
		return trackers[0]->inputType();
	}
	void setImagePyramid(const vector<cv::Mat> &_img_pyramid);
	const vector<cv::Mat>& getImagePyramid() const{ return img_pyramid; }
	void updateImagePyramid();
	void showImagePyramid();
};

_MTF_END_NAMESPACE

#endif

