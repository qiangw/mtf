#ifndef MTF_NN_PARAMS_H
#define MTF_NN_PARAMS_H

#include <flann/flann.hpp>

#define NN_MAX_ITERS 10
#define NN_EPSILON 0.01
#define NN_CORNER_SIGMA_D 0.06
#define NN_CORNER_SIGMA_T 0.04
#define NN_N_SAMPLES 1000
#define NN_N_TREES 6
#define NN_N_CHECKS 50
#define NN_SEARCH_TYPE 0
#define NN_INDEX_TYPE 1
#define NN_CONFIG_FILE "Config/nn.cfg"
#define NN_ADDITIVE_UPDATE 1
#define NN_SHOW_SAMPLES 1
#define NN_ADD_POINTS 0
#define NN_REMOVE_POINTS 0
#define NN_SSM_SIGMA_PREC 1.1
#define NN_LOAD_INDEX 0
#define NN_SAVE_INDEX 0
#define NN_INDEX_FILE_TEMPLATE "nn_saved_index"
#define NN_DEBUG_MODE false
#define NN_CORNER_GRAD_EPS 1e-5

#define GNN_BUILD_USING_FLANN -1
#define NN_KDT_TREES 6
#define NN_KM_BRANCHING 32
#define NN_KM_ITERATIONS 11
#define NN_KM_CENTERS_INIT flann::FLANN_CENTERS_RANDOM
#define NN_KM_CB_INDEX 0.2
#define NN_KDTS_LEAF_MAX_SIZE 10
#define NN_KDTC_LEAF_MAX_SIZE 64
#define NN_HC_BRANCHING 32
#define NN_HC_CENTERS_INIT flann::FLANN_CENTERS_RANDOM
#define NN_HC_TREES 4
#define NN_HC_LEAF_MAX_SIZE 100
#define NN_AUTO_TARGET_PRECISION 0.9
#define NN_AUTO_BUILD_WEIGHT 0.01
#define NN_AUTO_MEMORY_WEIGHT 0
#define NN_AUTO_SAMPLE_FRACTION 0.1

typedef std::vector<double> vectord;
typedef std::vector<vectord> vectorvd;

namespace mtf{
	// index specific params
	struct NNIndexParams{
		int gnn_degree;
		int gnn_max_steps;
		int gnn_cmpt_dist_thresh;
		int gnn_build_using_flann;
		bool gnn_verbose;
		int kdt_trees;
		int km_branching;
		int km_iterations;
		flann::flann_centers_init_t km_centers_init;
		float km_cb_index;
		int kdts_leaf_max_size;
		int kdtc_leaf_max_size;
		int hc_branching;
		flann::flann_centers_init_t hc_centers_init;
		int hc_trees;
		int hc_leaf_max_size;
		float auto_target_precision;
		float auto_build_weight;
		float auto_memory_weight;
		float auto_sample_fraction;

		NNIndexParams(
			int gnn_degree,
			int gnn_max_steps,
			int gnn_cmpt_dist_thresh,
			int gnn_build_using_flann,
			bool gnn_verbose,
			int kdt_trees,
			int km_branching,
			int km_iterations,
			flann::flann_centers_init_t,
			float km_cb_index,
			int kdts_leaf_max_size,
			int kdtc_leaf_max_size,
			int hc_branching,
			flann::flann_centers_init_t,
			int hc_trees,
			int hc_leaf_max_size,
			float auto_target_precision,
			float auto_build_weight,
			float auto_memory_weight,
			float auto_sample_fraction);
		NNIndexParams(const NNIndexParams *params = nullptr);
	};
	struct NNParams{
		enum class IdxType{
			GNN, KDTree, HierarchicalClustering, KMeans, Composite, Linear,
			KDTreeSingle, KDTreeCuda3d, Autotuned
		};
		enum class SearchType{ KNN, Radius };
		static const char* toString(IdxType index_type);
		static const char* toString(SearchType index_type);

		NNIndexParams index;
		flann::SearchParams search;

		int max_iters; //! maximum iterations of the NN algorithm to run for each frame
		int n_samples;
		double epsilon; //! maximum L2 norm of the state update vector at which to stop the iterations	

		vectorvd ssm_sigma;
		vectorvd ssm_mean;
		vectord pix_sigma;

		SearchType search_type;
		IdxType index_type;
		int n_checks;

		bool additive_update;
		int show_samples;
		int add_points;
		int remove_points;

		bool save_index;
		bool load_index;
		std::string saved_index_dir;

		//! decides whether logging data will be printed for debugging purposes; 
		//! only matters if logging is enabled at compile time
		bool debug_mode;

		/**
		obsolete parameters - to be removed soon
		*/
		double corner_sigma_d;
		double corner_sigma_t;
		//! precision within which the change in corners produced by the change in
		// a parameter for the chosen sigma should match the desired change in corners
		double ssm_sigma_prec;

		NNParams(const NNIndexParams &_index, int _max_iters, int _n_samples, 
			double _epsilon, const vectorvd &_ssm_sigma, vectorvd _ssm_mean,
			vectord _pix_sigma, SearchType _search_type,
			IdxType _index_type, int _n_checks,
			bool _additive_update, int _show_samples,
			int _add_points, int _remove_points,
			bool load_index, bool _save_index, std::string _saved_index_dir,
			double _corner_sigma_d, double _corner_sigma_t,
			double _ssm_sigma_prec, bool _debug_mode);
		NNParams(const NNParams *params = nullptr);
	};

}

#endif

