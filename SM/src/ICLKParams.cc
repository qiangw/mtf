#include "mtf/SM/ICLKParams.h"
_MTF_BEGIN_NAMESPACE

ICLKParams::ICLKParams(int _max_iters, double _epsilon,
HessType _hess_type, bool _sec_ord_hess,
bool _update_ssm, bool _chained_warp,
bool _debug_mode){
	max_iters = _max_iters;
	epsilon = _epsilon;
	hess_type = _hess_type;
	sec_ord_hess = _sec_ord_hess;
	update_ssm = _update_ssm;
	chained_warp = _chained_warp;
	debug_mode = _debug_mode;
}
ICLKParams::ICLKParams(const ICLKParams *params) :
max_iters(ICLK_MAX_ITERS),
epsilon(ICLK_EPSILON),
hess_type(static_cast<HessType>(ICLK_HESS_TYPE)),
sec_ord_hess(ICLK_SEC_ORD_HESS),
update_ssm(ICLK_UPDATE_SSM),
chained_warp(ICLK_CHAINED_WARP),
debug_mode(ICLK_DEBUG_MODE){
	if(params){
		max_iters = params->max_iters;
		epsilon = params->epsilon;
		hess_type = params->hess_type;
		sec_ord_hess = params->sec_ord_hess;
		update_ssm = params->update_ssm;
		chained_warp = params->chained_warp;
		debug_mode = params->debug_mode;
	}
}

_MTF_END_NAMESPACE


