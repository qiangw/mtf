#include "mtf/SM/PFParams.h"

_MTF_BEGIN_NAMESPACE
const char* PFParams::toString(DynamicModel _dyn_model){
	switch(_dyn_model){
	case DynamicModel::RandomWalk:
		return "RandomWalk";
	case DynamicModel::AutoRegression1:
		return "AutoRegression(1)";
	default:
		throw std::invalid_argument("Invalid dynamic model provided");
	}
}
const char* PFParams::toString(UpdateType _upd_type){
	switch(_upd_type){
	case UpdateType::Additive:
		return "Additive";
	case UpdateType::Compositional:
		return "Compositional";
	default:
		throw std::invalid_argument("Invalid dynamic model provided");
	}
}
const char* PFParams::toString(ResamplingType _resampling_type){
	switch(_resampling_type){
	case ResamplingType::None:
		return "None";
	case ResamplingType::BinaryMultinomial:
		return "BinaryMultinomial";
	case ResamplingType::LinearMultinomial:
		return "LinearMultinomial";
	case ResamplingType::Residual:
		return "Residual";
	default:
		throw std::invalid_argument("Invalid resampling type provided");
	}
}
const char* PFParams::toString(LikelihoodFunc _likelihood_func){
	switch(_likelihood_func){
	case LikelihoodFunc::AM:
		return "AM";
	case LikelihoodFunc::Gaussian:
		return "Gaussian";
	case LikelihoodFunc::Reciprocal:
		return "Reciprocal";
	default:
		throw std::invalid_argument("Invalid likelihood function provided");
	}
}
const char* PFParams::toString(MeanType _likelihood_func){
	switch(_likelihood_func){
	case MeanType::None:
		return "None";
	case MeanType::SSM:
		return "SSM";
	case MeanType::Corners:
		return "Corners";
	default:
		throw std::invalid_argument("Invalid mean type provided");
	}
}
PFParams::PFParams(int _max_iters, int _n_particles, double _epsilon,
	DynamicModel _dyn_model, UpdateType _upd_type,
	LikelihoodFunc _likelihood_func,
	ResamplingType _resampling_type,
	MeanType _mean_type, bool _reset_to_mean, 
	const vectorvd &_ssm_sigma, const vectorvd &_ssm_mean,
	bool _update_sampler_wts, double _min_particles_ratio,
	const vectord &_pix_sigma, double _measurement_sigma,
	int _show_particles, bool _update_template,
	bool _debug_mode){
	max_iters = _max_iters;
	n_particles = _n_particles;
	epsilon = _epsilon;
	dynamic_model = _dyn_model;
	update_type = _upd_type;
	likelihood_func = _likelihood_func;
	resampling_type = _resampling_type;
	reset_to_mean = _reset_to_mean;
	mean_type = _mean_type;
	ssm_sigma = _ssm_sigma;
	ssm_mean = _ssm_mean;
	update_sampler_wts = _update_sampler_wts;
	min_particles_ratio = _min_particles_ratio;
	pix_sigma = _pix_sigma;
	measurement_sigma = _measurement_sigma;
	show_particles = _show_particles;
	update_template = _update_template;
	debug_mode = _debug_mode;
}

PFParams::PFParams(const PFParams *params) :
max_iters(PF_MAX_ITERS),
n_particles(PF_N_PARTICLES),
epsilon(PF_EPSILON),
dynamic_model(static_cast<DynamicModel>(PF_DYN_MODEL)),
update_type(static_cast<UpdateType>(PF_UPD_TYPE)),
likelihood_func(static_cast<LikelihoodFunc>(PF_LIKELIHOOD_FUNC)),
resampling_type(static_cast<ResamplingType>(PF_RESAMPLING_TYPE)),
mean_type(static_cast<MeanType>(PF_MEAN_TYPE)),
reset_to_mean(PF_RESET_TO_MEAN),
update_sampler_wts(PF_UPDATE_SAMPLER_WTS),
min_particles_ratio(PF_MIN_PARTICLES_RATIO),
pix_sigma(PF_PIX_SIGMA),
measurement_sigma(PF_MEASUREMENT_SIGMA),
show_particles(PF_SHOW_PARTICLES),
update_template(PF_UPDATE_TEMPLATE),
debug_mode(PF_DEBUG_MODE){
	if(params){
		max_iters = params->max_iters;
		n_particles = params->n_particles;
		epsilon = params->epsilon;
		dynamic_model = params->dynamic_model;
		update_type = params->update_type;
		likelihood_func = params->likelihood_func;
		resampling_type = params->resampling_type;
		reset_to_mean = params->reset_to_mean;
		mean_type = params->mean_type;
		ssm_sigma = params->ssm_sigma;
		ssm_mean = params->ssm_mean;
		update_sampler_wts = params->update_sampler_wts;
		min_particles_ratio = params->min_particles_ratio;
		pix_sigma = params->pix_sigma;
		show_particles = params->show_particles;
		update_template = params->update_template;
		measurement_sigma = params->measurement_sigma;
		debug_mode = params->debug_mode;
	}
}

_MTF_END_NAMESPACE
