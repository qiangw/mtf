#include "mtf/SM/NT/PF.h"
#include "mtf/Utilities/miscUtils.h" 
#include <boost/random/random_device.hpp>
#include <boost/random/seed_seq.hpp>
#include "opencv2/highgui/highgui.hpp"
#include "mtf/Utilities/graphUtils.h"
#include "mtf/Utilities/histUtils.h"

_MTF_BEGIN_NAMESPACE
namespace nt{	
	PF::PF(AM *_am, SSM *_ssm, const ParamType *nn_params) :
		SearchMethod(_am, _ssm), params(nn_params){
		printf("\n");
		printf("Using Particle Filter (NT) SM with:\n");
		printf("max_iters: %d\n", params.max_iters);
		printf("n_particles: %d\n", params.n_particles);
		printf("epsilon: %f\n", params.epsilon);
		printf("dyn_model: %s\n", PFParams::toString(params.dynamic_model));
		printf("upd_type: %s\n", PFParams::toString(params.update_type));
		printf("likelihood_func: %s\n", PFParams::toString(params.likelihood_func));
		printf("resampling_type: %s\n", PFParams::toString(params.resampling_type));
		printf("mean_type: %s\n", PFParams::toString(params.mean_type));
		printf("reset_to_mean: %d\n", params.reset_to_mean);
		ssm_state_size = ssm->getStateSize();
		if(params.pix_sigma.empty() || params.pix_sigma[0]<=0){			
			if(params.ssm_sigma.empty()){
				throw std::invalid_argument("Sigma must be provided for at least one sampler");
			}
			using_pix_sigma = false;
			n_samplers = max(params.ssm_sigma.size(), params.ssm_mean.size());
		} else{
			using_pix_sigma = true;
			n_samplers = params.pix_sigma.size();
			utils::printMatrix(Map<const RowVectorXd>(params.pix_sigma.data(), params.pix_sigma.size()),
				"pix_sigma");
		}
		printf("n_samplers: %d\n", n_samplers);
		printf("update_sampler_wts: %d\n", params.update_sampler_wts);
		printf("min_particles_ratio: %f\n", params.min_particles_ratio);
		printf("measurement_sigma: %f\n", params.measurement_sigma);
		printf("show_particles: %d\n", params.show_particles);
		printf("update_template: %d\n", params.update_template);
		printf("debug_mode: %d\n", params.debug_mode);
		printf("appearance model: %s\n", am->name.c_str());
		printf("state space model: %s\n", ssm->name.c_str());
		printf("\n");

		name = "pf_nt";
		log_fname = "log/pf_debug.txt";
		time_fname = "log/pf_times.txt";
		frame_id = 0;

		const double pi = 3.14159265358979323846;
		measurement_factor = 1.0 / sqrt(2 * pi * params.measurement_sigma);

		for(int set_id = 0; set_id < 2; ++set_id){
			particle_states[set_id].resize(params.n_particles);
			particle_ar[set_id].resize(params.n_particles);
			particle_sampler[set_id].resize(params.n_particles);
			for(int particle_id = 0; particle_id < params.n_particles; ++particle_id){
				particle_states[set_id][particle_id].resize(ssm->getStateSize());
				particle_ar[set_id][particle_id].resize(ssm->getStateSize());
			}
		}

		min_particles = (params.n_particles*params.min_particles_ratio) / static_cast<double>(n_samplers);
		dynamic_particles = (params.n_particles - min_particles*n_samplers);

		curr_set_id = 0;
		particle_wts.resize(params.n_particles);
		particle_cum_wts.setZero(params.n_particles);
		sampler_wts.resize(n_samplers);
		sampler_n_particles.resize(n_samplers);

		perturbed_state.resize(ssm->getStateSize());
		perturbed_ar.resize(ssm->getStateSize());
		mean_state.resize(ssm->getStateSize());

		state_sigma.resize(n_samplers);
		state_mean.resize(n_samplers);
		if(!using_pix_sigma){
			if(params.ssm_mean.empty()){
				params.ssm_mean.push_back(vectord(ssm_state_size, 0));
			}
			int sampler_id = 0, sigma_id = 0, mean_id = 0;
			while(sampler_id < n_samplers){
				if(params.ssm_sigma[sigma_id].size() < ssm_state_size){
					throw std::invalid_argument(
						cv::format("PF :: SSM sigma for sampler %d has invalid size: %d",
						sampler_id, params.ssm_sigma[sigma_id].size()));
				}
				if(params.ssm_mean[mean_id].size() < ssm_state_size){
					throw std::invalid_argument(
						cv::format("PF :: SSM mean for sampler %d has invalid size: %d",
						sampler_id, params.ssm_mean[mean_id].size()));
				}
				state_sigma[sampler_id] = Map<const VectorXd>(params.ssm_sigma[sigma_id].data(), ssm_state_size);
				state_mean[sampler_id] = Map<const VectorXd>(params.ssm_mean[mean_id].data(), ssm_state_size);
				++sampler_id;
				if(sampler_id < params.ssm_sigma.size()){ ++sigma_id; }
				if(sampler_id < params.ssm_mean.size()){ ++mean_id; }
			}
		}
		// initialize random number generators for resampling and measurement function
		boost::random_device r;
		boost::random::seed_seq measurement_seed{ r(), r(), r(), r(), r(), r(), r(), r() };

		measurement_gen = RandGenT(measurement_seed);
		measurement_dist = MeasureDistT(0, params.measurement_sigma);

		boost::random::seed_seq resample_seed{ r(), r(), r(), r(), r(), r(), r(), r() };
		resample_gen = RandGenT(resample_seed);
		resample_dist = ResampleDistT(0, 1);

		if(params.debug_mode){
			fclose(fopen(log_fname, "w"));
			fclose(fopen("log/pf_particle_wts.txt", "w"));
			fclose(fopen("log/pf_particle_cum_wts.txt", "w"));
			resample_ids.resize(params.n_particles);
			resample_ids_hist.resize(params.n_particles);
			uniform_rand_nums.resize(params.n_particles);
		}
	}

	
	void PF::initialize(const cv::Mat &corners){
		am->clearInitStatus();
		ssm->clearInitStatus();

		ssm->initialize(corners, am->getNChannels());

		if(using_pix_sigma){
			//! estimate SSM parameter sigma from pixel sigma
			for(int sampler_id = 0; sampler_id < n_samplers; ++sampler_id){
				state_sigma[sampler_id].resize(ssm_state_size);
				state_mean[sampler_id] = VectorXd::Zero(ssm_state_size);
				ssm->estimateStateSigma(state_sigma[sampler_id], params.pix_sigma[sampler_id]);
			}
		} 
		//! initialize SSM sampler with the first distribution
		ssm->initializeSampler(state_sigma[0], state_mean[0]);

		//! print the mean and sigma for the SSM samplers
		for(int sampler_id = 0; sampler_id < n_samplers; ++sampler_id){
			utils::printMatrix(state_sigma[sampler_id].transpose(),
				cv::format("state_sigma[%d]", sampler_id).c_str(), "%e");
			utils::printMatrix(state_mean[sampler_id].transpose(),
				cv::format("state_mean[%d]", sampler_id).c_str(), "%e");
		}
		am->initializePixVals(ssm->getPts());
		am->initializeSimilarity();
		max_similarity = am->getSimilarity();
		utils::printScalar(max_similarity, "max_similarity");

		initializeParticles();
		initializeSamplers();

		prev_corners = ssm->getCorners();
		ssm->getCorners(cv_corners_mat);
	}

	
	void PF::initializeParticles(){
		double init_wt = 1.0 / params.n_particles;
		for(int particle_id = 0; particle_id < params.n_particles; ++particle_id){
			particle_states[curr_set_id][particle_id] = ssm->getState();
			particle_wts[particle_id] = init_wt;
			if(particle_id > 0){
				particle_cum_wts[particle_id] = particle_wts[particle_id] + particle_cum_wts[particle_id - 1];
			} else{
				particle_cum_wts[particle_id] = particle_wts[particle_id];
			}
			particle_ar[curr_set_id][particle_id].setZero();
		}

	}
	void PF::initializeSamplers(){
		double init_sampler_wt = 1.0 / n_samplers;
		int particles_per_sampler = params.n_particles / n_samplers;
		for(int sampler_id = 0; sampler_id < n_samplers; ++sampler_id){
			sampler_wts[sampler_id] = init_sampler_wt;
			sampler_n_particles[sampler_id] = particles_per_sampler;
		}
		int residual_particles = params.n_particles - n_samplers*particles_per_sampler;
		if(residual_particles >= n_samplers){
			throw std::logic_error(
				cv::format("PF :: Residual particle count: %d exceeds the no. of samplers: %d",
				residual_particles, n_samplers));
		}
		//! distribute the residual particles evenly among the samplers;
		for(int sampler_id = 0; sampler_id < residual_particles; ++sampler_id){
			++sampler_n_particles[sampler_id];
		}
	}
	
	void PF::update(){
		++frame_id;
		if(params.debug_mode){
			utils::printScalarToFile(frame_id, "\n\n-------------------\nframe_id", log_fname, "%d");
		}
		am->setFirstIter();
		int pause_after_show = 1;
		for(int iter_id = 0; iter_id < params.max_iters; iter_id++){
			if(params.show_particles){
				am->getCurrImg().convertTo(curr_img_uchar, CV_8UC1);
			}
			int particle_id = 0;
			double sampler_wt_sum = 0;
			double max_wt = 0;
			for(int sampler_id = 0; sampler_id < n_samplers; ++sampler_id){
				sampler_wts[sampler_id] = 0;
				if(n_samplers>1){
					//! need to reset SSM sampler only if multiple samplers are in use
					ssm->setSampler(state_sigma[sampler_id], state_mean[sampler_id]);
				}				
				for(int sampler_particle_id = 0; sampler_particle_id < sampler_n_particles[sampler_id]; ++sampler_particle_id){
					switch(params.dynamic_model){
					case DynamicModel::AutoRegression1:
						switch(params.update_type){
						case UpdateType::Additive:
							ssm->additiveAutoRegression1(perturbed_state, perturbed_ar,
								particle_states[curr_set_id][particle_id], particle_ar[curr_set_id][particle_id]);
							break;
						case UpdateType::Compositional:
							ssm->compositionalAutoRegression1(perturbed_state, perturbed_ar,
								particle_states[curr_set_id][particle_id], particle_ar[curr_set_id][particle_id]);
							break;
						}
						particle_ar[curr_set_id][particle_id] = perturbed_ar;
						break;
					case DynamicModel::RandomWalk:
						switch(params.update_type){
						case UpdateType::Additive:
							ssm->additiveRandomWalk(perturbed_state, particle_states[curr_set_id][particle_id]);
							break;
						case UpdateType::Compositional:
							ssm->compositionalRandomWalk(perturbed_state, particle_states[curr_set_id][particle_id]);
							break;
						}
						break;
					}
					particle_states[curr_set_id][particle_id] = perturbed_state;

					ssm->setState(particle_states[curr_set_id][particle_id]);
					am->updatePixVals(ssm->getPts());
					am->updateSimilarity(false);

					// a positive number that measures the dissimilarity between the
					// template and the patch corresponding to this particle
					double measuremnt_val = max_similarity - am->getSimilarity();

					// convert this dissimilarity to a likelihood proportional to the dissimilarity
					switch(params.likelihood_func){
					case LikelihoodFunc::AM:
						measurement_likelihood = am->getLikelihood();
						break;
					case LikelihoodFunc::Gaussian:
						measurement_likelihood = measurement_factor * exp(-0.5*measuremnt_val / params.measurement_sigma);
						break;
					case LikelihoodFunc::Reciprocal:
						measurement_likelihood = 1.0 / (1.0 + measuremnt_val);
						break;
					}
					if(params.show_particles){
						cv::Point2d corners[4];
						ssm->getCorners(corners);
						utils::drawCorners(curr_img_uchar, corners,
							cv::Scalar(0, 0, 255), cv::format("%d: %5.3e", particle_id + 1, measurement_likelihood));
						//printf("measurement_likelihood: %e\n", measurement_likelihood);
						if((particle_id + 1) % params.show_particles == 0){
							cv::imshow("Particles", curr_img_uchar);
							int key = cv::waitKey(1 - pause_after_show);
							if(key == 27){
								cv::destroyWindow("Particles");
								params.show_particles = 0;
							} else if(key == 32){
								pause_after_show = 1 - pause_after_show;
							}
							am->getCurrImg().convertTo(curr_img_uchar, CV_8UC1);
						}
					}
					particle_wts[particle_id] = measurement_likelihood;
					particle_cum_wts[particle_id] = particle_id == 0 ? particle_wts[particle_id] :
						particle_wts[particle_id] + particle_cum_wts[particle_id - 1];						
					if(params.update_sampler_wts){
						sampler_wts[sampler_id] += particle_wts[particle_id];
					}
					if(particle_wts[particle_id]>max_wt){
						max_wt = particle_wts[particle_id];
						max_wt_id = particle_id;
					}
					++particle_id;
				}
				if(params.update_sampler_wts){
					sampler_wts[sampler_id] /= static_cast<double>(sampler_n_particles[sampler_id]);
					sampler_wt_sum += sampler_wts[sampler_id];
				}
			}
			if(params.update_sampler_wts){
				int particles_used = 0;
				//! normalize sampler weights
				for(int sampler_id = 0; sampler_id < n_samplers; ++sampler_id){
					sampler_wts[sampler_id] /= sampler_wt_sum;
					sampler_n_particles[sampler_id] = min_particles + sampler_wts[sampler_id] * dynamic_particles;
					particles_used += sampler_n_particles[sampler_id];
				}
				int residual_particles = params.n_particles - particles_used;
				if(residual_particles >= n_samplers){
					throw std::logic_error(
						cv::format("Residual particle count: %d exceeds the no. of samplers: %d",
						residual_particles, n_samplers));
				}
				//! distribute the residual particles evenly among the samplers;
				for(int sampler_id = 0; sampler_id < residual_particles; ++sampler_id){
					++sampler_n_particles[sampler_id];
				}
				if(params.debug_mode){
					utils::printMatrixToFile(particle_wts.transpose(), nullptr, "log/pf_particle_wts.txt", "%e");
					utils::printMatrixToFile(particle_cum_wts.transpose(), nullptr, "log/pf_particle_cum_wts.txt", "%e");
					utils::printMatrixToFile(sampler_wts.transpose(), "sampler_wts", log_fname, "%15.12f");
					utils::printMatrixToFile(sampler_n_particles.transpose(), "sampler_n_particles", log_fname, "%d");
					VectorXf particle_wts_float = particle_wts.cast<float>();
					cv::imshow("Particle Weights", cv::Mat(utils::drawFloatGraph(particle_wts_float.data(), params.n_particles)));
				}
			}
			switch(params.resampling_type){
			case ResamplingType::None:
				break;
			case ResamplingType::BinaryMultinomial:
				binaryMultinomialResampling();
				break;
			case ResamplingType::LinearMultinomial:
				linearMultinomialResampling();
				break;
			case ResamplingType::Residual:
				residualResampling();
				break;
			}
			switch(params.mean_type){
			case MeanType::None:
				//! set the SSM state to that of the highest weighted particle
				ssm->setState(particle_states[curr_set_id][max_wt_id]);
				break;
			case MeanType::SSM:
				ssm->estimateMeanOfSamples(mean_state, particle_states[curr_set_id], params.n_particles);
				ssm->setState(mean_state);
				break;
			case MeanType::Corners:
				updateMeanCorners();
				ssm->setCorners(mean_corners);
			}
			double update_norm = (prev_corners - ssm->getCorners()).squaredNorm();
			prev_corners = ssm->getCorners();
			if(update_norm < params.epsilon){
				if(params.debug_mode){
					printf("n_iters: %d\n", iter_id + 1);
				}
				break;
			}
			am->clearFirstIter();
		}
		if(params.reset_to_mean){
			initializeParticles();
		}
		if(params.update_template){
			am->updateTemplate(ssm->getPts());
			am->initializeSimilarity();
			max_similarity = am->getSimilarity();
		}
		ssm->getCorners(cv_corners_mat);
	}

	// uses binary search to find the particle with the smallest
	// index whose cumulative weight is greater than the provided
	// random number supposedly drawn from a uniform distribution 
	// between 0 and max cumulative weight
	
	void PF::binaryMultinomialResampling(){
		// change the range of the uniform distribution used for resampling instead of normalizing the weights
		//resample_dist.param(ResampleDistParamT(0, particle_cum_wts[params.n_particles - 1]));

		// normalize the cumulative weights and leave the uniform distribution range to (0, 1]
		particle_cum_wts /= particle_cum_wts[params.n_particles - 1];
		if(params.debug_mode){
			utils::printMatrixToFile(particle_cum_wts.transpose(), "normalized particle_cum_wts", log_fname, "%e");
		}
		double max_wt = 0;
		for(int particle_id = 0; particle_id < params.n_particles; ++particle_id){
			double uniform_rand_num = resample_dist(resample_gen);
			int lower_id = 0, upper_id = params.n_particles - 1;
			int resample_id = (lower_id + upper_id) / 2;
			int iter_id = 0;
			while(upper_id > lower_id){
				if(particle_cum_wts[resample_id] >= uniform_rand_num){
					upper_id = resample_id;
				} else{
					lower_id = resample_id + 1;
				}
				resample_id = (lower_id + upper_id) / 2;
				//if(params.debug_mode){
				//	printf("iter_id: %d upper_id: %d lower_id: %d resample_id: %d\n", iter_id, upper_id,
				//		lower_id, resample_id);
				//}
				++iter_id;
			}

			if(params.debug_mode){
				resample_ids[particle_id] = resample_id;
				uniform_rand_nums[particle_id] = uniform_rand_num;
			}

			// place the resampled particle states into the other set so as not to overwrite the current one
			particle_states[1 - curr_set_id][particle_id] = particle_states[curr_set_id][resample_id];
			particle_ar[1 - curr_set_id][particle_id] = particle_ar[curr_set_id][resample_id];
			if(particle_wts[resample_id]>max_wt){
				max_wt = particle_wts[resample_id];
				max_wt_id = particle_id;
			}
		}
		if(params.debug_mode){
			utils::getDiracHist(resample_ids_hist, resample_ids, params.n_particles);
			cv::imshow("Resampled ID Histogram", cv::Mat(utils::drawIntGraph(resample_ids_hist.data(), resample_ids_hist.size())));
			utils::printMatrixToFile(resample_ids.transpose(), "resample_ids", log_fname, "%d");
			utils::printMatrixToFile(uniform_rand_nums.transpose(), "uniform_rand_nums", log_fname, "%e");
		}
		// make the other particle set the current one
		curr_set_id = 1 - curr_set_id;
	}

	
	void PF::linearMultinomialResampling(){
		// change the range of the uniform distribution used for resampling instead of normalizing the weights
		//resample_dist.param(ResampleDistParamT(0, particle_cum_wts[params.n_particles - 1]));

		// normalize the cumulative weights and leave the uniform distribution range to (0, 1]
		particle_cum_wts /= particle_cum_wts[params.n_particles - 1];
		//if(params.debug_mode){
		//	utils::printMatrix(particle_cum_wts.transpose(), "normalized particle_cum_wts");
		//}
		double max_wt = 0;
		for(int particle_id = 0; particle_id < params.n_particles; ++particle_id){
			double uniform_rand_num = resample_dist(resample_gen);
			int resample_id = 0;
			while(particle_cum_wts[resample_id] < uniform_rand_num){ ++resample_id; }

			//if(params.debug_mode){
			//	utils::printScalar(uniform_rand_num, "uniform_rand_num");
			//	utils::printScalar(resample_id, "resample_id", "%d");
			//}

			// place the resampled particle states into the other set so as not to overwrite the current one
			particle_states[1 - curr_set_id][particle_id] = particle_states[curr_set_id][resample_id];
			particle_ar[1 - curr_set_id][particle_id] = particle_ar[curr_set_id][resample_id];
			if(particle_wts[resample_id]>max_wt){ 
				max_wt = particle_wts[resample_id];
				max_wt_id = particle_id;			
			}
		}
		// make the other particle set the current one
		curr_set_id = 1 - curr_set_id;
	}



	
	void PF::residualResampling() {
		// normalize the weights
		particle_wts /= particle_cum_wts[params.n_particles - 1];
		// vector of particle indies
		VectorXi particle_idx = VectorXi::LinSpaced(params.n_particles, 0, params.n_particles - 1);
		//if(params.debug_mode){
		//	utils::printMatrix(particle_wts.transpose(), "normalized particle_wts");
		//	utils::printMatrix(particle_idx.transpose(), "particle_idx", "%d");
		//}
		// sort, with highest weight first
		std::sort(particle_idx.data(), particle_idx.data() + params.n_particles - 1,
			[&](int a, int b){
			return particle_wts[a] > particle_wts[b];
		});
		//if(params.debug_mode){
		//	utils::printMatrix(particle_idx.transpose(), "sorted particle_idx", "%d");
		//}

		// now we append	
		int particles_found = 0;
		for(int particle_id = 0; particle_id < params.n_particles; ++particle_id) {
			int resample_id = particle_idx[particle_id];
			int particle_copies = round(particle_wts[resample_id] * params.n_particles);
			for(int copy_id = 0; copy_id < particle_copies; ++copy_id) {
				particle_states[1 - curr_set_id][particles_found] = particle_states[curr_set_id][resample_id];
				particle_ar[1 - curr_set_id][particles_found] = particle_ar[curr_set_id][resample_id];				
				if(++particles_found == params.n_particles) { break; }
			}
			if(particles_found == params.n_particles) { break; }
		}
		int resample_id = particle_idx[0];
		for(int particle_id = particles_found; particle_id < params.n_particles; ++particle_id) {
			// duplicate particle with highest weight to get exactly same number again
			particle_states[1 - curr_set_id][particle_id] = particle_states[curr_set_id][resample_id];
			particle_ar[1 - curr_set_id][particle_id] = particle_ar[curr_set_id][resample_id];			
		}
		curr_set_id = 1 - curr_set_id;
		max_wt_id=particle_idx[0];
	}

	
	void PF::updateMeanCorners(){
		mean_corners.setZero();
		for(int particle_id = 0; particle_id < params.n_particles; ++particle_id) {
			// compute running average of corners corresponding to the resampled particle states
			ssm->setState(particle_states[curr_set_id][particle_id]);
			mean_corners += (ssm->getCorners() - mean_corners) / (particle_id + 1);
		}
	}
	
	void PF::setRegion(const cv::Mat& corners){
		ssm->setCorners(corners);
		ssm->getCorners(cv_corners_mat);
		initializeParticles();
	}	
}

_MTF_END_NAMESPACE
