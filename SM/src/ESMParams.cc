#include "mtf/SM/ESMParams.h"

_MTF_BEGIN_NAMESPACE

ESMParams::ESMParams(int _max_iters, double _epsilon,
JacType _jac_type, HessType _hess_type, bool _sec_ord_hess,
bool _enable_spi, double _spi_thresh,
bool _debug_mode){
	max_iters = _max_iters;
	epsilon = _epsilon;
	jac_type = _jac_type;
	hess_type = _hess_type;
	sec_ord_hess = _sec_ord_hess;
	enable_spi = _enable_spi;
	spi_thresh = _spi_thresh;
	debug_mode = _debug_mode;
}
// default and copy constructor
ESMParams::ESMParams(const ESMParams *params) :
max_iters(ESM_MAX_ITERS), epsilon(ESM_EPSILON),
jac_type(static_cast<JacType>(ESM_JAC_TYPE)),
hess_type(static_cast<HessType>(ESM_HESS_TYPE)),
sec_ord_hess(ESM_SEC_ORD_HESS),
enable_spi(ESM_ENABLE_SPI), spi_thresh(ESM_SPI_THRESH),
debug_mode(ESM_DEBUG_MODE){
	if(params){
		max_iters = params->max_iters;
		epsilon = params->epsilon;
		jac_type = params->jac_type;
		hess_type = params->hess_type;
		sec_ord_hess = params->sec_ord_hess;
		enable_spi = params->enable_spi;
		spi_thresh = params->spi_thresh;
		debug_mode = params->debug_mode;
	}
}

_MTF_END_NAMESPACE


