var searchData=
[
  ['miparams',['MIParams',['../structMIParams.html#af730e361cb5b5e89ba1361604e519037',1,'MIParams::MIParams(ImgParams *img_params, int _n_bins, double _pre_seed, bool _partition_of_unity, ImageBase *_pix_mapper, bool _debug_mode)'],['../structMIParams.html#a1a7c51b8e71b4cfa7e7ebfaab40951d1',1,'MIParams::MIParams(MIParams *params=nullptr)']]],
  ['mtfnet',['MTFNet',['../classutils_1_1MTFNet.html#a7e0012613cd38f6f0b6494c79f471ace',1,'utils::MTFNet']]]
];
