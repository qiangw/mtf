var searchData=
[
  ['rec_5finit_5ferr_5fgrad',['rec_init_err_grad',['../structHACLKParams.html#a18e6aab09bf131d48b5c7ee013fe26f0',1,'HACLKParams::rec_init_err_grad()'],['../structHESMParams.html#a31e31801829a257b843e260c3068caee',1,'HESMParams::rec_init_err_grad()']]],
  ['reset_5fto_5fmean',['reset_to_mean',['../structPFParams.html#aae13de4248a029114bf17d63938dd944',1,'PFParams']]],
  ['resx',['resx',['../structImgParams.html#aa12bf4a79cb15c878b37e424b52683c3',1,'ImgParams::resx()'],['../classImageBase.html#a4ea85cd1ec4b0655d0de1d452eb0e1ec',1,'ImageBase::resx()']]],
  ['rklt',['RKLT',['../classRKLT.html',1,'']]],
  ['rkltparams',['RKLTParams',['../structRKLTParams.html',1,'']]],
  ['rscv',['RSCV',['../classRSCV.html',1,'']]],
  ['rscvparams',['RSCVParams',['../structRSCVParams.html',1,'RSCVParams'],['../structRSCVParams.html#adcf75922b7abc67b3091662aaebdf6d2',1,'RSCVParams::RSCVParams(ImgParams *img_params, bool _use_bspl, int _n_bins, double _pre_seed, bool _partition_of_unity, bool _weighted_mapping, bool _mapped_gradient, bool _approx_dist_feat, bool _debug_mode)'],['../structRSCVParams.html#a8884ebb5f95a277f39039500cec54045',1,'RSCVParams::RSCVParams(RSCVParams *params=nullptr)']]]
];
