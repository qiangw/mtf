var searchData=
[
  ['max_5fiters',['max_iters',['../structPFParams.html#a247f6354e907eac4019b5f194dc00e53',1,'PFParams']]],
  ['mcccre',['MCCCRE',['../classMCCCRE.html',1,'']]],
  ['mcmi',['MCMI',['../classMCMI.html',1,'']]],
  ['mcncc',['MCNCC',['../classMCNCC.html',1,'']]],
  ['mcrscv',['MCRSCV',['../classMCRSCV.html',1,'']]],
  ['mcscv',['MCSCV',['../classMCSCV.html',1,'']]],
  ['mcspss',['MCSPSS',['../classMCSPSS.html',1,'']]],
  ['mcssd',['MCSSD',['../classMCSSD.html',1,'']]],
  ['mcssim',['MCSSIM',['../classMCSSIM.html',1,'']]],
  ['mczncc',['MCZNCC',['../classMCZNCC.html',1,'']]],
  ['mean_5ftype',['mean_type',['../structPFParams.html#adef741dc9861c4a22ee8543d0b383221',1,'PFParams']]],
  ['mi',['MI',['../classMI.html',1,'']]],
  ['min_5fparticles_5fratio',['min_particles_ratio',['../structPFParams.html#a16a7f4688337858b1ef814f08956afba',1,'PFParams']]],
  ['miparams',['MIParams',['../structMIParams.html',1,'MIParams'],['../structMIParams.html#af730e361cb5b5e89ba1361604e519037',1,'MIParams::MIParams(ImgParams *img_params, int _n_bins, double _pre_seed, bool _partition_of_unity, ImageBase *_pix_mapper, bool _debug_mode)'],['../structMIParams.html#a1a7c51b8e71b4cfa7e7ebfaab40951d1',1,'MIParams::MIParams(MIParams *params=nullptr)']]],
  ['mtfnet',['MTFNet',['../classutils_1_1MTFNet.html#a7e0012613cd38f6f0b6494c79f471ace',1,'utils::MTFNet']]],
  ['mtfnet',['MTFNet',['../classutils_1_1MTFNet.html',1,'utils']]]
];
