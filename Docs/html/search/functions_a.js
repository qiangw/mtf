var searchData=
[
  ['nccparams',['NCCParams',['../structNCCParams.html#a7b7c09d110e795d1ad451f1b7639126e',1,'NCCParams::NCCParams(ImgParams *img_params, bool _fast_hess)'],['../structNCCParams.html#a1a52a2dff2c8d6b7a2acd24aac0e35e2',1,'NCCParams::NCCParams(NCCParams *params=nullptr)']]],
  ['nnparams',['NNParams',['../structmtf_1_1NNParams.html#ad01b60703dd44be2171dae2b45ab116e',1,'mtf::NNParams']]],
  ['nssdparams',['NSSDParams',['../structNSSDParams.html#a6f0af151cd9ea52b873bec03068c9531',1,'NSSDParams::NSSDParams(ImgParams *img_params, double _norm_pix_max, double _norm_pix_min, bool _debug_mode)'],['../structNSSDParams.html#a9835067e4675d572a429adb08e174af7',1,'NSSDParams::NSSDParams(NSSDParams *params=nullptr)']]]
];
