#include "mtf/AM/ZNCC.h"
#include "mtf/Utilities/imgUtils.h"

_MTF_BEGIN_NAMESPACE

ZNCC::ZNCC(const ParamType *ncc_params, const int _n_channels) :
SSDBase(ncc_params, _n_channels) {
	name = "zncc";
	printf("\n");
	printf("Using Zero mean Normalized Cross Correlation AM...\n");
	I0_mean = I0_var = I0_std = 0;
	It_mean = It_var = It_std = 0;
}

void ZNCC::initializePixVals(const Matrix2Xd& init_pts) {
	assert(init_pts.cols() == n_pix);
	if(!isInitialized()->pix_vals){
		I0.resize(patch_size);
		It.resize(patch_size);
	}
	++frame_count;
	switch(n_channels){
	case 1:
		utils::getPixVals(I0, curr_img, init_pts, n_pix,
			img_height, img_width);
		break;
	case 3:
		utils::getPixVals(I0, curr_img_cv, init_pts, n_pix,
			img_height, img_width);
		break;
	default:
		mc_not_implemeted(ZNCC::initializePixVals, n_channels);
	}
	I0_mean = I0.mean();
	I0 = (I0.array() - I0_mean);

	I0_var = I0.squaredNorm() / patch_size;
	I0_std = sqrt(I0_var);
	I0 /= I0_std;

	pix_norm_mult = 1.0 / I0_std;
	pix_norm_add = -I0_mean;

	if(!is_initialized.pix_vals){
		It = I0;
		It_mean = I0_mean;
		It_var = I0_var;
		It_std = I0_std;
		is_initialized.pix_vals = true;
	}
}

void ZNCC::updatePixVals(const Matrix2Xd& curr_pts) {
	assert(curr_pts.cols() == n_pix);
	switch(n_channels){
	case 1:
		utils::getPixVals(It, curr_img, curr_pts, n_pix, img_height, img_width); 
		break;
	case 3:
		utils::getPixVals(It, curr_img_cv, curr_pts, n_pix, img_height, img_width); 
		break;
	default:
		mc_not_implemeted(ZNCC::updatePixVals, n_channels);
	}
	It_mean = It.mean();
	It = (It.array() - It_mean);

	It_var = It.squaredNorm() / patch_size;
	It_std = sqrt(It_var);
	It /= It_std;

	pix_norm_mult = 1.0 / It_std;
	pix_norm_add = -It_mean;
}

_MTF_END_NAMESPACE

