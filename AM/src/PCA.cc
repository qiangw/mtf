#include "mtf/AM/PCA.h"
#include <fstream>      // std::ifstream
#include <iostream>     // std::cout
#include <time.h>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <assert.h>
#include "mtf/Utilities/imgUtils.h"

_MTF_BEGIN_NAMESPACE

//! value constructor
PCAParams::PCAParams(const ImgParams *img_params,
string _basis_data_root_dir,
string _actor,
string _seq_name,
int _extraction_id,
int _n_feat,
int _len_history):
ImgParams(img_params),
basis_data_root_dir(_basis_data_root_dir),
actor(_actor),
seq_name(_seq_name),
extraction_id(_extraction_id),
n_feat(_n_feat),
len_history(_len_history){}

//! default/copy constructor
PCAParams::PCAParams(const PCAParams *params) :
ImgParams(params),
basis_data_root_dir(PCA_BASIS_DATA_ROOT_DIR),
actor(PCA_ACTOR),
seq_name(PCA_SEQ_NAME),
extraction_id(PCA_EXTRACTION_ID),
n_feat(PCA_N_FEAT),
len_history(PCA_LEN_HISTORY)
{
	if(params){
		basis_data_root_dir = params->basis_data_root_dir;
		actor = params->actor;
		seq_name = params->seq_name;
		extraction_id = params->extraction_id;
		n_feat = params->n_feat;
		len_history = params->len_history;
	}
}

PCA::PCA(const ParamType *pca_params) :
SSDBase(pca_params), params(pca_params){
	printf("\nInitializing PCA AM with...\n");
	name = "pca";
	//n_feat = n_pix; // a hack to work around the error with testMTF
	n_feat = params.n_feat;
	len_history = params.len_history;
}

//void PCA::updateDistFeat(){
	//eigen_feat = eigen_patch.transpose() * getCurrPixVals();
	//std::cout << "The eigen feature is " << eigen_feat << std::endl;
//}

//const double* PCA::getDistFeat(){
//	return getCurrFeat().data();
//}

double PCA::getLikelihood() const{
	// Convert to likelihood for particle filters.
	// Normalize over the number of pixels
	double result = exp(-sqrt(-f / static_cast<double>(n_pix)));
	std::cout << "The likelihood at frame " << frame_count << " is " << result << std::endl;
	return result;
}

void PCA::initializeSimilarity() {
	SSDBase::initializeSimilarity();
	// ****** Read basis patches from files
	// Each column is an image
	string in_fname = cv::format("%s/%s/%s_%dx%d_%d.bin",
		params.basis_data_root_dir.c_str(), 
		params.actor.c_str(), params.seq_name.c_str(),
		params.resx, params.resy, params.extraction_id);
	string basis_fname = cv::format("%s/%s/%s_%dx%d_%d_%d_%d_basis.bin",
		params.basis_data_root_dir.c_str(),
		params.actor.c_str(), params.seq_name.c_str(),
		params.resx, params.resy, params.n_feat, params.len_history, params.extraction_id);
	printf("The basis file name: %s\n", basis_fname.c_str());
	printf("The patches file name: %s\n", in_fname.c_str());
	ifstream basis_file;
	basis_file.open(basis_fname, ios::in | ios::binary);
	cout << "Frame id: " << frame_count << endl;
	if (basis_file.good()){
		// First read in the reference patches
		ifstream in_file;
		in_file.open(in_fname, ios::in | ios::binary);
		assert(!in_file.is_open() && "Couldn't open patch file");
		in_file.seekg(0, in_file.end);
		int length = in_file.tellg();
		in_file.seekg(0, in_file.beg);
		int num_elements = (length - sizeof(int)*2) / sizeof(double);
		std::cout << "Reading " << num_elements << " numbers ... " << std::endl;
		// read data as a block:
		int rows, cols;
		in_file.read((char*)(&rows), sizeof(int));
		in_file.read((char*)(&cols), sizeof(int));
		std::cout << "Number of rows: " << rows << std::endl;
		std::cout << "Number of columns/frames: " << cols << std::endl;
		assert((rows == n_pix) && "Reading: number of patch size do not match");
		n_feat = n_feat <= cols ? n_feat : cols;
		extracted_patches.resize(rows, cols);
		in_file.read((char*)(extracted_patches.data()), length-sizeof(int)*2);
		assert(!in_file && "Error reading the patch file");
		in_file.close();
		// if there is a basis file, read the first basis from it
		// Read basis for each frame and store into basis_vec
		printf("PCA: reading basis.\n");
		basis_file.seekg(0, basis_file.end);
		length = basis_file.tellg();
		basis_file.seekg(0, basis_file.beg);
		num_elements = (length-2*sizeof(int)) / sizeof(double);
		printf("PCA: reading basis, number of elements is %d\n", num_elements);
		//int rows, cols;
		basis_file.read((char*)(&rows), sizeof(int));
		basis_file.read((char*)(&cols), sizeof(int));
		printf("PCA: the rows and cols are %d,%d\n", rows, cols);
		printf("PCA: the expected rows & cols are %d,%d\n", n_pix, n_feat);
		if (rows != n_pix || cols != n_feat) {
			cout << "Dimensionality did not match !" <<endl;
			exit(-1);
		}
		assert((rows == n_pix) && "PCA: number of pixels do not match");
		assert((cols == n_feat) && "PCA: number of features do not match");
		int num_frames = num_elements/n_pix/(n_feat+1);
		printf("PCA: reading basis, number of frames is %d\n", num_frames);
		for (int i=0;i<num_frames;i++){
			basis_vec.push_back(MatrixXd(n_pix, n_feat));
			basis_file.read((char*)(basis_vec.back().data()), sizeof(double)*n_pix*n_feat);
			mean_img_vec.push_back(VectorXd(n_pix));
			basis_file.read((char*)(mean_img_vec.back().data()), sizeof(double)*n_pix);
		}
		assert(!basis_file && "Error reading the basis file");
		basis_file.close();
		printf("PCA: mean_img_vec size: %lu\n", mean_img_vec.size());
		printf("PCA: basis_vec size: %lu\n", basis_vec.size());
		printf("PCA: Done reading basis.\n");
		// Show the eigen_patch as image
		VectorXd basis_cv;
		for(int i = 0; i < 0; i++) {
	//	for(int i = 0; i < mean_img_vec.size(); i++) {
			// Construct Mat object for current patch. It points to the memory without copying data
			cv::Mat curr_img_cv = cv::Mat(getResY(), getResX(), CV_64FC1,
				const_cast<double*>(mean_img_vec.at(i).data()));
			// Construct Mat object for the uint8 gray scale patch
			cv::Mat curr_img_cv_uchar(getResY(), getResX(), CV_8UC1);
			// Convert the RGB patch to grayscale
			curr_img_cv.convertTo(curr_img_cv_uchar, curr_img_cv_uchar.type());
			// first need to rescale to [0,255] since basis_vec could be negative
			basis_cv = basis_vec.at(i).col(0);
			basis_cv = basis_cv.array() - basis_cv.minCoeff(); // convert to positive
			basis_cv = basis_cv.array() / basis_cv.maxCoeff() * 255;
			cv::Mat curr_basis_cv = cv::Mat(getResY(), getResX(), CV_64FC1,
				const_cast<double*>(basis_cv.data()));
			// Construct Mat object for the uint8 gray scale patch
			cv::Mat curr_basis_cv_uchar(getResY(), getResX(), CV_8UC1);
			// Convert the RGB patch to grayscale
			curr_basis_cv.convertTo(curr_basis_cv_uchar, curr_basis_cv_uchar.type());
			//imshow("Mean Image", curr_img_cv_uchar);
			//imshow("Basis", curr_basis_cv_uchar);
			if(cv::waitKey(0)%256 == 27)
				break;
		}
	}
	else{ // no basis file, read from patch and write to it
		printf("Basis file not found: %s\n", basis_fname.c_str());
		ofstream out_file;
		out_file.open(basis_fname, ios::out | ios::binary);
		assert(!out_file.is_open() && "Couldn't open basis file to write to");

		printf("Reading patch data from: %s\n", in_fname.c_str());
		ifstream in_file;
		in_file.open(in_fname, ios::in | ios::binary);
		assert(!in_file.is_open() && "Couldn't open patch file");
		in_file.seekg(0, in_file.end);
		int length = in_file.tellg();
		in_file.seekg(0, in_file.beg);
		int num_elements = (length - sizeof(int)*2) / sizeof(double);
		std::cout << "Reading " << num_elements << " numbers ... " << std::endl;
		// read data as a block:
		int rows, cols;
		in_file.read((char*)(&rows), sizeof(int));
		in_file.read((char*)(&cols), sizeof(int));
		std::cout << "Number of columns/frames: " << cols << std::endl;
		assert((rows == n_pix) && "Reading: number of patch size do not match");
		n_feat = n_feat <= cols ? n_feat : cols;
		extracted_patches.resize(rows, cols);
		in_file.read((char*)(extracted_patches.data()), length-sizeof(int)*2);
		assert(!in_file && "Error reading the patch file");
		in_file.close();

		// ****** Compute PCA
		// Mean center the data
		printf("PCA: start of batch computation.\n");
		MatrixXd centered;
		MatrixXd eigen_basis;
		MatrixXd patches;
		out_file.write((char*)(&rows),sizeof(int));
		out_file.write((char*)(&n_feat),sizeof(int));
		for (int i=0; i<cols; i++){
			printf("PCA: iteration %d\n", i);
			if (i<=len_history) {
				patches = extracted_patches.leftCols(len_history);
			}
			else{
				patches = extracted_patches.leftCols(i).rightCols(len_history);
			}
			mean_img = patches.rowwise().mean();
			mean_img_vec.push_back(mean_img);
			centered = patches.colwise() - mean_img;
			// SVD
			JacobiSVD<MatrixXd> svd(centered, ComputeThinU | ComputeThinV);
			eigen_basis = svd.matrixU().leftCols(n_feat);
			basis_vec.push_back(eigen_basis);
			// write into out_file
			out_file.write((char*)(eigen_basis.data()),
				sizeof(double)*eigen_basis.size());
			// write the mean_img
			out_file.write((char*)(mean_img.data()),
				sizeof(double)*mean_img.size());
		}
		out_file.close();
		printf("PCA: mean_img_vec size: %lu\n", mean_img_vec.size());
		printf("PCA: basis_vec size: %lu\n", basis_vec.size());
		printf("PCA: end of batch computation.\n");
	}

/* Code for extracting one global basis for the entire sequence
	if(in_file.is_open()) {
		printf("Now reading the training patches...\n");
		in_file.seekg(0, in_file.end);
		int length = in_file.tellg();
		in_file.seekg(0, in_file.beg);
		int num_elements = length / sizeof(double);
		std::cout << "Reading " << num_elements << " numbers ... " << std::endl;
		int rows = n_pix;
		int cols = num_elements / rows;
		std::cout << "Number of columns: " << cols << std::endl;
		n_feat = n_feat <= cols ? n_feat : cols;
		MatrixXd extracted_patches(rows, cols);
		// read data as a block:
		in_file.read((char*)(extracted_patches.data()), length);
		in_file.close();

		// Print the data in matrix
		//std::cout << "The extracted patches is: " << extracted_patches.col(0)
		//		<< std::endl;

		// ****** Compute PCA
		// Mean center the data
		printf("PCA: start of computation.\n");
		cout << "The first pixel " << extracted_patches.col(0).topRows(2) << endl;
		mean_img = extracted_patches.rowwise().mean();
		MatrixXd centered = extracted_patches.colwise() - mean_img;
		cout << "The centered patch first 10 numbers: " << centered.col(1).topRows(10) << endl;
		//cout << "The mean image: " << centered.topRows(10) << endl;

		// SVD
		JacobiSVD<MatrixXd> svd(centered, ComputeThinU | ComputeThinV);
		eigen_patch = svd.matrixU().leftCols(n_feat);
		cout << "Its singular values are:" << endl << svd.singularValues() << endl;
		cout << "The eigen patch (first row) is: \n" << eigen_patch.row(0) << endl;

		// Show the eigen_patch as image

		//for(int i = 0; i < eigen_patch.cols(); i++) {
		//	// Construct Mat object for current patch. It points to the memory without copying data
		//	cv::Mat curr_img_cv = cv::Mat(getResY(), getResX(), CV_64FC1,
		//		const_cast<double*>(eigen_patch.col(i).data()));
		//	// Construct Mat object for the uint8 gray scale patch
		//	cv::Mat curr_img_cv_uchar(getResY(), getResX(), CV_8UC1);
		//	// Convert the RGB patch to grayscale
		//	curr_img_cv.convertTo(curr_img_cv_uchar, curr_img_cv_uchar.type());
		//	imshow("Patch", curr_img_cv_uchar);
		//	if(cv::waitKey(0) == 27)
		//		break;
		//}

		// Show the condition numbers of basis
		//JacobiSVD<MatrixXd> svd(pcaTransform);
		//double cond = svd.singularValues()(0) / svd.singularValues()(svd.singularValues().size()-1);
		//cout << "Condition number is: " << cond << endl;
		printf("PCA: end of computation.\n");

	} else {
		printf("PCA Could not open binary file.\n");
	}
*/
	mean_img = mean_img_vec.at(frame_count-1);
	eigen_patch = basis_vec.at(frame_count-1);
	VectorXd current_img = getInitPixVals();
	current_img -= mean_img;
	init_feat = current_img;
	init_feat.noalias() -= eigen_patch * (eigen_patch.transpose() * current_img);
	init_norm = init_feat.squaredNorm();
	std::cout << "The init eigen feature is " << init_norm << std::endl;
}


void PCA::updateSimilarity(bool prereq_only/*=true*/) {
	//! Similarity: negative number, the higher the score, the more similar it is
	VectorXd current_img = getCurrPixVals();
	current_img -= mean_img;
	VectorXd template_img = getRefPixVals();
	template_img -= mean_img;
	curr_feat_diff = current_img;
	curr_feat_diff.noalias() -= eigen_patch * (eigen_patch.transpose() * current_img);
	//curr_feat_diff.noalias() -= eigen_patch * (eigen_patch.transpose() * template_img);
	curr_pix_diff = curr_feat_diff;
	//curr_feat_diff.noalias() -= ref_img;
	//curr_feat_diff = curr_feat - init_feat;
	//std::cout << "The current feature diff is " << curr_feat_diff.transpose() << std::endl;
	if(prereq_only){ return; }
	f = -curr_feat_diff.squaredNorm()/2;
	//similarity = similarity / init_norm *50 ;
	std::cout << "The similarity is " << f  << std::endl;
}

const PixValT& PCA::getRefPixVals() {
	return ref_img;
}

void PCA::setFirstIter() {
	first_iter = true;
	frame_count++;
	printf("It's the first particle at frame %d\n", frame_count);
	int current_frame_id = frame_count - 2; // use the ground truth from previous frame as the reference
	//current_frame_id = 0;
	// Update the template
	ref_img = extracted_patches.col(current_frame_id);
	// update mean image and the eigen patch
	mean_img = mean_img_vec.at(frame_count-1);
	eigen_patch = basis_vec.at(frame_count-1);
	// Get mean subtracted template
	VectorXd template_img = ref_img;
	template_img -= mean_img;
	template_img = eigen_patch * (eigen_patch.transpose() * template_img) + mean_img;
	// Visualize the template and the reconstructed template
	cv::Scalar gt_color(0, 255, 0);
	cv::Mat curr_img_cv = cv::Mat(getResY(), getResX(), CV_64FC1,
		const_cast<double*>(ref_img.data()));
	// Construct Mat object for the uint8 gray scale patch
	cv::Mat curr_img_cv_uchar(getResY(), getResX(), CV_8UC1);
	// Convert the RGB patch to grayscale
	curr_img_cv.convertTo(curr_img_cv_uchar, curr_img_cv_uchar.type());
	cv::Mat curr_img_cv_uchar_bigger(5*getResY(), 5*getResX(), CV_8UC1);
	cv::resize(curr_img_cv_uchar, curr_img_cv_uchar_bigger, curr_img_cv_uchar_bigger.size() );
	cv::putText(curr_img_cv_uchar_bigger, "Template", cv::Point2d(5,15),
					cv::FONT_HERSHEY_SIMPLEX, 0.5 , gt_color);
	imshow("Template Image", curr_img_cv_uchar_bigger);
	// first need to rescale to [0,255] since basis_vec could be negative
	cv::Mat curr_recon_cv = cv::Mat(getResY(), getResX(), CV_64FC1,
		const_cast<double*>(template_img.data()));
	// Construct Mat object for the uint8 gray scale patch
	cv::Mat curr_recon_cv_uchar(getResY(), getResX(), CV_8UC1);
	cv::Mat curr_recon_cv_uchar_bigger(5*getResY(), 5*getResX(), CV_8UC1);
	// Convert the RGB patch to grayscale
	curr_recon_cv.convertTo(curr_recon_cv_uchar, curr_recon_cv_uchar.type());
	cv::resize(curr_recon_cv_uchar, curr_recon_cv_uchar_bigger, curr_recon_cv_uchar_bigger.size() );
	cv::putText(curr_recon_cv_uchar_bigger, "Reconstructed Template", cv::Point2d(5,15),
					cv::FONT_HERSHEY_SIMPLEX, 0.5 , gt_color);
	imshow("Reconstructed Template", curr_recon_cv_uchar_bigger);
}

/*
void PCA::initializePixVals(const Matrix2Xd& init_pts) {
// Call the function from the base class
ImageBase::initializePixVals(int_pts);
}*/


/*void ImageBase::initializePixVals(const Matrix2Xd& init_pts){
	if(!isInitialized()->pix_vals){
	init_pix_vals.resize(n_pix);
	curr_pix_vals.resize(n_pix);
	}
	++frame_count;
	utils::getPixVals(init_pix_vals, curr_img, init_pts, n_pix,
	img_height, img_width, pix_norm_mult, pix_norm_add);

	if(!isInitialized()->pix_vals){
	curr_pix_vals = init_pix_vals;
	isInitialized()->pix_vals = true;
	}
	}
	*/
_MTF_END_NAMESPACE

