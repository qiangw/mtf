#include "mtf/AM/SSDBase.h"
#include "mtf/Utilities/miscUtils.h"

_MTF_BEGIN_NAMESPACE

SSDBase::SSDBase(const ImgParams *img_params, const int _n_channels) :
AppearanceModel(img_params, _n_channels),
curr_pix_diff(0, 0){
	pix_norm_mult = 1;
	pix_norm_add = 0;
	if(ilm){ state_size = ilm->getStateSize(); }
}

void SSDBase::initializeSimilarity(){
	if(is_initialized.similarity)
		return;

	df_dI0.resize(patch_size);
	new (&curr_pix_diff) VectorXdM(df_dI0.data(), patch_size);
	curr_pix_diff.fill(0);
	f = 0;
	if(ilm){
		It_orig.resize(patch_size);
		It_orig = It;
		p_am.resize(state_size);
		ilm->initialize(p_am.data(), patch_size);
	}
	is_initialized.similarity = true;
}

void SSDBase::initializeGrad(){
	if(is_initialized.grad)
		return;

	df_dIt.resize(patch_size);
	setCurrGrad(getInitGrad());

	if(ilm){
		df_dpam.resize(state_size);
		df_dg.resize(patch_size);
		df_dg = df_dIt;
		ilm->cmptParamJacobian(df_dpam.data(), df_dg.data(), It_orig.data(), p_am.data());
		ilm->cmptPixJacobian(df_dIt.data(), df_dg.data(), It_orig.data(), p_am.data());
	}
	is_initialized.grad = true;
}
double SSDBase::getLikelihood() const{
	/**
	since SSD can be numerically too large for exponential function to work,
	we take the square root of the per pixel SSD instead;
	it is further normalized by dividing with the range of pixel values to avoid
	very small numbers that may lead to loss of information due to the limited precision
	of floating point operations;
	*/
	return exp(-sqrt(-f / (static_cast<double>(patch_size))));
}

void SSDBase::updateSimilarity(bool prereq_only){
	if(ilm){
		// I_t actually refers to g(I_t, p_am) to keep the illumination model transparent
		// and avoid any unnecessary runtime costs if it is not used
		It_orig = It;
		ilm->apply(It.data(), It_orig.data(), p_am.data());
	}
	curr_pix_diff = It - I0;
	if(prereq_only){ return; }
#ifndef DISABLE_SPI
	if(spi_mask){
		VectorXd masked_err_vec = Map<const VectorXb>(spi_mask, patch_size).select(curr_pix_diff, 0);
		f = -masked_err_vec.squaredNorm() / 2;
	} else{
#endif
		f = -curr_pix_diff.squaredNorm() / 2;
#ifndef DISABLE_SPI
	}
#endif
}
void SSDBase::updateParam(const VectorXd& state_update){
	if(ilm){
		VectorXd p_am_new(ilm->getStateSize());
		utils::printMatrix(state_update, "state_update");
		utils::printMatrix(p_am, "p_am before update");
		ilm->update(p_am_new.data(), p_am.data(), state_update.data());
		p_am = p_am_new;
		utils::printMatrix(p_am_new, "p_am_new");
		utils::printMatrix(p_am, "p_am after update");
	}
}
// curr_grad is same as negative of init_grad
void SSDBase::updateCurrGrad(){
	df_dIt = -df_dI0;
	if(ilm){
		df_dg = df_dIt;
		ilm->cmptPixJacobian(df_dIt.data(), df_dg.data(), It_orig.data(), p_am.data());
	}
}

void SSDBase::cmptInitJacobian(RowVectorXd &df_dp,
	const MatrixXd &dI0_dpssm){
	assert(df_dp.size() == state_size + dI0_dpssm.cols());
	assert(dI0_dpssm.rows() == patch_size);
	if(ilm){
		df_dp.head(dI0_dpssm.cols()).noalias() = df_dI0 * dI0_dpssm;
		df_dp.tail(state_size) = df_dpam;
	} else{
#ifndef DISABLE_SPI
		if(spi_mask){
			getJacobian(df_dp, spi_mask, df_dI0, dI0_dpssm);
		} else{
#endif
			df_dp.noalias() = df_dI0 * dI0_dpssm;
#ifndef DISABLE_SPI
		}
#endif
	}
}
void SSDBase::cmptCurrJacobian(RowVectorXd &df_dp,
	const MatrixXd &dIt_dpssm){
	assert(df_dp.size() == state_size + dIt_dpssm.cols());
	assert(dIt_dpssm.rows() == patch_size);

	if(ilm){
		df_dp.head(dIt_dpssm.cols()).noalias() = df_dIt * dIt_dpssm;
		VectorXd df_dpam(state_size);
		ilm->cmptParamJacobian(df_dp.tail(state_size).data(),
			df_dIt.data(), It_orig.data(), p_am.data());
	} else{
#ifndef DISABLE_SPI
		if(spi_mask){
			getJacobian(df_dp, spi_mask, df_dIt, dIt_dpssm);
		} else{
#endif
			//printf("df_dp: %ld x %ld\n", df_dp.rows(), df_dp.cols());
			//printf("df_dIt: %ld x %ld\n", df_dIt.rows(), df_dIt.cols());
			//printf("dIt_dpssm: %ld x %ld\n", dIt_dpssm.rows(), dIt_dpssm.cols());
			df_dp.noalias() = df_dIt * dIt_dpssm;
#ifndef DISABLE_SPI
		}
#endif
	}
}
void SSDBase::cmptDifferenceOfJacobians(RowVectorXd &diff_of_jacobians,
	const MatrixXd &dI0_dpssm, const MatrixXd &dIt_dpssm){
	assert(diff_of_jacobians.size() == state_size + dIt_dpssm.cols());
	assert(dI0_dpssm.cols() == dIt_dpssm.cols());
	assert(dIt_dpssm.rows() == patch_size && dI0_dpssm.rows() == patch_size);
	if(ilm){

	}
#ifndef DISABLE_SPI
	if(spi_mask){
		getDifferenceOfJacobians(diff_of_jacobians, spi_mask, dI0_dpssm, dIt_dpssm);
	} else{
#endif
		diff_of_jacobians.noalias() = df_dIt * (dI0_dpssm + dIt_dpssm);
#ifndef DISABLE_SPI
	}
#endif
}

void SSDBase::cmptInitHessian(MatrixXd &d2f_dp2,
	const MatrixXd &dI0_dpssm){
	assert(d2f_dp2.rows() == dI0_dpssm.cols() + state_size && d2f_dp2.rows() == d2f_dp2.cols());
	assert(dI0_dpssm.rows() == patch_size);
	if(ilm){
	} else{
#ifndef DISABLE_SPI
		if(spi_mask){
			getHessian(d2f_dp2, spi_mask, dI0_dpssm);
		} else{
#endif
			d2f_dp2.noalias() = -dI0_dpssm.transpose() * dI0_dpssm;
#ifndef DISABLE_SPI
		}
#endif
	}
}
void SSDBase::cmptCurrHessian(MatrixXd &d2f_dp2,
	const MatrixXd &dIt_dpssm){
	assert(d2f_dp2.rows() == dIt_dpssm.cols() + state_size && d2f_dp2.rows() == d2f_dp2.cols());
	assert(dIt_dpssm.rows() == patch_size);
	if(ilm){
		int ssm_state_size = dIt_dpssm.cols();
		Matrix2d d2f_dpam2;
		ilm->cmptParamHessian(d2f_dpam2.data(), nullptr, df_dg.data(), It_orig.data(), p_am.data());
		d2f_dp2.bottomRightCorner(state_size, state_size) = -d2f_dpam2;
		double d2f_dIt2_const = ilm->cmptPixHessian(d2f_dIt2.data(), nullptr, df_dg.data(), It_orig.data(), p_am.data());
		if(d2f_dIt2_const >= 0){
			d2f_dp2.topLeftCorner(ssm_state_size, ssm_state_size).noalias() = -d2f_dIt2_const * dIt_dpssm.transpose() * dIt_dpssm;
		} else{
			d2f_dp2.topLeftCorner(ssm_state_size, ssm_state_size).noalias() = -dIt_dpssm.transpose() * d2f_dIt2 * dIt_dpssm;
		}
		ilm->cmptCrossHessian(d2f_dpam_dIt.data(), nullptr, df_dg.data(), It_orig.data(), p_am.data());
		d2f_dp2.topRightCorner(ssm_state_size, state_size).transpose() =
			d2f_dp2.bottomLeftCorner(state_size, ssm_state_size) = -d2f_dpam_dIt*dIt_dpssm;
	} else{
#ifndef DISABLE_SPI
		if(spi_mask){
			getHessian(d2f_dp2, spi_mask, dIt_dpssm);
		} else{
#endif
			d2f_dp2.noalias() = -dIt_dpssm.transpose() * dIt_dpssm;
#ifndef DISABLE_SPI
		}
#endif
	}
}
//! analogous to cmptDifferenceOfJacobians except for computing the difference between the current and initial Hessians
void SSDBase::cmptSumOfHessians(MatrixXd &sum_of_hessians,
	const MatrixXd &init_pix_jacobian,
	const MatrixXd &curr_pix_jacobian){
#ifndef DISABLE_SPI
	if(spi_mask){
		getSumOfHessians(sum_of_hessians, spi_mask,
			init_pix_jacobian, curr_pix_jacobian);
	} else{
#endif
		sum_of_hessians.noalias() = -(init_pix_jacobian.transpose() * init_pix_jacobian
			+ curr_pix_jacobian.transpose() * curr_pix_jacobian);
#ifndef DISABLE_SPI
	}
#endif
}

void SSDBase::cmptCurrHessian(MatrixXd &d2f_dp2, const MatrixXd &dIt_dpssm,
	const MatrixXd &d2It_dpssm2){
	int ssm_state_size = d2f_dp2.rows();

	assert(d2f_dp2.cols() == ssm_state_size + state_size);
	assert(d2It_dpssm2.rows() == ssm_state_size * ssm_state_size && d2It_dpssm2.cols() == n_channels * n_pix);

	cmptCurrHessian(d2f_dp2, dIt_dpssm);
	int ch_pix_id = 0;
	for(int pix_id = 0; pix_id < n_pix; ++pix_id){
#ifndef DISABLE_SPI
		if(spi_mask && !spi_mask[pix_id]){ continue; }
#endif
		for(int channel_id = 0; channel_id < n_channels; ++channel_id){
			d2f_dp2 += Map<const MatrixXd>(d2It_dpssm2.col(ch_pix_id).data(), ssm_state_size, ssm_state_size) * df_dIt(ch_pix_id);
			++ch_pix_id;
		}
	}
}
void SSDBase::cmptInitHessian(MatrixXd &d2f_dp2, const MatrixXd &dI0_dpssm,
	const MatrixXd &d2I0_dpssm2){
	int ssm_state_size = d2f_dp2.rows();
	assert(d2f_dp2.cols() == ssm_state_size + state_size);
	assert(d2I0_dpssm2.rows() == ssm_state_size * ssm_state_size && d2I0_dpssm2.cols() == n_channels*n_pix);

	cmptInitHessian(d2f_dp2, dI0_dpssm);
	int ch_pix_id = 0;
	for(int pix_id = 0; pix_id < n_pix; pix_id++){
#ifndef DISABLE_SPI
		if(spi_mask && !spi_mask[pix_id]){ continue; }
#endif
		for(int channel_id = 0; channel_id < n_channels; ++channel_id){
			d2f_dp2 += Map<const MatrixXd>(d2I0_dpssm2.col(ch_pix_id).data(), ssm_state_size, ssm_state_size) * df_dI0(ch_pix_id);
			++ch_pix_id;
		}
	}
}

void SSDBase::cmptSumOfHessians(MatrixXd &sum_of_hessians,
	const MatrixXd &dI0_dpssm, const MatrixXd &dIt_dpssm,
	const MatrixXd &d2I0_dpssm2, const MatrixXd &d2It_dpssm2){

	int ssm_state_size = sum_of_hessians.rows();
	assert(sum_of_hessians.cols() == ssm_state_size);
	assert(d2I0_dpssm2.rows() == ssm_state_size * ssm_state_size && d2I0_dpssm2.cols() == n_channels * n_pix);
	assert(d2It_dpssm2.rows() == ssm_state_size * ssm_state_size && d2It_dpssm2.cols() == n_channels * n_pix);

	cmptSumOfHessians(sum_of_hessians, dI0_dpssm, dIt_dpssm);
	int ch_pix_id = 0;
	for(int pix_id = 0; pix_id < n_pix; pix_id++){
#ifndef DISABLE_SPI
		if(spi_mask && !spi_mask[pix_id]){ continue; }
#endif
		for(int channel_id = 0; channel_id < n_channels; ++channel_id){
			sum_of_hessians += df_dI0(pix_id)*
				(Map<const MatrixXd>(d2I0_dpssm2.col(ch_pix_id).data(), ssm_state_size, ssm_state_size)
				+ Map<const MatrixXd>(d2It_dpssm2.col(ch_pix_id).data(), ssm_state_size, ssm_state_size));
			++ch_pix_id;
		}
	}
}


void SSDBase::getJacobian(RowVectorXd &jacobian, const bool *pix_mask,
	const RowVectorXd &df_dI, const MatrixXd &dI_dpssm){
	assert(dI_dpssm.rows() == patch_size && dI_dpssm.rows() == jacobian.size());
	jacobian.setZero();
	int ch_pix_id = 0;
	for(int pix_id = 0; pix_id < n_pix; pix_id++){
		if(!pix_mask[pix_id]){ continue; }
		for(int channel_id = 0; channel_id < n_channels; ++channel_id){
			jacobian += df_dI[ch_pix_id] * dI_dpssm.row(ch_pix_id);
			++ch_pix_id;
		}

	}
}

void SSDBase::getDifferenceOfJacobians(RowVectorXd &diff_of_jacobians, const bool *pix_mask,
	const MatrixXd &dI0_dpssm, const MatrixXd &dIt_dpssm){
	assert(dI0_dpssm.rows() == n_channels * n_pix && dIt_dpssm.rows() == n_channels * n_pix);
	assert(dI0_dpssm.rows() == diff_of_jacobians.size());

	diff_of_jacobians.setZero();
	int ch_pix_id = 0;
	for(int pix_id = 0; pix_id < n_pix; pix_id++){
		if(!pix_mask[pix_id]){ continue; }
		for(int channel_id = 0; channel_id < n_channels; ++channel_id){
			diff_of_jacobians += df_dIt[pix_id] *
				(dI0_dpssm.row(ch_pix_id) + dIt_dpssm.row(ch_pix_id));
			++ch_pix_id;
		}

	}
}

void SSDBase::getHessian(MatrixXd &d2f_dp2, const bool *pix_mask, const MatrixXd &dI_dpssm){
	assert(dI_dpssm.rows() == n_channels * n_pix);

	d2f_dp2.setZero();
	int ch_pix_id = 0;
	for(int pix_id = 0; pix_id < n_pix; pix_id++){
		if(!pix_mask[pix_id]){ continue; }
		for(int channel_id = 0; channel_id < n_channels; ++channel_id){
			d2f_dp2 -= dI_dpssm.row(ch_pix_id).transpose()*dI_dpssm.row(ch_pix_id);
			++ch_pix_id;
		}

	}
}

void SSDBase::getSumOfHessians(MatrixXd &d2f_dp2, const bool *pix_mask,
	const MatrixXd &dI0_dpssm, const MatrixXd &dIt_dpssm){
	assert(dI0_dpssm.rows() == n_channels * n_pix && dIt_dpssm.rows() == n_channels * n_pix);
	assert(d2f_dp2.rows() == d2f_dp2.cols() && d2f_dp2.rows() == dI0_dpssm.cols());

	d2f_dp2.setZero();
	int ch_pix_id = 0;
	for(int pix_id = 0; pix_id < n_pix; pix_id++){
		if(!pix_mask[pix_id]){ continue; }
		for(int channel_id = 0; channel_id < n_channels; ++channel_id){
			d2f_dp2 -= dI0_dpssm.row(ch_pix_id).transpose()*dI0_dpssm.row(ch_pix_id)
				+ dIt_dpssm.row(ch_pix_id).transpose()*dIt_dpssm.row(ch_pix_id);
			++ch_pix_id;
		}
	}
}

/**
* Squared Euclidean distance functor, optimized version
*/
/**
*  Compute the squared Euclidean distance between two vectors.
*
*	This is highly optimized, with loop unrolling, as it is one
*	of the most expensive inner loops.
*
*	The computation of squared root at the end is omitted for
*	efficiency.
*/
double SSDBase::operator()(const double* a, const double* b,
	size_t size, double worst_dist) const{
	double result = double();
	double diff0, diff1, diff2, diff3;
	const double* last = a + size;
	const double* lastgroup = last - 3;

	/* Process 4 items with each loop for efficiency. */
	while(a < lastgroup){
		diff0 = (double)(a[0] - b[0]);
		diff1 = (double)(a[1] - b[1]);
		diff2 = (double)(a[2] - b[2]);
		diff3 = (double)(a[3] - b[3]);
		result += diff0 * diff0 + diff1 * diff1 + diff2 * diff2 + diff3 * diff3;
		a += 4;
		b += 4;

		if((worst_dist > 0) && (result > worst_dist)){
			return result;
		}
	}
	/* Process last 0-3 pixels.  Not needed for standard vector lengths. */
	while(a < last){
		diff0 = (double)(*a++ - *b++);
		result += diff0 * diff0;
	}
	return result;
}

_MTF_END_NAMESPACE

