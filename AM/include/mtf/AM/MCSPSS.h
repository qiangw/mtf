#ifndef MTF_MC_SPSS_H
#define MTF_MC_SPSS_H

#include "SPSS.h"

_MTF_BEGIN_NAMESPACE

// Multi Channel Normalized Cross Correlation
class MCSPSS : public SPSS{
public:
	MCSPSS(const ParamType *spss_params);
};

_MTF_END_NAMESPACE

#endif