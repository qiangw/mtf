#ifndef MTF_PCA_H
#define MTF_PCA_H

#include "SSDBase.h"

// Default parameters
#define PCA_BASIS_DATA_ROOT_DIR "Datasets"
#define PCA_ACTOR "TMT"
#define PCA_SEQ_NAME "nl_mugII_s3"
#define PCA_EXTRACTION_ID 0
#define PCA_N_FEAT 20
#define PCA_LEN_HISTORY 25

_MTF_BEGIN_NAMESPACE
struct PCAParams : ImgParams{
	//! The root directory of the basis
	string basis_data_root_dir;
	//! The name of the dataset
	string actor;
	//! The name of the source image sequence
	string seq_name;
	//! The id identifying the bin file
	int extraction_id;
	//! number of features in the image patch (number of eigen basis)
	int n_feat;
	//! number of frames in the history to keep
	int len_history;
	//! value constructor
	PCAParams(const ImgParams *img_params,
		string _basis_data_root_dir,
		string _actor,
		string _seq_name,
		int _extraction_id,
		int _n_feat,
		int _len_history);
	//! default/copy constructor
	PCAParams(const PCAParams *params = nullptr);
};

class PCA : public SSDBase{
public:

	typedef PCAParams ParamType;
	const ParamType params;

	// Define feature vectors
	VectorXd eigen_feat;  // eigen coefficients/features
	int n_feat; // number of features in the image patch (number of eigen basis)
	int len_history;//! number of frames in the history to keep
	VectorXd init_feat, curr_feat; //! features in the object patch being tracked (flattened as a vector)
	VectorXd curr_feat_diff;
	double init_norm;
	MatrixXd eigen_patch; // eigen template
	MatrixXd extracted_patches; // reference image at all frames, each column is an image
	VectorXd ref_img;
	vector<MatrixXd> basis_vec;
	VectorXd mean_img;
	vector<VectorXd> mean_img_vec;
	//bool first_particle = false; // for PF SM method, to identify the first particle

	PCA(const ParamType *pca_params);
	PCA() : SSDBase(){}

	// Returns a normalized version of the similarity that lies between 0 and 1
	double getLikelihood() const override;

	// Initialize the features
	void initializeSimilarity() override;

	// Update the similarity
	void updateSimilarity(bool prereq_only=true) override;

	// Defined in Appearance Model,
	// It is called before performing the first iteration on a new image
	// to indicate that the image has changed since the last time the
	// update functions were called
	virtual void setFirstIter() override;

	// Grab the reference patch from the extracted patches
	const PixValT& getRefPixVals();
	// A list of methods that is not required for now

	//void initializePixVals(const Matrix2Xd& init_pts) override;
	//void updatePixVals(const Matrix2Xd& curr_pts) override;

	//int getDistFeatSize() override{ return n_feat; }

	// Returns the feature vector as VectorXd
	//const VectorXd& getCurrFeat(){ return eigen_feat; }

	// computes a "distance" vector using the current image patch
	//void updateDistFeat() override;
	//using SSDBase ::updateDistFeat;

	//void initializeDistFeat() override{ n_feat = eigen_patch.cols(); }// initialize the number of features

	// Returns a pointer to the feature vector
	//const double* getDistFeat() override;

};

_MTF_END_NAMESPACE

#endif
