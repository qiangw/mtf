#ifndef MTF_NCC_H
#define MTF_NCC_H

#include "AppearanceModel.h"

#define NCC_FAST_HESS 0

_MTF_BEGIN_NAMESPACE
struct NCCParams : ImgParams{
	bool fast_hess; 
	//! value constructor
	NCCParams(const ImgParams *img_params,
		bool _fast_hess);
	//! default/copy constructor
	NCCParams(const NCCParams *params = nullptr);
};
// Normalized Cross Correlation
class NCC : public AppearanceModel{
public:
	typedef NCCParams ParamType;
	const ParamType params;

	NCC(const ParamType *ncc_params, const int _n_channels = 1);

	double getLikelihood() const override{
		double d = (1.0 / f) - 1;
		return exp(-d*d);
	}
	//-------------------------------initialize functions------------------------------------//
	void initializeSimilarity() override;
	void initializeGrad() override;
	void initializeHess() override {}

	//-------------------------------update functions------------------------------------//
	void updateSimilarity(bool prereq_only = true) override;
	void updateInitGrad() override;
	// nothing is done here since curr_grad is same as and shares memory with  curr_pix_diff
	void updateCurrGrad() override;

	void cmptInitHessian(MatrixXd &init_hessian, const MatrixXd &init_pix_jacobian) override;
	void cmptInitHessian(MatrixXd &init_hessian, const MatrixXd &init_pix_jacobian,
		const MatrixXd &init_pix_hessian) override;

	void cmptCurrHessian(MatrixXd &curr_hessian, const MatrixXd &curr_pix_jacobian) override;
	void cmptCurrHessian(MatrixXd &curr_hessian, const MatrixXd &curr_pix_jacobian,
		const MatrixXd &curr_pix_hessian) override;

	void cmptSelfHessian(MatrixXd &self_hessian, const MatrixXd &curr_pix_jacobian) override;
	void cmptSelfHessian(MatrixXd &self_hessian, const MatrixXd &curr_pix_jacobian,
		const MatrixXd &curr_pix_hessian) override{
		cmptSelfHessian(self_hessian, curr_pix_jacobian);
	}

	/*Support for FLANN library*/
	VectorXd curr_feat_vec;
	NCC() : AppearanceModel(){}
	typedef bool is_kdtree_distance;
	typedef double ElementType;
	typedef double ResultType;
	double operator()(const double* a, const double* b, 
		size_t size, double worst_dist = -1) const override;
	double accum_dist(const double& a, const double& b, int) const{	return -a*b;}
	void updateDistFeat(double* feat_addr) override;
	void updateDistFeat() override{
		updateDistFeat(curr_feat_vec.data());
	}
	int  getDistFeatSize() override{ return patch_size; }
	void initializeDistFeat() override{
		curr_feat_vec.resize(getDistFeatSize());
	}
	const double* getDistFeat() override{ return curr_feat_vec.data(); }

private:

	//! mean of the initial and current pixel values
	double init_pix_mean, curr_pix_mean;
	double a, b, c;
	double bc, b2c;

	VectorXd init_pix_vals_cntr, curr_pix_vals_cntr;
	VectorXd init_pix_vals_cntr_c, curr_pix_vals_cntr_b;
	VectorXd init_grad_ncntr, curr_grad_ncntr;
	double init_grad_ncntr_mean, curr_grad_ncntr_mean;

};

_MTF_END_NAMESPACE

#endif