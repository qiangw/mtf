#ifndef MTF_APPEARANCE_MODEL_H
#define MTF_APPEARANCE_MODEL_H

#define am_not_implemeted(func_name) \
	throw std::domain_error(cv::format("%s :: %s :: Not implemented Yet", name.c_str(), #func_name))

#include "ImageBase.h"
#include <stdexcept>


_MTF_BEGIN_NAMESPACE

/**
set of indicator variables to keep track of which dependent state variables need updating and executing
the update expression for any variable only when at least one of the other variables it depends on has
been updated; this can help to increase speed by avoiding repeated computations
*/
struct AMStatus : public ImgStatus{
	bool similarity, grad, hess;

	AMStatus() : ImgStatus(){ clear(); }

	void set(){
		setPixState();
		similarity = grad = hess = true;
	}
	void clear(){
		clearPixState();
		similarity = grad = hess = false;
	}
};
/**
Similarity function that indicates how well a candidate warped patch matches the template.
Registration based tracking is expressed as the solution to the following optimization problem:
p_t = argmax(p_ssm, p_am) f(I_0(x), I_t(w(x, p_ssm)), p_am)
where p_ssm and p_am are respectively the geometric and photometric parameters to be optimized.
Appearance Model corresponds to the similarity function f.
*/
class AppearanceModel : public ImageBase{
protected:
	/**
	f(I_0, I_t, p_am): R(N) x R(N) x R(K) -> R
	measures the similarity between the current (I_t) and
	initial (I_0) patches using the photometric parameters (p_am)
	this is the quantity to be maximized by the optimization process
	*/
	double f;

	/**
	1 x N gradients of the similarity
	w.r.t. initial and current pixel values;
	*/
	RowVectorXd df_dI0, df_dIt;

	/** parameters of the photomtric model */
	VectorXd p_am;

	/** size of p_am, i.e. number of parameters in the similarity function */
	int state_size;

	/** 1 x K Jacobian of the similarity function w.r.t. its parameters */
	RowVectorXd df_dpam;

	/** K x K Hessian of the similarity w.r.t. its parameters */
	MatrixXd d2f_dpam2;

	/**
	1 x N gradient of the similarity w.r.t. illumination model
	if such a one is used: f->f(I_0, g(I_t, p_am))
	*/
	RowVectorXd df_dg;

	/**
	K x N cross Hessian of the similarity
	w.r.t. photomtric parameters p_am and the current patch  I_t
	assuming again that an illumination model is in use;
	*/
	MatrixXd d2f_dpam_dIt;

	/**
	optional pointer to an illumination model
	if such a one is to be used as a photometric function
	*/
	IlluminationModel *ilm;

	/**
	these NxN Hessians of the similarity wrt pixel values are usually not stored or computed explicitly because:
	1. the matrices are just too large for higher sampling resolutions
	2. they are often very sparse so allocating so much space is wasteful
	3. computing the Hessian wrt SSM parameters by multiplying this matrix with the SSM Hessian is highly inefficient is highly inefficient
	*/
	MatrixXd d2f_dI02, d2f_dIt2;

	/**
	indicator variable that can be set by iterative search methods to indicate if the initial or first iteration is being run on the current image;
	can be used to perform some costly operations/updates only once per frame rather than at every iteration
	*/
	bool first_iter;

	/**
	pixels corresponding to false entries in the mask will be ignored in all respective computations where pixel values are used;
	it is up to the AM to do this in a way that makes sense; since none of the state variables are actually being resized they will
	still have entries corresponding to these ignored pixels but the AM s at liberty to put anything there assuming
	that the SM will not use these entries in its own computations; this is why all of these have default implementations that simply ignore the mask;
	these can be used by the AM when the non masked entries of the computed variable do not depend on the masked pixels;
	*/
	const bool *spi_mask;

	/**
	indicator variables used to keep track of which state variables have been initialized;
	*/
	AMStatus is_initialized;

public:
	/** name of the appearance model */
	string name;

	/** default and value constructor */
	explicit AppearanceModel(const ImgParams *img_params = nullptr, const int _n_channels = 1) :
		ImageBase(img_params, _n_channels), f(0), state_size(0),
		ilm(nullptr), first_iter(false), spi_mask(nullptr){
		if(img_params){ ilm = img_params->ilm; }
	}
	/** destructor */
	virtual ~AppearanceModel(){}

	/** accessor methods */
	virtual int getStateSize() const{ return 0; }
	virtual double getSimilarity() const{ return f; }
	/**
	returns a normalized version of the similarity that lies between 0 and 1
	and can be interpreted as the likelihood that the current patch represents 
	the same object as the initial patch
	*/
	virtual double getLikelihood() const{ am_not_implemeted(getLikelihood); }
	virtual const RowVectorXd& getInitGrad() const{ return df_dI0; }
	virtual const RowVectorXd& getCurrGrad() const{ return df_dIt; }

	/** modifier methods */
	virtual void setSimilarity(double _similarity){ f = _similarity; }
	virtual void setInitGrad(const RowVectorXd &_init_grad){ df_dI0 = _init_grad; }
	virtual void setCurrGrad(const RowVectorXd &_curr_grad){ df_dIt = _curr_grad; }


	/**
	methods to initialize the state variables - to be called once when the tracker is initialized.
	if any of these are reimplemented, there should be a statement there copying the computed value in the "init"
	variable to the corresponding "curr" variable so the two have the same value after this function is called;

	Note for the SM: the "initialize" function for any state variable whose "update" function will be called later should be called
	once from the SM's own initialize function even if the initial value of that variable will not be used later;
	this is because updating the state variable may involve some computations which need to be performed only once and the AM is free to delegate
	any such computations to the respective "initialize" function to avoid repeating them in the "update" function which can have a negative impact on performance;
	thus if this function is not called, the results of these computations that are needed by the "update" function will remain uncomputed leading to undefined behavior;

	also the initialize functions have a boolean indicator parameter called "is_initialized" which defaults to true but should be set to false
	if they are called only to update the internal state to take into account any changes to the initial template, i.e. if they are called
	again after the first call to initialize the state (when it should be left to true)
	*/
	virtual void initializeSimilarity(){ am_not_implemeted(initializeSimilarity); }
	virtual void initializeGrad() { am_not_implemeted(initializeGrad); }
	/**
	// even though the Hessian of the error norm w.r.t. pixel values is not a state variable (since it does not need
	// to be computed separately to get the Hessian w.r.t SSM), this function is provided as a place to perform
	// any one-time computations that may help to decrease the runtime cost of the interfacing function that computes this Hessian
	*/
	virtual void initializeHess(){ am_not_implemeted(initializeHess); }

	/**
	functions for updating state variables when a new image arrives
	*/
	/**
	prereq_only should be left to true if update is only called to compute the prerequisites
	for the two gradient functions and the actual value of similarity is not needed
	*/
	virtual void updateSimilarity(bool prereq_only = true){ am_not_implemeted(updateSimilarity); }
	virtual void updateParam(const VectorXd& state_update){}
	virtual void updateInitGrad(){ am_not_implemeted(updateInitGrad); }
	virtual void updateCurrGrad() { am_not_implemeted(updateCurrGrad); }
	virtual void updateParamGrad() { am_not_implemeted(updateParamGrad); }

	//virtual void updateInitHess() { throw std::domain_error("updateInitHess :: Not implemented Yet"); }
	//virtual void updateCurrHess() { throw std::domain_error("updateCurrHess :: Not implemented Yet"); }

	/**
	// ----- interfacing functions that take pixel jacobians/Hessians w.r.t. SSM parameters and combine them with AM jacobians/Hessians w.r.t. pixel values ---- //
	*/
	/** to produce corresponding jacobians w.r.t. SSM parameters */
	virtual void cmptInitJacobian(RowVectorXd &init_similarity_jacobian, const MatrixXd &init_pix_jacobian){
		assert(init_pix_jacobian.rows() == n_pix && init_pix_jacobian.cols() == init_similarity_jacobian.cols());
		init_similarity_jacobian.noalias() = df_dI0 * init_pix_jacobian;
	}
	virtual void cmptCurrJacobian(RowVectorXd &similarity_jacobian, const MatrixXd &curr_pix_jacobian){
		assert(curr_pix_jacobian.rows() == n_pix && curr_pix_jacobian.cols() == similarity_jacobian.cols());
		similarity_jacobian.noalias() = df_dIt * curr_pix_jacobian;
	}
	/**
	multiplies the gradients of the current error norm w.r.t. current and initial pixel values with the respective pixel jacobians and computes
	the difference between the resultant jacobians of the current error norm  w.r.t. external (e.g. SSM) parameters
	though this can be done by the search method itself, a dedicated method is provided to take advantage of any special constraints to
	speed up the computations; if mean of the two pixel jacobians is involved in this computation, it is stored in the provided matrix to
	avoid recomputing it while computing the error norm hessian
	*/
	virtual void cmptDifferenceOfJacobians(RowVectorXd &diff_of_jacobians,
		const MatrixXd &init_pix_jacobian, const MatrixXd &curr_pix_jacobian){
		diff_of_jacobians.noalias() = (df_dIt * curr_pix_jacobian) - (df_dI0 * init_pix_jacobian);
	}

	/**
	compute the S x S Hessian of the error norm using the supplied N x S Jacobian of pixel values w.r.t. external parameters;
	compute the approximate Hessian by ignoring the second order terms
	*/
	virtual void cmptInitHessian(MatrixXd &init_hessian, const MatrixXd &init_pix_jacobian){
		am_not_implemeted(cmptInitHessian(first order));
	}
	virtual void cmptCurrHessian(MatrixXd &curr_hessian, const MatrixXd &curr_pix_jacobian){
		am_not_implemeted(cmptCurrHessian(first order));
	}
	virtual void cmptSelfHessian(MatrixXd &self_hessian, const MatrixXd &curr_pix_jacobian) {
		am_not_implemeted(cmptSelfHessian(first order));
	}

	/** compute the exact Hessian by considering the second order terms too */
	virtual void cmptInitHessian(MatrixXd &init_hessian, const MatrixXd &init_pix_jacobian, const MatrixXd &init_pix_hessian){
		am_not_implemeted(cmptInitHessian(second order));
	}
	virtual void cmptCurrHessian(MatrixXd &curr_hessian, const MatrixXd &curr_pix_jacobian,
		const MatrixXd &curr_pix_hessian){
		am_not_implemeted(cmptCurrHessian(second order));
	}
	virtual void cmptSelfHessian(MatrixXd &self_hessian, const MatrixXd &curr_pix_jacobian,
		const MatrixXd &curr_pix_hessian) {
		am_not_implemeted(cmptSelfHessian(second order));
	}

	/** analogous to cmptDifferenceOfJacobians except for computing the mean of the current and initial Hessians */
	virtual void cmptSumOfHessians(MatrixXd &sum_of_hessians,
		const MatrixXd &init_pix_jacobian, const MatrixXd &curr_pix_jacobian){
		int ssm_state_size = curr_pix_jacobian.cols();
		assert(sum_of_hessians.rows() == ssm_state_size && sum_of_hessians.cols() == ssm_state_size);
		MatrixXd init_hessian(ssm_state_size, ssm_state_size);
		cmptInitHessian(init_hessian, init_pix_jacobian);

		MatrixXd curr_hessian(ssm_state_size, ssm_state_size);
		cmptCurrHessian(curr_hessian, curr_pix_jacobian);

		sum_of_hessians = init_hessian + curr_hessian;
	}
	virtual void cmptSumOfHessians(MatrixXd &sum_of_hessians,
		const MatrixXd &init_pix_jacobian, const MatrixXd &curr_pix_jacobian,
		const MatrixXd &init_pix_hessian, const MatrixXd &curr_pix_hessian){
		int ssm_state_size = curr_pix_jacobian.cols();
		assert(sum_of_hessians.rows() == ssm_state_size && sum_of_hessians.cols() == ssm_state_size);
		MatrixXd init_hessian(ssm_state_size, ssm_state_size);
		cmptInitHessian(init_hessian, init_pix_jacobian, init_pix_hessian);

		MatrixXd curr_hessian(ssm_state_size, ssm_state_size);
		cmptCurrHessian(curr_hessian, curr_pix_jacobian, curr_pix_hessian);

		sum_of_hessians = init_hessian + curr_hessian;
	}

	virtual void setSPIMask(const bool *_spi_mask){ spi_mask = _spi_mask; }
	virtual const bool* getSPIMask() const{ return spi_mask; }
	virtual void clearSPIMask(){ spi_mask = nullptr; }

	/**
	should be overridden by an implementing class once it implements SPI functionality
	for all functions where it makes logical sense
	*/
	virtual bool supportsSPI() const { return false; }

	/**
	should be called before performing the first iteration on a new image to indicate that the image
	has changed since the last time the update funcvtions were called
	*/
	virtual void setFirstIter(){ first_iter = true; }
	/** should be called after the first iteration on a new frame is done */
	virtual void clearFirstIter(){ first_iter = false; }

	ImgStatus* isInitialized() override { return &is_initialized; }
	virtual void setInitStatus(){ is_initialized.set(); }
	virtual void clearInitStatus(){ is_initialized.clear(); }

	/**
	return false if the similarity function f is not symmetrical, i.e. f(a,b) != f(b, a);
	also applies to the distance functor so the two should be consistent;
	*/
	virtual bool isSymmetrical() const{ return true; }

	// ------------------------ Distance Feature ------------------------ //

	typedef double ElementType;
	typedef double ResultType;

	/**
	computes the distance / dissimilarity between two patches where each is codified or represented
	by a suitable distance feature computed using updateDistFeat
	*/
	virtual double operator()(const double* a, const double* b,
		size_t dist_size, double worst_dist = -1) const {
		am_not_implemeted(distance functor);
	}

	/** to be called once during initialization if any of the distance feature functionality is to be used */
	virtual void initializeDistFeat() {
		am_not_implemeted(initializeDistFeat);
	}
	/**
	 computes a "distance" vector using the current image patch such that,
	 when the distance vectors corresponding to two patches are passed to the distance operator above,
	 it uses these to compute a scalar that measures the distance or dissimilarity between the two patches;
	 this distance vector should be designed so as to offload as much computation as possible from the
	 distance operator, i.e. every computation that depends only on the current patch should be performed here
	 and the results should be stored, suitably coded, in the distannce vector
	 where they will be decoded and used to compute the distance measure
	 */
	virtual void updateDistFeat() {
		am_not_implemeted(updateDistFeat);
	}
	virtual const double* getDistFeat(){
		am_not_implemeted(getDistFeat);
	}
	/**
	overloaded version to write the distance feature directly to a row (or column) of a matrix
	storing the distance features corresponding to several patches;
	*/
	virtual void updateDistFeat(double* feat_addr) {
		am_not_implemeted(updateDistFeat);
	}
	/** returns the size of the distance vector */
	virtual int getDistFeatSize() {
		am_not_implemeted(getDistFeatSize);
	}
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

_MTF_END_NAMESPACE

#endif



