#ifndef MTF_NSSD_H
#define MTF_NSSD_H

#include "SSDBase.h"

#define DEF_NORM_MAX 1.0
#define DEF_NORM_MIN 0.0
#define DEF_DEBUG false

_MTF_BEGIN_NAMESPACE

struct NSSDParams : ImgParams{
	//! minimum pix value after normalization
	double norm_pix_min;
	//! maximum pix value after normalization
	double norm_pix_max;
	//! decides whether logging data will be printed for debugging purposes; 
	//! only matters if logging is enabled at compile time
	bool debug_mode;

	//! value constructor
	NSSDParams(const ImgParams *img_params,
		double _norm_pix_max, double _norm_pix_min,
		bool _debug_mode);
	//! default/copy constructor
	NSSDParams(const NSSDParams *params = nullptr);
};
class NSSD : public SSDBase{
public:

	typedef NSSDParams ParamType; 

	const ParamType params;
	NSSD(const ParamType *nssd_params);
};

_MTF_END_NAMESPACE

#endif