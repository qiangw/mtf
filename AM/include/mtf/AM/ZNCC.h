#ifndef MTF_ZNCC_H
#define MTF_ZNCC_H

#include "SSDBase.h"

_MTF_BEGIN_NAMESPACE

// Zero mean Normalized Cross Correlation
class ZNCC : public SSDBase{
public:

	typedef ImgParams ParamType; 

	//! mean, variance and standard deviation of the initial pixel values
	double I0_mean, I0_var, I0_std;
	//! mean, variance and standard deviation of the current pixel values
	double It_mean, It_var, It_std;

	ZNCC(const ParamType *ncc_params, const int _n_channels = 1);
	void initializePixVals(const Matrix2Xd& curr_pts) override;
	void updatePixVals(const Matrix2Xd& curr_pts) override;
};

_MTF_END_NAMESPACE

#endif