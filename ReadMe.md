Welcome to the home of **Modular Tracking Framework (MTF)** - a highly efficient and extensible library for
[**registration based tracking**](https://en.wikipedia.org/wiki/Kanade%E2%80%93Lucas%E2%80%93Tomasi_feature_tracker)
that utilizes a modular decomposition of trackers in this domain.
Each tracker within this framework comprises the following 3 modules:

1. **Search Method (SM)**: [ESM](http://far.in.tum.de/pub/benhimane2007ijcv/benhimane2007ijcv.pdf), [IC](http://ieeexplore.ieee.org/xpl/login.jsp?tp=&arnumber=990652&url=http%3A%2F%2Fieeexplore.ieee.org%2Fiel5%2F7768%2F21353%2F00990652.pdf%3Farnumber%3D990652), [IA](http://dl.acm.org/citation.cfm?id=290123), [FC](http://link.springer.com/article/10.1023%2FA%3A1008195814169), [FA](http://dl.acm.org/citation.cfm?id=1623280), [NN](http://www.comp.nus.edu.sg/~haoyu/rss/rss09/p44.html), [PF](http://cv.snu.ac.kr/jhkwon/PAMI14_tracking/index.htm) or [RANSAC](http://ieeexplore.ieee.org/xpl/login.jsp?tp=&arnumber=7158323&url=http%3A%2F%2Fieeexplore.ieee.org%2Fiel7%2F7158225%2F7158304%2F07158323.pdf%3Farnumber%3D7158323)
2. **Appearance Model (AM)**: [SSD](http://dl.acm.org/citation.cfm?id=290123), ZNCC, [SCV](http://ieeexplore.ieee.org/xpl/login.jsp?tp=&arnumber=6094650&url=http%3A%2F%2Fieeexplore.ieee.org%2Fxpls%2Fabs_all.jsp%3Farnumber%3D6094650), [NCC](http://link.springer.com/chapter/10.1007%2F978-3-642-33783-3_32), [MI](https://www.irisa.fr/lagadic/pdf/2010_ismar_dame.pdf), [CCRE](http://link.springer.com/article/10.1007%2Fs11263-006-0011-2), [KLD](https://en.wikipedia.org/wiki/Kullback%E2%80%93Leibler_divergence), [SSIM](http://link.springer.com/article/10.1007/s00138-007-0107-x), SPSS, [FMaps](http://webdocs.cs.ualberta.ca/~mennatul/?p=25) or [PCA](http://link.springer.com/article/10.1007/s11263-007-0075-7)
3. **State Space Model (SSM)**: [Spline](http://www.hpl.hp.com/techreports/Compaq-DEC/CRL-94-1.html) (50+ dof), Homography (8 dof), Affine (6 dof), Similitude(4 dof), Isometery (3 dof) or Translation (2 dof)

Please refer
[this paper](http://webdocs.cs.ualberta.ca/~asingh1/docs/Modular%20Tracking%20Framework%20A%20Uni%EF%AC%81ed%20Approach%20to%20Registration%20based%20Tracking%20(CRV%202016).pdf)
for more details on the system design and
[this one](http://webdocs.cs.ualberta.ca/~asingh1/docs/Modular%20Decomposition%20and%20Analysis%20of%20Registration%20based%20Trackers%20(CRV%202016).pdf)
for some preliminary results. There is also an [**official website**](http://webdocs.cs.ualberta.ca/~vis/mtf/) where Doxygen documentation will soon be available along with detailed tutorials and examples. It also provides several datasets formatted to work with MTF.

The library is implemented entirely in C++ though a Python interface called `pyMTF` also exists and works seamlessly with our [Python Tracking Framework](https://bitbucket.org/abhineet123/ptf). 
A Matlab interface similar to [Mexvision](http://ugweb.cs.ualberta.ca/~vis/courses/CompVis/lab/mexVision/) is currently under development too.

We also provide a simple interface for [ROS](http://www.ros.org/) called [mtf_bridge](https://gitlab.com/vis/mtf_bridge) for seamless integration with robotics applications.

Installation:
-------------
* Prerequisites:
    *  MTF uses some [C++11](https://en.wikipedia.org/wiki/C%2B%2B11) features so a supporting compiler is needed ([GCC 4.7](https://gcc.gnu.org/projects/cxx0x.html) or newer)
    * [Eigen](http://eigen.tuxfamily.org/index.php?title=Main_Page) should be installed and added to the C/C++ include paths. This can be done, for instance, by running `echo "export C_INCLUDE_PATH=$C_INCLUDE_PATH:/usr/include/eigen3" >> ~/.bashrc` and `echo "export CPLUS_INCLUDE_PATH=$CPLUS_INCLUDE_PATH:/usr/include/eigen3" >> ~/.bashrc` assuming that Eigen is installed in _/usr/include/eigen3_
    * [OpenCV](http://opencv.org/) should be installed.
    * [FLANN library](http://www.cs.ubc.ca/research/flann/) and its dependency [HDF5](https://www.hdfgroup.org/HDF5/release/obtain5.html) should be installed for the NN search method
	    - NN can be disabled at compile time using nn=0 if these are not available (see compile time switches below)
	* [Boost Library](http://www.boost.org/) should be installed
    * [Intel TBB](https://www.threadingbuildingblocks.org/) / [OpenMP](http://openmp.org/wp/) should be installed if parallelization is to be enabled.
    * [ViSP library](https://visp.inria.fr/) should be installed if its [template tracker module](https://visp.inria.fr/template-tracking/) or [input pipeline](http://visp-doc.inria.fr/doxygen/visp-3.0.0/group__group__io__video.html) is enabled during compilation (see below).
	    - Note that [version 3.0.0](http://gforge.inria.fr/frs/download.php/latestfile/475/visp-3.0.0.zip)+ is required. The Ubuntu apt package is 2.8 and is therefore incompatible.
    * [Xvision](https://bitbucket.org/abhineet123/xvision2) should be installed if it is enabled during compilation (see below).
* Download the source code using `git clone https://abhineet123@bitbucket.org/abhineet123/mtf.git`.
* MTF comes with both a [make](https://www.gnu.org/software/make/) and a [cmake](https://cmake.org/) build system where the former is recommended for developers/contributors as it offers finer level of control while the latter is for users of the library who only want to install it once (or when the former does not work). For cmake, first use the [standard method](https://cmake.org/runningcmake/) (i.e. ``mkdir build && cd build && cmake ..``) to create the makefile and then use one of the make commands as specified below.
* Use one of the following make commands to compile and install the library and the demo application:
    * `make` or `make mtf` : compiles the shared library (_libmtf.so_) to the build directory (_Build/Release_)
    * `make install` : compiles the shared library if needed and copies it to _/usr/lib_; also copies the headers to _/usr/include/mtf_; this needs administrative (sudo) privilege.
	    - if this is not available, then the variables `MTF_LIB_INSTALL_DIR` and `MTF_HEADER_INSTALL_DIR` in the makefile can be modified to install elsewhere. This can be done either by editing the file itself or providing these with the make command as: `make install MTF_LIB_INSTALL_DIR=<library_installation_dir> MTF_HEADER_INSTALL_DIR=<header_installation_dir>`
		    - these folders should be present in LD_LIBRARY_PATH and C_INCLUDE_PATH/CPLUS_INCLUDE_PATH environment variables respectively
    * `make mtfe` compiles the example file _Examples/runMTF.cc_ to create an executable called _runMTF_ that uses this library to track objects. This too is placed in the build directory.
	* `make install_exec`: creates _runMTF_ if needed and copies it to _/usr/bin_; this needs administrative privilege too - change `MTF_EXEC_INSTALL_DIR` in Examples/Examples.mak if this is not available
    * **`make mtfi`** : all of the above - **recommended command that compiles and installs the library and the executable**
    * `make mtfp` : compile the Python interface to MTF - this creates a Python module called _pyMTF.so_ that serves as a front end for running these trackers from Python.
Usage of this module is fully demonstrated in the `mtfTracker.py` file in our [Python Tracking Framework](https://bitbucket.org/abhineet123/ptf)
    * **Compile time switches** for all of the above commands (only applicable to the make build system - for cmake there are corresponding options that can be configured before building its makefile):
	    - `only_nt=1` will enable only the **Non Templated (NT)** implementations of SMs and disable their templated implementations that are extremely time consuming to compile though being faster at runtime (enabled by default)
		    - can be very useful for rapid debugging of AMs and SSMs where headers need to be modified;
		    - the NT implementation of NN only works with GNN since FLANN library needs its object to be templated on the AM; 
	    - `nn=0` will disable the templated implementation of NN search method (enabled by default).
		    - should be specified if FLANN is not available
		    - only matters if the previous option is not specified
	    - `lt=0` will disable the third party open source learning based trackers - [DSST](http://www.cvl.isy.liu.se/en/research/objrec/visualtracking/scalvistrack/index.html), [KCF](http://home.isr.uc.pt/~henriques/circulant/), [CMT](http://www.gnebehay.com/cmt/), [TLD](http://www.gnebehay.com/tld/), [RCT](http://www4.comp.polyu.edu.hk/~cslzhang/CT/CT.htm), [MIL](http://ieeexplore.ieee.org/xpl/login.jsp?tp=&arnumber=5206737&url=http%3A%2F%2Fieeexplore.ieee.org%2Fxpls%2Fabs_all.jsp%3Farnumber%3D5206737), [Struck](http://www.samhare.net/research/struck), [Fragments based tracker](http://www.cs.technion.ac.il/~amita/fragtrack/fragtrack.htm) and [Descriptor Fields Tracker (DFT)](http://cvlab.epfl.ch/page-107683-en.html) - that are also bundled with this library (in _ThirdParty_ subfolder) (enabled by default).
	    - `vp=1` will enable ViSP template trackers and input pipeline(disabled by default).
	    - `xv=1` will enable Xvision trackers and input pipeline (disabled by default and not recommended).
	    - `o=0` will compile the library in debug mode (_libmtf_debug.so_) and disable optimization - the corresponding executable is called _runMTFd_
		- `ver=<version_postfix>` will postfix all compiled library and executable files with the provided postfix so that multiple versions may coexist on the same system without conflict; e.g. `ver=2` will create libmtf_v2.so and runMTF_v2;
    * Clean up commands:
	    * `make clean` : removes all the .o files and the .so file created during compilation from the Build folder folder
        * `make mtfc` : also removes the executable

Setting Parameters:
-------------------
MTF parameters can be specified either in the _cfg_ files present in the _Config_ sub folder or from the command line.
Please refer the ReadMe in the **_Config_** sub folder (Source->Config) for detailed instructions.

Running the demo application:
-----------------------------

Use either `make run` or `runMTF` to start the tracking application using the settings specified as above.


**For Developers**
==================

Adding a new Appearance Model (AM) or State Space Model (SSM):
-------------------------------------------------------------

make
----
1. Modify the .mak file in the respective sub directory to:
    - add the name of the AM/SSM to the variable `APPEARANCE_MODELS` or `STATE_SPACE_MODELS` respectively
	- add rule to compile the .o of the new AM/SSM that is dependent on its source and header files - simplest method would be to copy an existing command and change the names of the model.
2. Modify makefile to add any extra dependencies that the new files need to include under variable `FLAGS64`
and extra libraries to link against under `LIB_MTF_LIBS`

cmake
-----
1. - Modify the AM/AM.cmake file to add the name of the AM to set(APPEARANCE_MODELS …)
   - Modify the SM/SM.cmake file to add the name of the SM to set(SEARCH_METHODS …)

3. Modify "mtf.h" to add a new AM/SSM option in the overloaded function `getTrackerObj` (starting lines 498 and 533 respectively) that can be used with your config file to pick the AM/SSM, and create an object with this selected model.
4. Modify "Macros/register.h" to add the new AM/SSM class name under `_REGISTER_TRACKERS_AM/_REGISTER_HTRACKERS_AM` or `_REGISTER_TRACKERS/_REGISTER_TRACKERS_SSM/_REGISTER_HTRACKERS/_REGISTER_HTRACKERS_SSM` respectively

All steps are identical for adding a new Search Method (SM) too except the last one which is not needed. Instead this header needs to be included and the appropriate macro needs to be called from its source file to register the SM with all existing AMs and SSMs. Refer to the last 2 lines of the .cc file of any existing SM to see how to do this.



Example: Implementing a minimalistic AM that can be used with NN search method:
-------------------------------------------------------------------------------

You need to create a new derived class from AppearanceModel.
Implement the following functions:

1. `initializePixVals/updatePixVals` : takes the current location of the object as a 2xN matrix where N = no. of sampled points and each column contains the x,y coordinates of one point. By default, it is simply supposed to extract the pixel values at these locations from the current image but there is no restriction by design and you are free to compute anything from these pixel values.
2. `initializeDistFeat/updateDistFeat` : these compute a distance feature transform from the current patch.
3. `getDistFeat`: returns a pointer to an array containing the distance feature vector computed by the above function. There is also an overloaded variant of updateDistFeat  that takes a pointer as input and directly writes this feature vector to the pre-allocated array pointed to by this pointer.
4. distance functor (`operator()`): computes a scalar that measures the dissimilarity or distance between two feature vectors (obtained using the previous two functions).

Example: Implementing a minimalistic AM that can be used with Particle Filter search method:
-------------------------------------------------------------------------------

You need to create a new derived class from AppearanceModel.
Implement the following functions:

1. Same as with NN. No need to overwrite if just need to extract the pixel values
2. `getLikelihood`: get the likelihood/weights of the particle
3. `initialize`: initialize some class variables like the initial features.
4. `update`: update the similarty between current patch and the template 
