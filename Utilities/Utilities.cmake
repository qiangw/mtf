set(MTF_UTILITIES histUtils warpUtils imgUtils miscUtils netUtils)
addPrefixAndSuffix("${MTF_UTILITIES}" "Utilities/src/" ".cc" MTF_UTILITIES_SRC)
set(MTF_SRC ${MTF_SRC} ${MTF_UTILITIES_SRC})
set(MTF_INCLUDE_DIRS ${MTF_INCLUDE_DIRS} Utilities/include)
