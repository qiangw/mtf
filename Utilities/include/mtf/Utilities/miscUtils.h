#ifndef MTF_MISC_UTILS_H
#define MTF_MISC_UTILS_H

#include "mtf/Macros/common.h"

_MTF_BEGIN_NAMESPACE

namespace utils{
	template<typename MatT>
	inline void printMatrix(const MatT &eig_mat, const char* mat_name = nullptr,
		const char* fmt = "%15.9f", const char *coeff_sep = "\t",
		const char *row_sep = "\n"){
		if(mat_name)
			printf("%s:\n", mat_name);
		for(int i = 0; i < eig_mat.rows(); i++){
			for(int j = 0; j < eig_mat.cols(); j++){
				printf(fmt, eig_mat(i, j));
				printf("%s", coeff_sep);
			}
			printf("%s", row_sep);
		}
		printf("\n");
	}
	template<typename ScalarT>
	inline void printScalar(ScalarT scalar_val, const char* scalar_name,
		const char* fmt = "%15.9f", const char *name_sep = "\t",
		const char *val_sep = "\n"){
		fprintf(stdout, "%s:%s", scalar_name, name_sep);
		fprintf(stdout, fmt, scalar_val);
		fprintf(stdout, "%s", val_sep);
	}
	// write matrix values to a custom formatted ASCII text file
	template<typename MatT>
	inline void printMatrixToFile(const MatT &eig_mat, const char* mat_name,
		const char* fname, const char* fmt = "%15.9f", const char* mode = "a",
		const char *coeff_sep = "\t", const char *row_sep = "\n",
		char** const row_labels = nullptr, const char **mat_header = nullptr,
		const char* header_fmt = "%15s", const char *name_sep = "\n"){
		//typedef typename ImageT::RealScalar ScalarT;
		//printf("Opening file: %s to write %s\n", fname, mat_name);
		FILE *fid = fopen(fname, mode);
		if(!fid){
			printf("File %s could not be opened successfully\n", fname);
			return;
		}
		if(mat_name)
			fprintf(fid, "%s:%s", mat_name, name_sep);
		if(mat_header){
			for(int j = 0; j < eig_mat.cols(); j++){
				fprintf(fid, header_fmt, mat_header[j]);
				fprintf(fid, "%s", coeff_sep);
			}
			fprintf(fid, "%s", row_sep);
		}
		for(int i = 0; i < eig_mat.rows(); i++){
			for(int j = 0; j < eig_mat.cols(); j++){
				fprintf(fid, fmt, eig_mat(i, j));
				fprintf(fid, "%s", coeff_sep);

			}
			if(row_labels){
				fprintf(fid, "\t%s", row_labels[i]);
			}
			fprintf(fid, "%s", row_sep);
		}
		fclose(fid);
	}
	// write scalar value to a custom formatted ASCII text file
	template<typename ScalarT>
	inline void printScalarToFile(ScalarT scalar_val, const char* scalar_name,
		const char* fname, const char* fmt = "%15.9f", const char* mode = "a", 
		const char *name_sep = "\t", const char *val_sep = "\n"){
		//typedef typename ImageT::RealScalar ScalarT;
		FILE *fid = fopen(fname, mode);
		if(!fid){
			printf("File %s could not be opened successfully\n", fname);
			return;
		}
		if(scalar_name)
			fprintf(fid, "%s:%s", scalar_name, name_sep);
		fprintf(fid, fmt, scalar_val);
		fprintf(fid, "%s", val_sep);
		fclose(fid);
	}
	// save matrix data to binary file
	template<typename ScalarT, typename MatT>
	inline void saveMatrixToFile(const MatT &eig_mat, const char* fname,
		const char* mode = "ab"){
		FILE *fid = fopen(fname, "ab");
		if(!fid){
			printf("File %s could not be opened successfully\n", fname);
			return;
		}
		fwrite(eig_mat.data(), sizeof(ScalarT), eig_mat.size(), fid);
		fclose(fid);
	}
	// save scalar data to binary file
	template<typename ScalarT>
	inline void saveScalarToFile(ScalarT &scalar_val, const char* fname,
		const char* mode = "ab"){
		FILE *fid = fopen(fname, mode);
		if(!fid){
			printf("File %s could not be opened successfully\n", fname);
			return;
		}
		fwrite(&scalar_val, sizeof(ScalarT), 1, fid);
		fclose(fid);
	}

	// printing functions for OpenCV Mat matrices
	template<typename ScalarT>
	inline void printMatrix(const cv::Mat &cv_mat, const char* mat_name,
		const char* fmt = "%15.9f", const char *coeff_sep = "\t",
		const char *row_sep = "\n"){
		printf("%s:\n", mat_name);
		for(int i = 0; i < cv_mat.rows; i++){
			for(int j = 0; j < cv_mat.cols; j++){
				printf(fmt, cv_mat.at<ScalarT>(i, j));
				printf("%s", coeff_sep);
			}
			printf("%s", row_sep);
		}
		printf("\n");
	}

	template<typename ScalarT>
	inline void printMatrixToFile(const cv::Mat &cv_mat, const char* mat_name,
		const char* fname, const char* fmt = "%15.9f", const char* mode = "a",
		const char *coeff_sep = "\t", const char *row_sep = "\n",
		const char **row_labels = NULL, const char **mat_header = NULL,
		const char* header_fmt = "%15s"){
		//typedef typename ImageT::RealScalar ScalarT;
		//printf("Opening file: %s to write %s\n", fname, mat_name);
		FILE *fid = fopen(fname, mode);
		if(!fid){
			printf("File %s could not be opened successfully\n", fname);
			return;
		}
		fprintf(fid, "%s:\n", mat_name);
		if(mat_header){
			for(int j = 0; j < cv_mat.cols; j++){
				fprintf(fid, header_fmt, mat_header[j]);
				fprintf(fid, "%s", coeff_sep);
			}
			fprintf(fid, "%s", row_sep);
		}
		for(int i = 0; i < cv_mat.rows; i++){
			for(int j = 0; j < cv_mat.cols; j++){
				fprintf(fid, fmt, cv_mat.at<ScalarT>(i, j));
				fprintf(fid, "%s", coeff_sep);

			}
			if(row_labels){
				fprintf(fid, "\t%s", row_labels[i]);
			}
			fprintf(fid, "%s", row_sep);
		}
		fclose(fid);
	}

	double getCenterLocationError(const cv::Mat &gt_corners, const cv::Mat &tracker_corners);
	double getMeanCornerDistanceError(const cv::Mat &gt_corners, const cv::Mat &tracker_corners);
	double getJaccardError(const cv::Mat &gt_corners, const cv::Mat &tracker_corners,
		int img_width, int img_height);
	void cornersToPoint2D(cv::Point2d(&cv_corners)[4], const cv::Mat &cv_corners_mat);
	void cornersToPoint2i(cv::Point2i(&cv_corners)[4], const cv::Mat &cv_corners_mat);
	void cornersToMat(cv::Mat &cv_corners_mat,
		const cv::Point2d(&cv_corners)[4]);
	void drawCorners(cv::Mat &img, const cv::Point2d(&cv_corners)[4],
		const cv::Scalar corners_col, const std::string label);
	// mask a vector, i.e. retain only those entries where the given mask is true
	void maskVector(VectorXd &masked_vec, const VectorXd &in_vec,
		const VectorXb &mask, int masked_size, int in_size);
	// returning version
	VectorXd maskVector(const VectorXd &in_vec,
		const VectorXb &mask, int masked_size, int in_size);
	// mask 2D matrix by row, i.e. retain only those columns whee the mask is true
	template<typename MatT>
	inline void maskMatrixByRow(MatT &masked_mat, const MatT &in_mat,
		const VectorXb &mask, int n_cols){
		assert(in_mat.cols() == n_cols);
		assert(in_mat.cols() == mask.size());
		assert(masked_mat.rows() == in_mat.rows());

		int masked_size = mask.array().count();
		masked_mat.resize(NoChange, masked_size);
		int mask_id = 0;
		for(int i = 0; i < n_cols; i++){
			if(mask(i)){ masked_mat.col(mask_id++) = in_mat.col(i); }
		}
	}
	// returning version
	template<typename MatT>
	inline MatT maskMatrixByRow(const MatT &in_mat,
		const VectorXb &mask, int n_cols){
		int masked_size = mask.array().count();
		MatT masked_mat(in_mat.rows(), masked_size);
		maskMatrixByRow(masked_mat, in_mat, mask, n_cols);
		return masked_mat;
	}
	// mask 2D matrix by column, i.e. retain only those rows where the mask is true
	template<typename MatT>
	inline void maskMatrixByCol(MatT &masked_mat, const MatT &in_mat,
		const VectorXb &mask, int n_rows){
		assert(in_mat.rows() == n_rows);
		assert(in_mat.rows() == mask.size());
		assert(masked_mat.rows() == in_mat.rows());

		int masked_size = mask.array().count();
		masked_mat.resize(NoChange, masked_size);
		int mask_id = 0;
		for(int i = 0; i < n_rows; i++){
			if(mask(i)){ masked_mat.row(mask_id++) = in_mat.row(i); }
		}
	}
	// returning version
	template<typename MatT>
	inline MatT maskMatrixByCol(const MatT &in_mat,
		const VectorXb &mask, int n_rows){
		int masked_size = mask.array().count();
		MatT masked_mat(masked_size, in_mat.cols());
		maskMatrixByRow(masked_mat, in_mat, mask, n_rows);
		return masked_mat;
	}

	template<typename ScalarT, typename EigT>
	inline void copyCVToEigen(EigT &eig_mat, const cv::Mat &cv_mat){
		assert(eig_mat.rows() == cv_mat.rows && eig_mat.cols() == cv_mat.cols);
		for(int i = 0; i < cv_mat.rows; i++){
			for(int j = 0; j < cv_mat.cols; j++){
				eig_mat(i, j) = cv_mat.at<ScalarT>(i, j);
			}
		}
	}
	// specialization with loop unrolling for copying a warp matrix from OpenCV to Eigen
	template<>
	void copyCVToEigen<double, Matrix3d>(Matrix3d &eig_mat, const cv::Mat &cv_mat);
	//returning version
	template<typename ScalarT>
	inline MatrixXd copyCVToEigen(const cv::Mat &cv_mat){
		MatrixXd eig_mat(cv_mat.rows, cv_mat.cols);
		copyCVToEigen<MatrixXd, ScalarT>(eig_mat, cv_mat);
		return eig_mat;
	}

	template<typename ScalarT, typename EigT>
	inline void copyEigenToCV(cv::Mat &cv_mat, const  EigT &eig_mat){
		assert(cv_mat.rows == eig_mat.rows() && cv_mat.cols == eig_mat.cols());
		for(int i = 0; i < cv_mat.rows; i++){
			for(int j = 0; j < cv_mat.cols; j++){
				cv_mat.at<ScalarT>(i, j) = eig_mat(i, j);
			}
		}
	}
	// specialization for copying corners
	template<>
	void copyEigenToCV<double, CornersT>(cv::Mat &cv_mat,
		const CornersT &eig_mat);
	//returning version
	template<typename EigT, typename ScalarT, int CVMatT>
	inline cv::Mat copyEigenToCV(const EigT &eig_mat){
		cv::Mat cv_mat(eig_mat.rows(), eig_mat.cols(), CVMatT);
		copyEigenToCV<EigT, ScalarT>(cv_mat, eig_mat);
		return cv_mat;
	}
	int writeTimesToFile(vector<double> &proc_times,
		vector<char*> &proc_labels, char *time_fname, int iter_id);
	// compute the rectangle that best fits an arbitrry quadrilateral
	// in terms of maximizing the Jaccard index of overlap 
	// between the corresponding regions; 
	// an optional resize factor is provided to avaoid further loss in precision
	// in case a resizing is needed and the input corners are floating point numbers;
	template<typename ValT>
	cv::Rect_<ValT> getBestFitRectangle(const cv::Mat &corners,
		int img_width=0, int img_height=0);
	// adjust the rectangle bounds so it lies entirely within the image with the given size
	template<typename ValT>
	cv::Rect_<ValT> getBoundedRectangle(cv::Rect_<ValT> _in_rect, int img_width, int img_height);
	void drawCorners(cv::Mat &img, const cv::Mat corners, cv::Scalar col = cv::Scalar(0, 255, 0),
		int line_thickness = 2, const char *label = nullptr, double font_size = 0.50,
		bool show_corner_ids = false, bool show_label = false);
	void drawGrid(cv::Mat &img, const PtsT &grid_pts, int res_x, int res_y);
	template<typename ImgValT, typename PatchValT>
	void drawPatch(cv::Mat &img, const cv::Mat &patch, int n_channels=1, int start_x = 0, int start_y = 0);


}
_MTF_END_NAMESPACE
#endif
