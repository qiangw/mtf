#include "mtf/Utilities/miscUtils.h"

_MTF_BEGIN_NAMESPACE

namespace utils{
	double getCenterLocationError(const cv::Mat &gt_corners, const cv::Mat &tracker_corners){
		// compute the center location error between the tracking result and the ground truth
		cv::Mat corner_diff = gt_corners - tracker_corners;
		double mean_x_diff = (corner_diff.at<double>(0, 0) + corner_diff.at<double>(0, 1) +
			corner_diff.at<double>(0, 2) + corner_diff.at<double>(0, 3)) / 4.0;
		double mean_y_diff = (corner_diff.at<double>(1, 0) + corner_diff.at<double>(1, 1) +
			corner_diff.at<double>(1, 2) + corner_diff.at<double>(1, 3)) / 4.0;
		double tracking_err = sqrt((mean_x_diff*mean_x_diff) + (mean_y_diff*mean_y_diff));
		return tracking_err;
	}
	double getMeanCornerDistanceError(const cv::Mat &gt_corners, const cv::Mat &tracker_corners){
		// compute the mean corner distance between the tracking result and the ground truth
		double tracking_err = 0;
		for(int corner_id = 0; corner_id < 4; ++corner_id){
			double x_diff = gt_corners.at<double>(0, corner_id) - tracker_corners.at<double>(0, corner_id);
			double y_diff = gt_corners.at<double>(1, corner_id) - tracker_corners.at<double>(1, corner_id);
			//printf("x_diff: %f\n", x_diff);
			//printf("y_diff: %f\n", y_diff);
			tracking_err += sqrt((x_diff*x_diff) + (y_diff*y_diff));
		}
		tracking_err /= 4.0;
		//printMatrix<double>(gt_corners, "gt_corners");
		//printMatrix<double>(tracker_corners, "tracker_corners");
		//printScalar(tracking_err, "tracking_err");
		return tracking_err;
	}
	double getJaccardError(const cv::Mat &gt_corners, const cv::Mat &tracker_corners, 
		int img_width, int img_height){
		// compute the Jaccard index error between the tracking result and the ground truth
		cv::Point2i gt_corners_int[4], tracker_corners_int[4];
		cornersToPoint2i(gt_corners_int, gt_corners);
		cornersToPoint2i(tracker_corners_int, tracker_corners);
		cv::Mat gt_img(img_height, img_width, CV_8UC1, cv::Scalar(0));
		cv::Mat tracker_img(img_height, img_width, CV_8UC1, cv::Scalar(0));
		fillConvexPoly(gt_img, gt_corners_int, 4, cv::Scalar(255));
		fillConvexPoly(tracker_img, tracker_corners_int, 4, cv::Scalar(255));
		cv::Mat intersection_img(img_height, img_width, CV_8UC1, cv::Scalar(0));
		cv::Mat union_img(img_height, img_width, CV_8UC1, cv::Scalar(0));
		cv::bitwise_and(gt_img, tracker_img, intersection_img);
		cv::bitwise_or(gt_img, tracker_img, union_img);
		double tracking_err = 1.0 - cv::sum(intersection_img)[0] / cv::sum(union_img)[0];
		return tracking_err;
	}
	void cornersToPoint2D(cv::Point2d(&cv_corners)[4],
		const cv::Mat &cv_corners_mat) {
		for(int i = 0; i < 4; i++) {
			cv_corners[i].x = cv_corners_mat.at<double>(0, i);
			cv_corners[i].y = cv_corners_mat.at<double>(1, i);
		}
	}
	void cornersToPoint2i(cv::Point2i(&cv_corners)[4],
		const cv::Mat &cv_corners_mat) {
		for(int i = 0; i < 4; i++) {
			cv_corners[i].x = cv_corners_mat.at<double>(0, i);
			cv_corners[i].y = cv_corners_mat.at<double>(1, i);
		}
	}
	void cornersToMat(cv::Mat &cv_corners_mat,
		const cv::Point2d(&cv_corners)[4]) {
		for(int i = 0; i < 4; i++) {
			cv_corners_mat.at<double>(0, i) = cv_corners[i].x;
			cv_corners_mat.at<double>(1, i) = cv_corners[i].y;
		}
	}
	void drawCorners(cv::Mat &img, const cv::Point2d(&corners)[4],
		const cv::Scalar corners_col, const std::string label){
		line(img, corners[0], corners[1], corners_col, 2);
		line(img, corners[1], corners[2], corners_col, 2);
		line(img, corners[2], corners[3], corners_col, 2);
		line(img, corners[3], corners[0], corners_col, 2);
		putText(img, label, corners[0],
			cv::FONT_HERSHEY_SIMPLEX, 0.5, corners_col);
	}
	// mask a vector, i.e. retain only those entries where the given mask is true
	void maskVector(VectorXd &masked_vec, const VectorXd &in_vec,
		const VectorXb &mask, int masked_size, int in_size){
		assert(in_vec.size() == mask.size());
		assert(mask.array().count() == masked_size);

		masked_vec.resize(masked_size);
		int mask_id = 0;
		for(int i = 0; i < in_size; i++){
			if(!mask(i)){ masked_vec(mask_id++) = in_vec(i); }
		}
	}
	// returning version
	VectorXd maskVector(const VectorXd &in_vec,
		const VectorXb &mask, int masked_size, int in_size){
		assert(in_vec.size() == mask.size());
		assert(mask.array().count() == masked_size);

		VectorXd masked_vec(masked_size);
		maskVector(masked_vec, in_vec, mask, masked_size, in_size);
		return masked_vec;
	}
	// specialization with loop unrolling for copying a warp matrix from OpenCV to Eigen
	template<>
	void copyCVToEigen<double, Matrix3d>(Matrix3d &eig_mat, const cv::Mat &cv_mat){
		assert(eig_mat.rows() == cv_mat.rows && eig_mat.cols() == cv_mat.cols);
		eig_mat(0, 0) = cv_mat.at<double>(0, 0);
		eig_mat(0, 1) = cv_mat.at<double>(0, 1);
		eig_mat(0, 2) = cv_mat.at<double>(0, 2);

		eig_mat(1, 0) = cv_mat.at<double>(1, 0);
		eig_mat(1, 1) = cv_mat.at<double>(1, 1);
		eig_mat(1, 2) = cv_mat.at<double>(1, 2);

		eig_mat(2, 0) = cv_mat.at<double>(2, 0);
		eig_mat(2, 1) = cv_mat.at<double>(2, 1);
		eig_mat(2, 2) = cv_mat.at<double>(2, 2);
	}
	// specialization for copying corners
	template<>
	void copyEigenToCV<double, CornersT>(cv::Mat &cv_mat,
		const CornersT &eig_mat){
		assert(cv_mat.rows == 2 && cv_mat.cols == 4);
		cv_mat.at<double>(0, 0) = eig_mat(0, 0);
		cv_mat.at<double>(1, 0) = eig_mat(1, 0);
		cv_mat.at<double>(0, 1) = eig_mat(0, 1);
		cv_mat.at<double>(1, 1) = eig_mat(1, 1);
		cv_mat.at<double>(0, 2) = eig_mat(0, 2);
		cv_mat.at<double>(1, 2) = eig_mat(1, 2);
		cv_mat.at<double>(0, 3) = eig_mat(0, 3);
		cv_mat.at<double>(1, 3) = eig_mat(1, 3);
	}
	int writeTimesToFile(vector<double> &proc_times,
		vector<char*> &proc_labels, char *time_fname, int iter_id){
		MatrixXd iter_times(proc_times.size(), 2);
		for(int proc_id = 0; proc_id < proc_times.size(); proc_id++){
			iter_times(proc_id, 0) = proc_times[proc_id];
		}
		double total_iter_time = iter_times.col(0).sum();
		iter_times.col(1) = (iter_times.col(0) / total_iter_time) * 100;
		utils::printScalarToFile(iter_id, "iteration", time_fname, "%6d", "a");
		//char **row_label_ptr = &proc_labels[0];
		utils::printMatrixToFile(iter_times, "iter_times", time_fname, "%15.9f", "a",
			"\t", "\n", &proc_labels[0]);
		utils::printScalarToFile(total_iter_time, "total_iter_time", time_fname, "%15.9f", "a");
		return total_iter_time;
	}
	template<typename ValT>
	cv::Rect_<ValT>  getBestFitRectangle(const cv::Mat &corners, int _img_width, int _img_height){
		double center_x = (corners.at<double>(0, 0) + corners.at<double>(0, 1) +
			corners.at<double>(0, 2) + corners.at<double>(0, 3)) / 4;
		double center_y = (corners.at<double>(1, 0) + corners.at<double>(1, 1) +
			corners.at<double>(1, 2) + corners.at<double>(1, 3)) / 4;

		double mean_half_width = (abs(corners.at<double>(0, 0) - center_x) + abs(corners.at<double>(0, 1) - center_x)
			+ abs(corners.at<double>(0, 2) - center_x) + abs(corners.at<double>(0, 3) - center_x)) / 4.0;
		double mean_half_height = (abs(corners.at<double>(1, 0) - center_y) + abs(corners.at<double>(1, 1) - center_y)
			+ abs(corners.at<double>(1, 2) - center_y) + abs(corners.at<double>(1, 3) - center_y)) / 4.0;

		cv::Rect_<ValT> best_fit_rect;
		best_fit_rect.x = center_x - mean_half_width;
		best_fit_rect.y = center_y - mean_half_height;
		best_fit_rect.width = 2 * mean_half_width;
		best_fit_rect.height = 2 * mean_half_height;

		return _img_width > 0 && _img_height > 0 ? getBoundedRectangle<ValT>(best_fit_rect, _img_width, _img_height) : best_fit_rect;
	}
	template cv::Rect_<int> getBestFitRectangle<int>(const cv::Mat &corners, int _img_width = 0, int _img_height = 0);
	template cv::Rect_<double> getBestFitRectangle<double>(const cv::Mat &corners, int _img_width = 0, int _img_height = 0);

	template<typename ValT>
	cv::Rect_<ValT> getBoundedRectangle(cv::Rect_<ValT> _in_rect, int _img_width, int _img_height){
		cv::Rect_<ValT> bounded_rect;
		bounded_rect.x = _in_rect.x < 0 ? 0 : _in_rect.x;
		bounded_rect.y = _in_rect.y < 0 ? 0 : _in_rect.y;
		bounded_rect.width = _in_rect.x + _in_rect.width > _img_width - 1 ? _img_width - 1 - _in_rect.x : _in_rect.width;
		bounded_rect.height = _in_rect.y + _in_rect.height > _img_height - 1 ? _img_height - 1 - _in_rect.y : _in_rect.height;
		return bounded_rect;
	}
	template cv::Rect_<int> getBoundedRectangle(cv::Rect_<int> _in_rect, int _img_width, int _img_height);
	template cv::Rect_<double> getBoundedRectangle(cv::Rect_<double> _in_rect, int _img_width, int _img_height);


	void drawCorners(cv::Mat &img, const cv::Mat corners, cv::Scalar col,
		int line_thickness, const char *label, double font_size,
		bool show_corner_ids, bool show_label){
		cv::Point2d corners_point2d[4];
		cornersToPoint2D(corners_point2d, corners);
		//printMatrix<double>(corners, "drawn corners");
		for(int corner_id = 0; corner_id < 4; ++corner_id) {
			cv::line(img, corners_point2d[corner_id], corners_point2d[(corner_id + 1) % 4],
				col, line_thickness);
			if(show_corner_ids){
				putText(img, to_string(corner_id), corners_point2d[corner_id],
					cv::FONT_HERSHEY_SIMPLEX, font_size, col);
			}
		}
		if(label && show_label){
			putText(img, label, corners_point2d[0], cv::FONT_HERSHEY_SIMPLEX, font_size, col);
		}
	}

	void drawGrid(cv::Mat &img, const PtsT &grid_pts, int res_x, int res_y){
		//draw vertical lines
		for(int x_id = 0; x_id < res_x; ++x_id){
			for(int y_id = 0; y_id < res_y - 1; ++y_id){
				int pt_id1 = (y_id * res_x + x_id);
				int pt_id2 = ((y_id + 1) * res_x + x_id);
				cv::Point p1(int(grid_pts(0, pt_id1)), int(grid_pts(1, pt_id1)));
				cv::Point p2(int(grid_pts(0, pt_id2)), int(grid_pts(1, pt_id2)));
				cv::line(img, p1, p2, cv::Scalar(0, 255, 0), 1);
			}
		}
		//draw horizontal lines
		for(int y_id = 0; y_id < res_y; ++y_id){
			for(int x_id = 0; x_id < res_x - 1; ++x_id){
				int pt_id1 = (y_id * res_x + x_id);
				int pt_id2 = (y_id  * res_x + x_id + 1);
				cv::Point p1(int(grid_pts(0, pt_id1)), int(grid_pts(1, pt_id1)));
				cv::Point p2(int(grid_pts(0, pt_id2)), int(grid_pts(1, pt_id2)));
				cv::line(img, p1, p2, cv::Scalar(0, 255, 0), 1);
			}
		}
	}
	template<typename ImgValT, typename PatchValT>
	void drawPatch(cv::Mat &img, const cv::Mat &patch, int n_channels, 
		int start_x, int start_y){
		if((patch.channels() > 1) && (patch.channels() != img.channels())){
			throw std::invalid_argument(
				cv::format("No. of channels in the image: %d does not match that in the patch: %d",
				img.channels(), patch.channels()));
		}

		for(int row_id = 0; row_id < patch.rows - 1; ++row_id){
			for(int col_id = 0; col_id < patch.cols; ++col_id){
				switch(n_channels){
				case 1:
					img.at<ImgValT>(start_y + row_id, start_x + col_id) =
						patch.at<PatchValT>(row_id, col_id);
					break;
				case 3:
					img.at< cv::Vec<ImgValT, 3> >(start_y + row_id, start_x + col_id) =
						patch.at< cv::Vec<PatchValT, 3> >(row_id, col_id);
					break;
				default:
					throw std::domain_error(
						cv::format("drawPatch :: %d channel images are not supported", n_channels));
				}				
			}
		}
	}
	template void drawPatch<uchar, uchar>(cv::Mat &img, const cv::Mat &patch, int n_channels,
		int start_x, int start_y);
}
_MTF_END_NAMESPACE
