#ifndef MTF_H
#define MTF_H

//! External interface of the MTF library; this is the only header that needs to be included to use the framework
//! Provides functions to create trackers corresponding to different combinations of 
//! search methods, appearance models and state space models as well as some third party 

//#define MTF_VERSION_MAJOR @MTF_VERSION_MAJOR@
//#define MTF_VERSION_MINOR @MTF_VERSION_MINOR@
#ifndef ROOT_HEADER_DIR
#define ROOT_HEADER_DIR mtf/
#endif
#define stringify(str) #str
#define concatenate(a, b) a##b
#define include_path(file) stringify(concatenate(ROOT_HEADER_DIR,file))

// parameters for the different modules
#include "mtf/Config/parameters.h"

#ifndef ENABLE_ONLY_NT
// search methods
#include "mtf/SM/ESM.h"
#include "mtf/SM/AESM.h"
#include "mtf/SM/ICLK.h"
#include "mtf/SM/FCLK.h"
#include "mtf/SM/FALK.h"
#include "mtf/SM/IALK.h"
#include "mtf/SM/FCGD.h"
#include "mtf/SM/PF.h"
#ifndef DISABLE_NN
#include "mtf/SM/NN.h"
#endif

// composite search methods
#include "mtf/SM/CascadeSM.h"
#include "mtf/SM/GridTrackerCV.h"
#include "mtf/SM/GridTracker.h"
#include "mtf/SM/RKLT.h"
#include "mtf/SM/ParallelSM.h"
#endif
#include "mtf/SM/CascadeTracker.h"
#include "mtf/SM/ParallelTracker.h"
#include "mtf/SM/PyramidalTracker.h"
#include "mtf/SM/LineTracker.h"

// Non templated implementations of search methods
#include "mtf/SM/NT/FCLK.h"
#include "mtf/SM/NT/ICLK.h"
#include "mtf/SM/NT/FALK.h"
#include "mtf/SM/NT/IALK.h"
#include "mtf/SM/NT/ESM.h"
#include "mtf/SM/NT/PF.h"
#include "mtf/SM/NT/NN.h"
#ifndef DISABLE_REGNET
#include "mtf/SM/NT/RegNet.h"
#endif

// Obsolete/failed search methods
//#include "mtf/SM/HACLK.h"
//#include "mtf/SM/ESMH.h"
//#include "mtf/SM/HESM.h"
//#include "mtf/SM/FESM.h"
//#include "mtf/SM/IALK2.h"

// appearance models
#include "mtf/AM/SSD.h"
#include "mtf/AM/NSSD.h"
#include "mtf/AM/ZNCC.h"
#include "mtf/AM/SCV.h"
#include "mtf/AM/LSCV.h"
#include "mtf/AM/RSCV.h"
#include "mtf/AM/LRSCV.h"
#include "mtf/AM/KLD.h"
#include "mtf/AM/LKLD.h"	
#include "mtf/AM/MI.h"
#include "mtf/AM/SPSS.h"
#include "mtf/AM/SSIM.h"
#include "mtf/AM/NCC.h"
#include "mtf/AM/CCRE.h"
// multi channel variants
#include "mtf/AM/MCSSD.h"
#include "mtf/AM/MCSCV.h"
#include "mtf/AM/MCRSCV.h"
#include "mtf/AM/MCZNCC.h"
#include "mtf/AM/MCNCC.h"
#include "mtf/AM/MCMI.h"
#include "mtf/AM/MCSSIM.h"
#include "mtf/AM/MCSPSS.h"
#include "mtf/AM/MCCCRE.h"
#ifndef DISABLE_FMAPS
#include "mtf/AM//FMaps.h"
#endif
#ifndef DISABLE_PCA
#include "mtf/AM/PCA.h"
#endif
// illumination models
#include "mtf/AM/GB.h"

// state space models
#include "mtf/SSM/Homography.h"
#include "mtf/SSM/Affine.h"
#include "mtf/SSM/Similitude.h"
#include "mtf/SSM/Isometry.h"
#include "mtf/SSM/Transcaling.h"
#include "mtf/SSM/Translation.h"
#include "mtf/SSM/LieHomography.h"
#include "mtf/SSM/CornerHomography.h"
#include "mtf/SSM/SL3.h"
#include "mtf/SSM/Spline.h"

// Third Party Trackers
#ifndef DISABLE_THIRD_PARTY_TRACKERS
// learning based trackers
#include "mtf/ThirdParty/DSST/DSST.h"
#include "mtf/ThirdParty/KCF/KCFTracker.h"
#include "mtf/ThirdParty/RCT/CompressiveTracker.h"
#include "mtf/ThirdParty/CMT/CMT.h"
#include "mtf/ThirdParty/TLD/TLD.h"
#include "mtf/ThirdParty/Struck/Struck.h"
#include "mtf/ThirdParty/MIL/MIL.h"
#include "mtf/ThirdParty/DFT/DFT.h"
#include "mtf/ThirdParty/FRG/FRG.h"
#ifndef DISABLE_PFSL3
#include "mtf/ThirdParty/PFSL3/PFSL3.h"
#endif
#ifndef DISABLE_VISP
#include "mtf/ThirdParty/ViSP//ViSP.h"
#endif
#ifndef DISABLE_XVISION
// Xvision trackers
#include "mtf/ThirdParty/Xvision/xvSSDTrans.h"
#include "mtf/ThirdParty/Xvision/xvSSDAffine.h"
#include "mtf/ThirdParty/Xvision/xvSSDSE2.h"
#include "mtf/ThirdParty/Xvision/xvSSDRT.h"
#include "mtf/ThirdParty/Xvision/xvSSDRotate.h"
#include "mtf/ThirdParty/Xvision/xvSSDScaling.h"
#include "mtf/ThirdParty/Xvision/xvSSDPyramidTrans.h"
#include "mtf/ThirdParty/Xvision/xvSSDPyramidAffine.h"
#include "mtf/ThirdParty/Xvision/xvSSDPyramidSE2.h"
#include "mtf/ThirdParty/Xvision/xvSSDPyramidRT.h"
#include "mtf/ThirdParty/Xvision/xvSSDPyramidRotate.h"
#include "mtf/ThirdParty/Xvision/xvColor.h"
#include "mtf/ThirdParty/Xvision/xvEdgeTracker.h"
#include "mtf/ThirdParty/Xvision/xvSSDGrid.h"
#include "mtf/ThirdParty/Xvision/xvSSDGridLine.h"
bool using_xv_tracker = false;
#endif
#endif

#include "boost/filesystem/operations.hpp"

_MTF_BEGIN_NAMESPACE

using namespace params;

template< class AMType, class SSMType >
TrackerBase *getTrackerObj(const char *sm_type,
	typename AMType::ParamType *am_params = nullptr,
	typename SSMType::ParamType *ssm_params = nullptr);
TrackerBase *getTrackerObj(const char *sm_type,
	const char *am_type, const char *ssm_type, const char *ilm_type = mtf_ilm);
// 3rd party trackers
TrackerBase *getTrackerObj(const char *sm_type);
nt::SearchMethod *getSM(const char *sm_type,const char *am_type,
	const char *ssm_type, const char *ilm_type = mtf_ilm);
TrackerBase *getCompositeSM(const char *sm_type,
	const char *am_type, const char *ssm_type, const char *ilm_type);
IlluminationModel *getILM(const char *ilm_type);
const PFParams* getPFParams();
const NNParams* getNNParams();
#ifndef DISABLE_REGNET
const RegNetParams* getRegNetParams();
#endif
ImageBase *getPixMapperObj(const char *pix_mapper_type, ImgParams *img_params);

//template< class AMType, class SSMType >
//TrackerBase *getFESMObj(
//	typename AMType::ParamType *am_params = nullptr,
//	typename SSMType::ParamType *ssm_params = nullptr);

template< class AMType, class SSMType >
bool getPF3(vector<SearchMethod<AMType, SSMType>*> &trackers,
	typename AMType::ParamType *am_params = nullptr,
	typename SSMType::ParamType *ssm_params = nullptr){
	if(pf3_ssm_sigma_ids.size()<3){
		printf("Insufficient sigma IDs specified for PF3: %d\n", pf3_ssm_sigma_ids.size());
		return false;
	}
	typedef SearchMethod<AMType, SSMType> SMType;	
	pf_ssm_sigma_ids = pf3_ssm_sigma_ids[0];
	trackers.push_back(dynamic_cast<SMType*>(getTrackerObj<AMType, SSMType>("pf", am_params, ssm_params)));
	pf_ssm_sigma_ids = pf3_ssm_sigma_ids[1];
	trackers.push_back(dynamic_cast<SMType*>(getTrackerObj<AMType, SSMType>("pf", am_params, ssm_params)));
	pf_ssm_sigma_ids = pf3_ssm_sigma_ids[2];
	trackers.push_back(dynamic_cast<SMType*>(getTrackerObj<AMType, SSMType>("pf", am_params, ssm_params)));
	return trackers[0] && trackers[1] && trackers[2];
}
template< class AMType, class SSMType >
bool getNN3(vector<SearchMethod<AMType, SSMType>*> &trackers,
	typename AMType::ParamType *am_params = nullptr,
	typename SSMType::ParamType *ssm_params = nullptr){
	if(nn3_ssm_sigma_ids.size()<3){
		printf("Insufficient sigma IDs specified for NN3: %d\n", nn3_ssm_sigma_ids.size());
		return false;
	}
	typedef SearchMethod<AMType, SSMType> SMType;
	nn_ssm_sigma_ids = nn3_ssm_sigma_ids[0];
	trackers.push_back(dynamic_cast<SMType*>(getTrackerObj<AMType, SSMType>("nn", am_params, ssm_params)));
	nn_ssm_sigma_ids = nn3_ssm_sigma_ids[1];
	trackers.push_back(dynamic_cast<SMType*>(getTrackerObj<AMType, SSMType>("nn", am_params, ssm_params)));
	nn_ssm_sigma_ids = nn3_ssm_sigma_ids[2];
	trackers.push_back(dynamic_cast<SMType*>(getTrackerObj<AMType, SSMType>("nn", am_params, ssm_params)));
	return trackers[0] && trackers[1] && trackers[2];
}
template< class AMType, class SSMType >
TrackerBase *getTrackerObj(const char *sm_type,
	typename AMType::ParamType *am_params = nullptr,
	typename SSMType::ParamType *ssm_params = nullptr){
#ifndef ENABLE_ONLY_NT
	typedef SearchMethod<AMType, SSMType> SMType;
	if(!strcmp(sm_type, "esm")){
		ESMParams esm_params(max_iters, epsilon,
			static_cast<ESMParams::JacType>(esm_jac_type),
			static_cast<ESMParams::HessType>(esm_hess_type),
			sec_ord_hess, spi_enable, spi_thresh, debug_mode);
		return new ESM<AMType, SSMType>(&esm_params, am_params, ssm_params);
	} else if(!strcmp(sm_type, "aesm")){
		ESMParams esm_params(max_iters, epsilon,
			static_cast<ESMParams::JacType>(esm_jac_type),
			static_cast<ESMParams::HessType>(esm_hess_type),
			sec_ord_hess, spi_enable, spi_thresh, debug_mode);
		return new AESM<AMType, SSMType>(&esm_params, am_params, ssm_params);
	} else if(!strcmp(sm_type, "iclk") || !strcmp(sm_type, "ic")){
		ICLKParams iclk_params(max_iters, epsilon,
			static_cast<ICLKParams::HessType>(ic_hess_type), sec_ord_hess,
			ic_update_ssm, ic_chained_warp, debug_mode);
		return new ICLK<AMType, SSMType>(&iclk_params, am_params, ssm_params);
	} else if(!strcmp(sm_type, "fclk") || !strcmp(sm_type, "fc")){
		FCLKParams fclk_params(max_iters, epsilon,
			static_cast<FCLKParams::HessType>(fc_hess_type), sec_ord_hess,
			update_templ, fc_chained_warp, fc_leven_marq, fc_lm_delta_init,
			fc_lm_delta_update, fc_write_ssm_updates, fc_show_grid, 
			fc_show_patch, fc_patch_resize_factor, fc_debug_mode);
		return new FCLK<AMType, SSMType>(&fclk_params, am_params, ssm_params);
	} else if(!strcmp(sm_type, "falk") || !strcmp(sm_type, "fa")){
		FALKParams falk_params(max_iters, epsilon,
			static_cast<FALKParams::HessType>(fa_hess_type), sec_ord_hess,
			fa_show_grid, fa_show_patch, fa_patch_resize_factor,
			fa_write_frames, update_templ, debug_mode);
		return new FALK<AMType, SSMType>(&falk_params, am_params, ssm_params);
	} else if(!strcmp(sm_type, "ialk") || !strcmp(sm_type, "ia")){
		IALKParams ialk_params(max_iters, epsilon,
			static_cast<IALKParams::HessType>(ia_hess_type), sec_ord_hess,
			debug_mode);
		return new IALK<AMType, SSMType>(&ialk_params, am_params, ssm_params);
	} else if(!strcmp(sm_type, "fcgd")){
		FCGDParams fcgd_params(max_iters, epsilon, gd_learning_rate,
			debug_mode, fc_hess_type);
		return new FCGD<AMType, SSMType>(&fcgd_params, am_params, ssm_params);
	} 
	//! Particle Filter tracker
	else if(!strcmp(sm_type, "pf")){
		return new PF<AMType, SSMType>(getPFParams(), am_params, ssm_params);
	} else if(!strcmp(sm_type, "pfic")){// PF + ICLK
		vector<SMType*> trackers;
		trackers.push_back(dynamic_cast<SMType*>(getTrackerObj<AMType, SSMType>("pf", am_params, ssm_params)));
		trackers.push_back(dynamic_cast<SMType*>(getTrackerObj<AMType, SSMType>("ic", am_params, ssm_params)));
		CascadeSMParams casc_params(casc_enable_feedback);
		return new CascadeSM<AMType, SSMType>(trackers, &casc_params);
	} else if(!strcmp(sm_type, "pffc")){// PF + FCLK
		vector<SMType*> trackers;
		trackers.push_back(dynamic_cast<SMType*>(getTrackerObj<AMType, SSMType>("pf", am_params, ssm_params)));
		trackers.push_back(dynamic_cast<SMType*>(getTrackerObj<AMType, SSMType>("fc", am_params, ssm_params)));
		CascadeSMParams casc_params(casc_enable_feedback);
		return new CascadeSM<AMType, SSMType>(trackers, &casc_params);
	} else if(!strcmp(sm_type, "pfes")){// PF + ESM
		vector<SMType*> trackers;
		trackers.push_back(dynamic_cast<SMType*>(getTrackerObj<AMType, SSMType>("pf", am_params, ssm_params)));
		trackers.push_back(dynamic_cast<SMType*>(getTrackerObj<AMType, SSMType>("esm", am_params, ssm_params)));
		CascadeSMParams casc_params(casc_enable_feedback);
		return new CascadeSM<AMType, SSMType>(trackers, &casc_params);
	} else if(!strcmp(sm_type, "pf3")){
		vector<SMType*> trackers;
		if(!getPF3(trackers, am_params, ssm_params)){ return nullptr; }
		CascadeSMParams casc_params(casc_enable_feedback);
		return new CascadeSM<AMType, SSMType>(trackers, &casc_params);
	} else if(!strcmp(sm_type, "pf3ic")){// PF3 + ICLK
		vector<SMType*> trackers;
		if(!getPF3(trackers, am_params, ssm_params)){ return nullptr; }
		trackers.push_back(dynamic_cast<SMType*>(getTrackerObj<AMType, SSMType>("ic", am_params, ssm_params)));
		CascadeSMParams casc_params(casc_enable_feedback);
		return new CascadeSM<AMType, SSMType>(trackers, &casc_params);
	} else if(!strcmp(sm_type, "pf3fc")){// PF3 + ICLK
		vector<SMType*> trackers;
		if(!getPF3(trackers, am_params, ssm_params)){ return nullptr; }
		trackers.push_back(dynamic_cast<SMType*>(getTrackerObj<AMType, SSMType>("fc", am_params, ssm_params)));
		CascadeSMParams casc_params(casc_enable_feedback);
		return new CascadeSM<AMType, SSMType>(trackers, &casc_params);
	} else if(!strcmp(sm_type, "pf3es")){// PF3 + ESM
		vector<SMType*> trackers;
		if(!getPF3(trackers, am_params, ssm_params)){ return nullptr; }
		trackers.push_back(dynamic_cast<SMType*>(getTrackerObj<AMType, SSMType>("esm", am_params, ssm_params)));
		CascadeSMParams casc_params(casc_enable_feedback);
		return new CascadeSM<AMType, SSMType>(trackers, &casc_params);
	}
#ifndef DISABLE_NN
	// NN tracker
	else if(!strcmp(sm_type, "nn")){
		return new NN<AMType, SSMType>(getNNParams(), am_params, ssm_params);
	} else if(!strcmp(sm_type, "gnn")){// Graph based NN
		nn_index_type = 0;
		return getTrackerObj<AMType, SSMType>("nn", am_params, ssm_params);
	} else if(!strcmp(sm_type, "nnic")){// NNIC with 1 layer of NN		
		vector<SMType*> trackers;
		trackers.push_back(dynamic_cast<SMType*>(getTrackerObj<AMType, SSMType>("nn", am_params, ssm_params)));
		trackers.push_back(dynamic_cast<SMType*>(getTrackerObj<AMType, SSMType>("ic", am_params, ssm_params)));
		CascadeSMParams casc_params(casc_enable_feedback);
		return new CascadeSM<AMType, SSMType>(trackers, &casc_params);
	} else if(!strcmp(sm_type, "nnfc")){
		vector<SMType*> trackers;
		trackers.push_back(dynamic_cast<SMType*>(getTrackerObj<AMType, SSMType>("nn", am_params, ssm_params)));
		trackers.push_back(dynamic_cast<SMType*>(getTrackerObj<AMType, SSMType>("fc", am_params, ssm_params)));
		CascadeSMParams casc_params(casc_enable_feedback);
		return new CascadeSM<AMType, SSMType>(trackers, &casc_params);
	} else if(!strcmp(sm_type, "nnes")){
		vector<SMType*> trackers;
		trackers.push_back(dynamic_cast<SMType*>(getTrackerObj<AMType, SSMType>("nn", am_params, ssm_params)));
		trackers.push_back(dynamic_cast<SMType*>(getTrackerObj<AMType, SSMType>("esm", am_params, ssm_params)));
		CascadeSMParams casc_params(casc_enable_feedback);
		return new CascadeSM<AMType, SSMType>(trackers, &casc_params);
	} else if(!strcmp(sm_type, "nnrk")){
		vector<TrackerBase*> trackers;
		trackers.push_back(dynamic_cast<SMType*>(getTrackerObj<AMType, SSMType>("nn", am_params, ssm_params)));
		trackers.push_back(dynamic_cast<SMType*>(getTrackerObj<AMType, SSMType>("rkl", am_params, ssm_params)));
		CascadeParams casc_params(casc_enable_feedback);
		return new CascadeTracker(trackers, &casc_params);
	} else if(!strcmp(sm_type, "nn3")){// 3 layers of NN
		vector<SMType*> trackers;
		if(!getNN3(trackers, am_params, ssm_params)){ return nullptr; }
		CascadeSMParams casc_params(casc_enable_feedback);
		return new CascadeSM<AMType, SSMType>(trackers, &casc_params);
	} else if(!strcmp(sm_type, "nn3ic")){// NNIC with 3 layers of NN
		vector<SMType*> trackers;
		if(!getNN3(trackers, am_params, ssm_params)){ return nullptr; }
		trackers.push_back(dynamic_cast<SMType*>(getTrackerObj<AMType, SSMType>("ic", am_params, ssm_params)));
		CascadeSMParams casc_params(casc_enable_feedback);
		return new CascadeSM<AMType, SSMType>(trackers, &casc_params);
	} else if(!strcmp(sm_type, "nn3fc")){
		vector<SMType*> trackers;
		if(!getNN3(trackers, am_params, ssm_params)){ return nullptr; }
		trackers.push_back(dynamic_cast<SMType*>(getTrackerObj<AMType, SSMType>("fc", am_params, ssm_params)));
		CascadeSMParams casc_params(casc_enable_feedback);
		return new CascadeSM<AMType, SSMType>(trackers, &casc_params);
	} else if(!strcmp(sm_type, "nn3es")){
		vector<SMType*> trackers;
		if(!getNN3(trackers, am_params, ssm_params)){ return nullptr; }
		trackers.push_back(dynamic_cast<SMType*>(getTrackerObj<AMType, SSMType>("esm", am_params, ssm_params)));
		CascadeSMParams casc_params(casc_enable_feedback);
		return new CascadeSM<AMType, SSMType>(trackers, &casc_params);
	}
#endif
	//else if(!strcmp(sm_type, "haclk")){
	//	HACLKParams *haclk_params = new HACLKParams(max_iters, epsilon, rec_init_err_grad, 
	//		debug_mode, hess_type, conv_corners);
	//	return new HACLK<AMType, SSMType>(haclk_params, am_params, ssm_params);
	//} 	
	else if(!strcmp(sm_type, "casm")){// cascade of search methods
		FILE *fid = nullptr;
		vector<SMType*> trackers;
		trackers.resize(casc_n_trackers);
		for(int i = 0; i < casc_n_trackers; i++){
			fid = readTrackerParams(fid, 1);
			if(!(trackers[i] = dynamic_cast<SMType*>(getTrackerObj(mtf_sm, mtf_am, mtf_ssm)))){
				printf("Invalid search method provided for cascade SM tracker: %s\n", mtf_sm);
				return nullptr;
			}
		}
		CascadeSMParams casc_params(casc_enable_feedback);
		return new CascadeSM<AMType, SSMType>(trackers, &casc_params);
	} else if(!strcmp(sm_type, "hrch")){ // hierarchical SSM tracker
		vector<TrackerBase*> trackers;
		trackers.push_back(mtf::getTrackerObj(hrch_sm, hrch_am, "2"));
		//trackers.push_back(mtf::getTrackerObj(hrch_sm, hrch_am, "3"));
		trackers.push_back(mtf::getTrackerObj(hrch_sm, hrch_am, "4"));
		trackers.push_back(mtf::getTrackerObj(hrch_sm, hrch_am, "6"));
		trackers.push_back(mtf::getTrackerObj(hrch_sm, hrch_am, "8"));

		CascadeParams casc_params(casc_enable_feedback);
		return new CascadeTracker(trackers, &casc_params);
	}
	// Grid Tracker
	else if(!strcmp(sm_type, "grid")){
		if(!strcmp(grid_sm, "cv")){
			GridTrackerCVParams grid_params(
				grid_res, grid_res, grid_patch_size, grid_patch_size,
				grid_pyramid_levels, grid_use_min_eig_vals, grid_min_eig_thresh,
				static_cast<GridTrackerCVParams::EstType>(grid_estimation_method),
				grid_ransac_reproj_thresh, max_iters, epsilon, grid_show_trackers,
				debug_mode);
			return new GridTrackerCV<SSMType>(&grid_params, ssm_params);
		} else{
			vector<TrackerBase*> trackers;
			int grid_n_trackers = grid_res*grid_res;
			trackers.resize(grid_n_trackers);
			int resx_back = resx, resy_back = resy;
			resx = resy = grid_patch_size;
			for(int i = 0; i < grid_n_trackers; i++){
				if(!(trackers[i] = getTrackerObj(grid_sm, grid_am, grid_ssm))){
					return nullptr;
				}
			}
			resx = resx_back;
			resy = resy_back;
			GridTrackerParams grid_params(
				grid_res, grid_res, grid_patch_size, grid_patch_size,
				static_cast<GridTrackerParams::EstType>(grid_estimation_method),
				grid_ransac_reproj_thresh, grid_init_at_each_frame,
				grid_dyn_patch_size, grid_use_tbb, max_iters, epsilon,
				!strcmp(grid_sm, "pyr"),
				grid_show_trackers, grid_show_tracker_edges, debug_mode);
			return new GridTracker<SSMType>(trackers, &grid_params, ssm_params);
		}
	} else if(!strcmp(sm_type, "gric")){// Grid + ICLK
		vector<TrackerBase*> trackers;
		trackers.push_back(getTrackerObj<AMType, SSMType>("grid", am_params, ssm_params));
		trackers.push_back(getTrackerObj<AMType, SSMType>("iclk", am_params, ssm_params));
		CascadeParams * casc_params = new CascadeParams(casc_enable_feedback);
		return new CascadeTracker(trackers, casc_params);
	} else if(!strcmp(sm_type, "grfc")){// Grid + FCLK
		vector<TrackerBase*> trackers;
		trackers.push_back(getTrackerObj<AMType, SSMType>("grid", am_params, ssm_params));
		trackers.push_back(getTrackerObj<AMType, SSMType>("fclk", am_params, ssm_params));
		CascadeParams * casc_params = new CascadeParams(casc_enable_feedback);
		return new CascadeTracker(trackers, casc_params);
	} else if(!strcmp(sm_type, "gres")){// Grid + ESM
		vector<TrackerBase*> trackers;
		trackers.push_back(getTrackerObj<AMType, SSMType>("grid", am_params, ssm_params));
		trackers.push_back(getTrackerObj<AMType, SSMType>("esm", am_params, ssm_params));
		CascadeParams * casc_params = new CascadeParams(casc_enable_feedback);
		return new CascadeTracker(trackers, casc_params);
	} else if(!strcmp(sm_type, "rklt") || !strcmp(sm_type, "rkl")){// Grid + Template tracker with SPI
		GridBase *grid_tracker = dynamic_cast<GridBase*>(getTrackerObj<AMType, SSMType>("grid", am_params, ssm_params));
		//if(rkl_enable_spi){
		//	printf("Setting sampling resolution of the template tracker equal to the grid size: %d x %d so that SPI can be enabled.",
		//		grid_tracker->getResX(), grid_tracker->getResY());
		//	am_params->resx = grid_tracker->getResX();
		//	am_params->resy = grid_tracker->getResY();
		//}
		SMType *templ_tracker = dynamic_cast<SMType*>(getTrackerObj<AMType, SSMType>(rkl_sm, am_params, ssm_params));
		if(!templ_tracker){
			// invalid or third party tracker has been specified in 'rkl_sm'
			throw std::invalid_argument("Search method provided is not compatible with RKLT");
		}
		RKLTParams rkl_params(rkl_enable_spi, rkl_enable_feedback,
			rkl_failure_detection, rkl_failure_thresh, debug_mode);
		return new RKLT<AMType, SSMType>(&rkl_params, grid_tracker, templ_tracker);
	} else if(!strcmp(sm_type, "prls") || !strcmp(sm_type, "prsm")) {// SM specific parallel tracker
		vector<SMType*> trackers;
		trackers.resize(prl_n_trackers);
		//char prl_am[strlen(mtf_am) + 1], prl_ssm[strlen(mtf_ssm) + 1];
		//strcpy(mtf_am, prl_am);
		//strcpy(mtf_ssm, prl_ssm);
		FILE *fid = nullptr;
		for(int tracker_id = 0; tracker_id < prl_n_trackers; tracker_id++) {
			fid = readTrackerParams(fid, 1);
			if(am_params){
				am_params->resx = resx;
				am_params->resy = resy;
				am_params->grad_eps = grad_eps;
				am_params->hess_eps = hess_eps;
			}
			if(!(trackers[tracker_id] = dynamic_cast<SMType*>(getTrackerObj<AMType, SSMType>(mtf_sm, am_params, ssm_params)))) {
				printf("Invalid search method provided for parallel SM tracker: %s\n", mtf_sm);
				return nullptr;
			}
		}
		ParallelSMParams prl_params(
			static_cast<ParallelSMParams::EstimationMethod>(prl_estimation_method),
			prl_reset_to_mean, prl_auto_reinit, prl_reinit_err_thresh, prl_reinit_frame_gap);
		return new ParallelSM<AMType, SSMType>(trackers, &prl_params, resx, resy, ssm_params);
	}
	//else if(!strcmp(sm_type, "esmh")){
	//	ESMHParams *esm_params = new ESMHParams(max_iters, epsilon,
	//		static_cast<ESMHParams::JacType>(jac_type),
	//		static_cast<ESMHParams::HessType>(hess_type),
	//		sec_ord_hess, spi_enable, spi_thresh, debug_mode);
	//	return new ESMH<AMType, SSMType>(esm_params, am_params, ssm_params);
	//}
	//else if(!strcmp(sm_type, "ialk2")){
	//	IALK2Params *ialk_params = new IALK2Params(max_iters, epsilon,
	//		static_cast<IALK2Params::HessType>(hess_type), sec_ord_hess, debug_mode);
	//	return new IALK2<AMType, SSMType>(ialk_params, am_params, ssm_params);
	//} else if(!strcmp(sm_type, "fesm")){
	//	return getFESMObj<AMType, SSMType>(am_params, ssm_params);
	//}
#endif
	if(!strcmp(sm_type, "casc")){// general purpose cascaded tracker 
		FILE *fid = nullptr;
		vector<TrackerBase*> trackers;
		trackers.resize(casc_n_trackers);
		for(int i = 0; i < casc_n_trackers; i++){
			fid = readTrackerParams(fid, 1);
			if(!(trackers[i] = getTrackerObj(mtf_sm, mtf_am, mtf_ssm))){
				return nullptr;
			}
		}
		CascadeParams casc_params(casc_enable_feedback);
		return new CascadeTracker(trackers, &casc_params);
	}
	// Parallel Tracker
	else if(!strcmp(sm_type, "prl") || !strcmp(sm_type, "prlt")) { // general purpose parallel tracker 
		vector<TrackerBase*> trackers;
		trackers.resize(prl_n_trackers);
		FILE *fid = nullptr;
		for(int tracker_id = 0; tracker_id < prl_n_trackers; tracker_id++) {
			fid = readTrackerParams(fid, 1);
			if(!(trackers[tracker_id] = getTrackerObj(mtf_sm, mtf_am, mtf_ssm))) {
				return nullptr;
			}
		}
		ParallelParams prl_params(
			static_cast<ParallelParams::EstimationMethod>(prl_estimation_method),
			prl_reset_to_mean, prl_auto_reinit, prl_reinit_err_thresh, prl_reinit_frame_gap);
		return new ParallelTracker(trackers, &prl_params);
	} else if(!strcmp(sm_type, "pyr") || !strcmp(sm_type, "pyrt")) { // pyramidal tracker
		PyramidalParams pyr_params(pyr_no_of_levels, pyr_scale_factor, pyr_show_levels);
		vector<TrackerBase*> trackers;
		trackers.resize(pyr_params.no_of_levels);
		for(int tracker_id = 0; tracker_id < pyr_params.no_of_levels; tracker_id++) {
			if(enable_nt){
				trackers[tracker_id] = getTrackerObj(pyr_sm.c_str(), mtf_am, mtf_ssm);
			} else{
				trackers[tracker_id] = getTrackerObj<AMType, SSMType>(pyr_sm.c_str(), am_params, ssm_params);
			}
			if(!trackers[tracker_id]){ return nullptr; }

		}
		return new PyramidalTracker(trackers, &pyr_params);
	} else if(!strcmp(sm_type, "line")){
		vector<TrackerBase*> trackers;
		int line_n_trackers = line_grid_size*line_grid_size;
		trackers.resize(line_n_trackers);
		int resx_back = resx, resy_back = resy;
		resx = resy = line_patch_size;
		for(int tracker_id = 0; tracker_id < line_n_trackers; tracker_id++){
			if(!(trackers[tracker_id] = getTrackerObj(line_sm, line_am, line_ssm))){
				return nullptr;
			}
		}
		resx = resx_back;
		resy = resy_back;
		LineTrackerParams line_params(line_grid_size, line_grid_size,
			line_patch_size, line_use_constant_slope, line_use_ls,
			line_inter_alpha_thresh, line_intra_alpha_thresh,
			line_reset_pos, line_reset_template, line_debug_mode);
		return new LineTracker(trackers, &line_params);
	}
	printf("Invalid search method provided: %s\n", sm_type);
	return nullptr;

	//if(am_params){ delete(am_params); }
	//if(ssm_params){ delete(ssm_params); }
}

template< class AMType >
TrackerBase *getTrackerObj(const char *sm_type, const char *ssm_type,
	typename AMType::ParamType *am_params = nullptr){
	if(!strcmp(ssm_type, "lie_hom") || !strcmp(ssm_type, "l8")){
		LieHomographyParams lhomm_params(lhom_normalized_init, debug_mode);
		return getTrackerObj<AMType, LieHomography>(sm_type, am_params, &lhomm_params);
	} else if(!strcmp(ssm_type, "chom") || !strcmp(ssm_type, "c8")){
		CornerHomographyParams chom_params(chom_normalized_init, chom_grad_eps, debug_mode);
		return getTrackerObj<AMType, mtf::CornerHomography>(sm_type, am_params, &chom_params);
	} else if(!strcmp(ssm_type, "sl3")){
		SL3Params sl3_params(sl3_normalized_init, sl3_iterative_sample_mean,
			sl3_sample_mean_max_iters, sl3_sample_mean_eps, debug_mode);
		return getTrackerObj<AMType, mtf::SL3>(sm_type, am_params, &sl3_params);
	} else if(!strcmp(ssm_type, "hom") || !strcmp(ssm_type, "8")){
		HomographyParams hom_params(hom_normalized_init, hom_corner_based_sampling, debug_mode);
		return getTrackerObj<AMType, mtf::Homography>(sm_type, am_params, &hom_params);
	} else if(!strcmp(ssm_type, "aff") || !strcmp(ssm_type, "6")){
		AffineParams aff_params(aff_normalized_init, debug_mode);
		return getTrackerObj<AMType, mtf::Affine>(sm_type, am_params, &aff_params);
	} else if(!strcmp(ssm_type, "sim") || !strcmp(ssm_type, "4")){
		return getTrackerObj<AMType, mtf::Similitude>(sm_type, am_params);
	} else if(!strcmp(ssm_type, "iso") || !strcmp(ssm_type, "3")){
		return getTrackerObj<AMType, mtf::Isometry>(sm_type, am_params);
	} else if(!strcmp(ssm_type, "trs") || !strcmp(ssm_type, "3s")){
		return getTrackerObj<AMType, mtf::Transcaling>(sm_type, am_params);
	} else if(!strcmp(ssm_type, "trans") || !strcmp(ssm_type, "2")){
		return getTrackerObj<AMType, mtf::Translation>(sm_type, am_params);
	} else if(!strcmp(ssm_type, "spline") || !strcmp(ssm_type, "spl")){
		SplineParams spl_params(spl_control_size, spl_control_size, spl_control_overlap,
			static_cast<SplineParams::InterpolationType>(spl_interp_type),
			spl_static_wts, spl_debug_mode);
		return getTrackerObj<AMType, mtf::Spline>(sm_type, am_params, &spl_params);
	} else{
		printf("Invalid state space model provided: %s\n", ssm_type);
		return nullptr;
	}
}
inline TrackerBase *getTrackerObj(const char *sm_type, const char *am_type, 
	const char *ssm_type, const char *ilm_type){

	// check for 3rd party trackers
	TrackerBase *third_party_tracker = getTrackerObj(sm_type);
	if(third_party_tracker)
		return third_party_tracker;
	// check if Non templated variant is needed
	if(enable_nt){
		TrackerBase *nt_tracker = getSM(sm_type, am_type, ssm_type, ilm_type);
		if(nt_tracker){ return nt_tracker; }
		nt_tracker = getCompositeSM(sm_type, am_type, ssm_type, ilm_type);
		if(nt_tracker){ return nt_tracker; }	
	}
	// parameters common to all AMs
	mtf::ImgParams img_params(resx, resy, grad_eps, hess_eps, getILM(ilm_type));
	if(!strcmp(am_type, "ssd")){
		return getTrackerObj<SSD>(sm_type, ssm_type, &img_params);
	} else if(!strcmp(am_type, "nssd")){
		NSSDParams nssd_params(&img_params, norm_pix_max, norm_pix_min, debug_mode);
		return getTrackerObj<NSSD>(sm_type, ssm_type, &nssd_params);
	} else if(!strcmp(am_type, "zncc")){
		return getTrackerObj<ZNCC>(sm_type, ssm_type, &img_params);
	} else if(!strcmp(am_type, "scv")){
		SCVParams scv_params(&img_params, scv_use_bspl, scv_n_bins, scv_preseed, scv_pou,
			scv_weighted_mapping, scv_mapped_gradient, scv_approx_dist_feat, debug_mode);
		return getTrackerObj<SCV>(sm_type, ssm_type, &scv_params);
	} else if(!strcmp(am_type, "lscv")){
		LSCVParams lscv_params(&img_params, lscv_sub_regions, lscv_sub_regions,
			lscv_spacing, lscv_spacing, scv_affine_mapping, scv_once_per_frame, scv_n_bins, scv_preseed,
			scv_weighted_mapping, lscv_show_subregions, debug_mode);
		return getTrackerObj<LSCV>(sm_type, ssm_type, &lscv_params);
	} else if(!strcmp(am_type, "rscv")){
		RSCVParams rscv_params(&img_params, scv_use_bspl, scv_n_bins, scv_preseed, scv_pou,
			scv_weighted_mapping, scv_mapped_gradient, scv_approx_dist_feat, debug_mode);
		return getTrackerObj<RSCV>(sm_type, ssm_type, &rscv_params);
	} else if(!strcmp(am_type, "lrscv") || !strcmp(am_type, "lrsc")){
		LRSCVParams lscv_params(&img_params, lscv_sub_regions, lscv_sub_regions,
			lscv_spacing, lscv_spacing, scv_affine_mapping, scv_once_per_frame, scv_n_bins, scv_preseed,
			scv_weighted_mapping, lscv_show_subregions, debug_mode);
		return getTrackerObj<LRSCV>(sm_type, ssm_type, &lscv_params);
	} else if(!strcmp(am_type, "kld")){
		KLDParams kld_params(&img_params, mi_n_bins, mi_pre_seed, mi_pou, debug_mode);
		return getTrackerObj<KLD>(sm_type, ssm_type, &kld_params);
	} else if(!strcmp(am_type, "lkld")){
		LKLDParams lkld_params(&img_params, lkld_sub_regions, lkld_sub_regions,
			lkld_spacing, lkld_spacing, lkld_n_bins, lkld_pre_seed, lkld_pou, debug_mode);
		return getTrackerObj<LKLD>(sm_type, ssm_type, &lkld_params);
	} else if(!strcmp(am_type, "mi")){
		MIParams mi_params(&img_params, mi_n_bins, mi_pre_seed, mi_pou,
			getPixMapperObj(pix_mapper, &img_params), debug_mode);
		return getTrackerObj<MI>(sm_type, ssm_type, &mi_params);
	} else if(!strcmp(am_type, "spss")){
		SPSSParams spss_params(&img_params, ssim_k1,
			getPixMapperObj(pix_mapper, &img_params));
		return getTrackerObj<SPSS>(sm_type, ssm_type, &spss_params);
	} else if(!strcmp(am_type, "ssim")){
		SSIMParams ssim_params(&img_params, ssim_k1, ssim_k2);
		return getTrackerObj<SSIM>(sm_type, ssm_type, &ssim_params);
	} else if(!strcmp(am_type, "ncc")){
		NCCParams ncc_params(&img_params, ncc_fast_hess);
		return getTrackerObj<NCC>(sm_type, ssm_type, &ncc_params);
	} else if(!strcmp(am_type, "ccre")){
		CCREParams ccre_params(&img_params, ccre_n_bins, ccre_pou, ccre_pre_seed,
			ccre_symmetrical_grad, ccre_n_blocks, debug_mode);
		return getTrackerObj<CCRE>(sm_type, ssm_type, &ccre_params);
	} else if(!strcmp(am_type, "mcssd") || !strcmp(am_type, "ssd3")){
		return getTrackerObj<MCSSD>(sm_type, ssm_type, &img_params);
	} else if(!strcmp(am_type, "mcncc") || !strcmp(am_type, "ncc3")){
		NCCParams ncc_params(&img_params, ncc_fast_hess);
		return getTrackerObj<MCNCC>(sm_type, ssm_type, &ncc_params);
	} else if(!strcmp(am_type, "mcmi") || !strcmp(am_type, "mi3")){
		MIParams mi_params(&img_params, mi_n_bins, mi_pre_seed, mi_pou,
			getPixMapperObj(pix_mapper, &img_params), debug_mode);
		return getTrackerObj<MCMI>(sm_type, ssm_type, &mi_params);
	} else if(!strcmp(am_type, "mczncc") || !strcmp(am_type, "zncc3")){
		return getTrackerObj<MCZNCC>(sm_type, ssm_type, &img_params);
	} else if(!strcmp(am_type, "mcscv") || !strcmp(am_type, "scv3")){
		SCVParams scv_params(&img_params, scv_use_bspl, scv_n_bins, scv_preseed, scv_pou,
			scv_weighted_mapping, scv_mapped_gradient, scv_approx_dist_feat, debug_mode);
		return getTrackerObj<MCSCV>(sm_type, ssm_type, &scv_params);
	} else if(!strcmp(am_type, "mcrscv") || !strcmp(am_type, "rscv3")){
		RSCVParams rscv_params(&img_params, scv_use_bspl, scv_n_bins, scv_preseed, scv_pou,
			scv_weighted_mapping, scv_mapped_gradient, scv_approx_dist_feat, debug_mode);
		return getTrackerObj<MCRSCV>(sm_type, ssm_type, &rscv_params);
	} else if(!strcmp(am_type, "mcssim") || !strcmp(am_type, "ssim3")){
		SSIMParams ssim_params(&img_params, ssim_k1, ssim_k2);
		return getTrackerObj<MCSSIM>(sm_type, ssm_type, &ssim_params);
	} else if(!strcmp(am_type, "mcspss") || !strcmp(am_type, "spss3")){
		SPSSParams spss_params(&img_params, ssim_k1,
			getPixMapperObj(pix_mapper, &img_params));
		return getTrackerObj<MCSPSS>(sm_type, ssm_type, &spss_params);
	} else if(!strcmp(am_type, "mcccre") || !strcmp(am_type, "ccre3")){
		CCREParams ccre_params(&img_params, ccre_n_bins, ccre_pou, ccre_pre_seed,
			ccre_symmetrical_grad, ccre_n_blocks, debug_mode);
		return getTrackerObj<MCCCRE>(sm_type, ssm_type, &ccre_params);
	}
#ifndef DISABLE_FMAPS
	else if(!strcmp(am_type, "fmaps")){
		FMapsParams fmaps_params(&img_params, fmaps_nfmaps, fmaps_layer_name,
			fmaps_vis, fmaps_zncc, fmaps_model_f_name, fmaps_mean_f_name, fmaps_params_f_name);
		return getTrackerObj<FMaps>(sm_type, ssm_type, &fmaps_params);
	}
#endif	
#ifndef DISABLE_PCA
	else if(!strcmp(am_type, "pca")){
		PCAParams pca_params(&img_params, db_root_path, actor,
			source_name, extraction_id, pca_n_feat, pca_len_history);
		return getTrackerObj<PCA>(sm_type, ssm_type, &pca_params);
	}
#endif	
	else{
		printf("Invalid appearance model provided: %s\n", am_type);
		return nullptr;
	}
}

inline StateSpaceModel *getSSM(const char *ssm_type){
	if(!strcmp(ssm_type, "lie_hom") || !strcmp(ssm_type, "l8")){
		LieHomographyParams lhomm_params(lhom_normalized_init, debug_mode);
		return new LieHomography(resx, resy, &lhomm_params);
	} else if(!strcmp(ssm_type, "chom") || !strcmp(ssm_type, "c8")){
		CornerHomographyParams chom_params(chom_normalized_init, chom_grad_eps, debug_mode);
		return new mtf::CornerHomography(resx, resy, &chom_params);
	} else if(!strcmp(ssm_type, "sl3")){
		SL3Params sl3_params(sl3_normalized_init, sl3_iterative_sample_mean,
			sl3_sample_mean_max_iters, sl3_sample_mean_eps, sl3_debug_mode);
		return new mtf::SL3(resx, resy, &sl3_params);
	} else if(!strcmp(ssm_type, "hom") || !strcmp(ssm_type, "8")){
		HomographyParams hom_params(hom_normalized_init, hom_corner_based_sampling, debug_mode);
		return new mtf::Homography(resx, resy, &hom_params);
	} else if(!strcmp(ssm_type, "aff") || !strcmp(ssm_type, "6")){
		AffineParams aff_params(aff_normalized_init, debug_mode);
		return new mtf::Affine(resx, resy, &aff_params);
	} else if(!strcmp(ssm_type, "sim") || !strcmp(ssm_type, "4")){
		return new mtf::Similitude(resx, resy);
	} else if(!strcmp(ssm_type, "iso") || !strcmp(ssm_type, "3")){
		return new mtf::Isometry(resx, resy);
	} else if(!strcmp(ssm_type, "trs") || !strcmp(ssm_type, "3s")){
		return new mtf::Transcaling(resx, resy);
	} else if(!strcmp(ssm_type, "trans") || !strcmp(ssm_type, "2")){
		return new mtf::Translation(resx, resy);
	} else if(!strcmp(ssm_type, "spline") || !strcmp(ssm_type, "spl")){
		SplineParams spl_params(spl_control_size, spl_control_size, spl_control_overlap,
			static_cast<SplineParams::InterpolationType>(spl_interp_type),
			spl_static_wts, spl_debug_mode);
		return new mtf::Spline(resx, resy, &spl_params);
	} else{
		printf("Invalid state space model provided: %s\n", ssm_type);
		return nullptr;
	}
}
inline IlluminationModel *getILM(const char *ilm_type = mtf_ilm){
	if(!strcmp(ilm_type, "gb")){
		GBParams gb_params(gb_additive_update);
		return new GB(&gb_params);
	} else{
		return nullptr;
	}
}
inline AppearanceModel *getAM(const char *am_type, const char *ilm_type = mtf_ilm){
	ImgParams img_params(resx, resy, grad_eps, hess_eps, getILM(ilm_type));
	if(!strcmp(am_type, "ssd")){
		return new SSD(&img_params);
	} else if(!strcmp(am_type, "nssd")){
		NSSDParams nssd_params(&img_params, norm_pix_max, norm_pix_min, debug_mode);
		return new NSSD(&nssd_params);
	} else if(!strcmp(am_type, "zncc")){
		return new ZNCC(&img_params);
	} else if(!strcmp(am_type, "scv")){
		SCVParams scv_params(&img_params, scv_use_bspl, scv_n_bins, scv_preseed, scv_pou,
			scv_weighted_mapping, scv_mapped_gradient, scv_approx_dist_feat, debug_mode);
		return new SCV(&scv_params);
	} else if(!strcmp(am_type, "lscv")){
		LSCVParams lscv_params(&img_params, lscv_sub_regions, lscv_sub_regions,
			lscv_spacing, lscv_spacing, scv_affine_mapping, scv_once_per_frame, scv_n_bins, scv_preseed,
			scv_weighted_mapping, lscv_show_subregions, debug_mode);
		return new LSCV(&lscv_params);
	} else if(!strcmp(am_type, "rscv")){
		RSCVParams rscv_params(&img_params, scv_use_bspl, scv_n_bins, scv_preseed, scv_pou,
			scv_weighted_mapping, scv_mapped_gradient, scv_approx_dist_feat, debug_mode);
		return new RSCV(&rscv_params);
	} else if(!strcmp(am_type, "lrscv") || !strcmp(am_type, "lrsc")){
		LRSCVParams *lscv_params = new LRSCVParams(&img_params, lscv_sub_regions, lscv_sub_regions,
			lscv_spacing, lscv_spacing, scv_affine_mapping, scv_once_per_frame, scv_n_bins, scv_preseed,
			scv_weighted_mapping, lscv_show_subregions, debug_mode);
		return new LRSCV(lscv_params);
	} else if(!strcmp(am_type, "kld")){
		KLDParams kld_params(&img_params, mi_n_bins, mi_pre_seed, mi_pou, debug_mode);
		return new KLD(&kld_params);
	} else if(!strcmp(am_type, "lkld")){
		LKLDParams lkld_params(&img_params, lkld_sub_regions, lkld_sub_regions,
			lkld_spacing, lkld_spacing, lkld_n_bins, lkld_pre_seed, lkld_pou, debug_mode);
		return new LKLD(&lkld_params);
	} else if(!strcmp(am_type, "mi")){
		MIParams mi_params(&img_params, mi_n_bins, mi_pre_seed, mi_pou,
			getPixMapperObj(pix_mapper, &img_params), debug_mode);
		return new MI(&mi_params);
	} else if(!strcmp(am_type, "spss")){
		SPSSParams spss_params(&img_params, ssim_k1,
			getPixMapperObj(pix_mapper, &img_params));
		return new SPSS(&spss_params);
	} else if(!strcmp(am_type, "ssim")){
		SSIMParams ssim_params(&img_params, ssim_k1, ssim_k2);
		return new SSIM(&ssim_params);
	} else if(!strcmp(am_type, "ncc")){
		NCCParams ncc_params(&img_params, ncc_fast_hess);
		return new NCC(&ncc_params);
	} else if(!strcmp(am_type, "ccre")){
		CCREParams ccre_params(&img_params, ccre_n_bins, ccre_pou, ccre_pre_seed,
			ccre_symmetrical_grad, ccre_n_blocks, debug_mode);
		return new CCRE(&ccre_params);
	} 

	else if(!strcmp(am_type, "mcssd") || !strcmp(am_type, "ssd3")){
		return new MCSSD(&img_params);
	} else if(!strcmp(am_type, "mczncc") || !strcmp(am_type, "zncc3")){
		return new MCZNCC(&img_params);
	} else if(!strcmp(am_type, "mcscv") || !strcmp(am_type, "scv3")){
		SCVParams scv_params(&img_params, scv_use_bspl, scv_n_bins, scv_preseed, scv_pou,
			scv_weighted_mapping, scv_mapped_gradient, scv_approx_dist_feat, debug_mode);
		return new MCSCV(&scv_params);
	} else if(!strcmp(am_type, "mcrscv") || !strcmp(am_type, "rscv3")){
		RSCVParams rscv_params(&img_params, scv_use_bspl, scv_n_bins, scv_preseed, scv_pou,
			scv_weighted_mapping, scv_mapped_gradient, scv_approx_dist_feat, debug_mode);
		return new MCRSCV(&rscv_params);
	} else if(!strcmp(am_type, "mcncc") || !strcmp(am_type, "ncc3")){
		NCCParams ncc_params(&img_params, ncc_fast_hess);
		return new MCNCC(&ncc_params);
	} else if(!strcmp(am_type, "mcmi") || !strcmp(am_type, "mi3")){
		MIParams mi_params(&img_params, mi_n_bins, mi_pre_seed, mi_pou,
			getPixMapperObj(pix_mapper, &img_params), debug_mode);
		return new MCMI(&mi_params);
	} else if(!strcmp(am_type, "mcssim") || !strcmp(am_type, "ssim3")){
		SSIMParams ssim_params(&img_params, ssim_k1, ssim_k2);
		return new MCSSIM(&ssim_params);
	} else if(!strcmp(am_type, "mcspss") || !strcmp(am_type, "spss3")){
		SPSSParams spss_params(&img_params, ssim_k1,
			getPixMapperObj(pix_mapper, &img_params));
		return new MCSPSS(&spss_params);
	} else if(!strcmp(am_type, "mcccre") || !strcmp(am_type, "ccre3")){
		CCREParams ccre_params(&img_params, ccre_n_bins, ccre_pou, ccre_pre_seed,
			ccre_symmetrical_grad, ccre_n_blocks, debug_mode);
		return new MCCCRE(&ccre_params);
	}
#ifndef DISABLE_FMAPS
	else if(!strcmp(am_type, "fmaps")){
		FMapsParams fmaps_params(&img_params, fmaps_nfmaps, fmaps_layer_name,
			fmaps_vis, fmaps_zncc, fmaps_model_f_name, fmaps_mean_f_name, fmaps_params_f_name);
		return new FMaps(&fmaps_params);
	}
#endif	
#ifndef DISABLE_PCA
	else if(!strcmp(am_type, "pca")){
		PCAParams pca_params(&img_params, db_root_path, actor,
			source_name, extraction_id, pca_n_feat, pca_len_history);
		return new PCA(&pca_params);
	}
#endif	
	else{
		printf("Invalid appearance model provided: %s\n", am_type);
		return nullptr;
	}
}

inline nt::SearchMethod *getSM(const char *sm_type ,
	const char *am_type, const char *ssm_type, const char *ilm_type){
	AppearanceModel *am = getAM(am_type, ilm_type);
	StateSpaceModel *ssm = getSSM(ssm_type);
	if(!am || !ssm){ return nullptr; }
	if(!strcmp(sm_type, "fclk") || !strcmp(sm_type, "fc")){
		FCLKParams fclk_params(max_iters, epsilon,
			static_cast<FCLKParams::HessType>(fc_hess_type), sec_ord_hess,
			update_templ, fc_chained_warp, fc_leven_marq, fc_lm_delta_init,
			fc_lm_delta_update, fc_write_ssm_updates, fc_show_grid, 
			fc_show_patch, fc_patch_resize_factor, fc_debug_mode);
		return new nt::FCLK(am, ssm, &fclk_params);
	} else  if(!strcmp(sm_type, "esm")){
		ESMParams esm_params(max_iters, epsilon,
			static_cast<ESMParams::JacType>(esm_jac_type),
			static_cast<ESMParams::HessType>(esm_hess_type),
			sec_ord_hess, spi_enable, spi_thresh, debug_mode);
		return new nt::ESM(am, ssm, &esm_params);
	} else if(!strcmp(sm_type, "iclk") || !strcmp(sm_type, "ic")){
		ICLKParams iclk_params(max_iters, epsilon,
			static_cast<ICLKParams::HessType>(ic_hess_type), sec_ord_hess,
			ic_update_ssm, ic_chained_warp, debug_mode);
		return new nt::ICLK(am, ssm, &iclk_params);
	} else if(!strcmp(sm_type, "falk") || !strcmp(sm_type, "fa")){
		FALKParams falk_params(max_iters, epsilon,
			static_cast<FALKParams::HessType>(fa_hess_type), sec_ord_hess,
			fa_show_grid, fa_show_patch, fa_patch_resize_factor,
			fa_write_frames, update_templ, debug_mode);
		return new nt::FALK(am, ssm, &falk_params);
	} else if(!strcmp(sm_type, "ialk") || !strcmp(sm_type, "ia")){
		IALKParams ialk_params(max_iters, epsilon,
			static_cast<IALKParams::HessType>(ia_hess_type), sec_ord_hess,
			debug_mode);
		return new nt::IALK(am, ssm, &ialk_params);
	} else if(!strcmp(sm_type, "pf")){
		return new nt::PF(am, ssm, getPFParams());
	} else if(!strcmp(sm_type, "nn")){
		return new nt::NN(am, ssm, getNNParams());
	} 
#ifndef DISABLE_REGNET
	else if(!strcmp(sm_type, "regnet")){		
		return new nt::RegNet(am, ssm, getRegNetParams());
    }
#endif
    else{
		printf("Invalid NT search method provided: %s\n", sm_type);
		return nullptr;
	}
}
inline const PFParams* getPFParams(){
	vectorvd pf_ssm_sigma, pf_ssm_mean;
	getSamplerParams(pf_ssm_sigma, pf_ssm_mean, pf_ssm_sigma_ids, pf_ssm_mean_ids, "PF");
	return new PFParams(pf_max_iters, pf_n_particles, epsilon,
		static_cast<PFParams::DynamicModel>(pf_dynamic_model),
		static_cast<PFParams::UpdateType>(pf_update_type),
		static_cast<PFParams::LikelihoodFunc>(pf_likelihood_func),
		static_cast<PFParams::ResamplingType>(pf_resampling_type),
		static_cast<PFParams::MeanType>(pf_mean_type),
		pf_reset_to_mean, pf_ssm_sigma, pf_ssm_mean,
		pf_update_sampler_wts, pf_min_particles_ratio,
		pf_pix_sigma, pf_measurement_sigma,
		pf_show_particles, pf_update_template, pf_debug_mode);
}
inline const NNParams* getNNParams(){
	string saved_index_dir = cv::format("log/NN/%s/%s",
		actor.c_str(), source_name.c_str());
	if(!boost::filesystem::exists(saved_index_dir)){
		printf("NN data directory: %s does not exist. Creating it...\n", saved_index_dir.c_str());
		boost::filesystem::create_directories(saved_index_dir);
	}
	vectorvd nn_ssm_sigma, nn_ssm_mean;
	getSamplerParams(nn_ssm_sigma, nn_ssm_mean, nn_ssm_sigma_ids, nn_ssm_mean_ids, "NN");
	NNIndexParams nn_idx_params(nn_gnn_degree, nn_gnn_max_steps,
		nn_gnn_cmpt_dist_thresh, nn_gnn_build_using_flann, nn_gnn_verbose,
		nn_kdt_trees, nn_km_branching, nn_km_iterations,
		static_cast<flann::flann_centers_init_t>(nn_km_centers_init),
		nn_km_cb_index, nn_kdts_leaf_max_size,
		nn_kdtc_leaf_max_size, nn_hc_branching,
		static_cast<flann::flann_centers_init_t>(nn_hc_centers_init),
		nn_hc_trees, nn_hc_leaf_max_size,
		nn_auto_target_precision, nn_auto_build_weight,
		nn_auto_memory_weight, nn_auto_sample_fraction
		);
	return new NNParams(nn_idx_params, nn_max_iters, nn_n_samples, epsilon,
		nn_ssm_sigma, nn_ssm_mean, nn_pix_sigma,
		static_cast<NNParams::SearchType>(nn_search_type),
		static_cast<NNParams::IdxType>(nn_index_type), nn_n_checks,
		nn_additive_update, nn_show_samples, nn_add_points, nn_remove_points,
		nn_load_index, nn_save_index, saved_index_dir,
		nn_corner_sigma_d, nn_corner_sigma_t, nn_ssm_sigma_prec, debug_mode);

}
#ifndef DISABLE_REGNET
inline const RegNetParams* getRegNetParams(){
	string saved_index_dir = cv::format("log/RegNet/%s/%s",
		actor.c_str(), source_name.c_str());
	if(!boost::filesystem::exists(saved_index_dir)){
		printf("RegNet data directory: %s does not exist. Creating it...\n", saved_index_dir.c_str());
		boost::filesystem::create_directories(saved_index_dir);
	}
	vectorvd rg_ssm_sigma, rg_ssm_mean;
	getSamplerParams(rg_ssm_sigma, rg_ssm_mean, rg_ssm_sigma_ids, rg_ssm_mean_ids, "RegNet");
	return new RegNetParams(rg_max_iters, rg_n_samples, epsilon,
		rg_ssm_sigma, rg_ssm_mean, rg_pix_sigma,
		rg_additive_update, rg_show_samples, rg_add_points, rg_remove_points,
		rg_load_index, rg_save_index, saved_index_dir, debug_mode);
}
#endif
inline bool getPF3(vector<TrackerBase*> &trackers,	const char *am_type, 
	const char *ssm_type, const char *ilm_type){
	if(pf3_ssm_sigma_ids.size()<3){
		printf("Insufficient sigma IDs specified for PF3: %d\n", pf3_ssm_sigma_ids.size());
		return false;
	}
	pf_ssm_sigma_ids = pf3_ssm_sigma_ids[0];
	trackers.push_back(getSM("pf", am_type, ssm_type, ilm_type));
	pf_ssm_sigma_ids = pf3_ssm_sigma_ids[1];
	trackers.push_back(getSM("pf", am_type, ssm_type, ilm_type));
	pf_ssm_sigma_ids = pf3_ssm_sigma_ids[2];
	trackers.push_back(getSM("pf", am_type, ssm_type, ilm_type));
	return trackers[0] && trackers[1] && trackers[2];
}
inline bool getNN3(vector<TrackerBase*> &trackers, const char *am_type,
	const char *ssm_type, const char *ilm_type){
	if(nn3_ssm_sigma_ids.size()<3){
		printf("Insufficient sigma IDs specified for PF3: %d\n", nn3_ssm_sigma_ids.size());
		return false;
	}
	nn_ssm_sigma_ids = nn3_ssm_sigma_ids[0];
	trackers.push_back(getSM("nn", am_type, ssm_type, ilm_type));
	nn_ssm_sigma_ids = nn3_ssm_sigma_ids[1];
	trackers.push_back(getSM("nn", am_type, ssm_type, ilm_type));
	nn_ssm_sigma_ids = nn3_ssm_sigma_ids[2];
	trackers.push_back(getSM("nn", am_type, ssm_type, ilm_type));
	return trackers[0] && trackers[1] && trackers[2];
}
inline TrackerBase *getCompositeSM(const char *sm_type,
	const char *am_type, const char *ssm_type, const char *ilm_type){
	//! check composite SMs first to avoid creating unnecessary instances of AM and SSM
	if(!strcmp(sm_type, "pfic")){// PF + ICLK
		vector<TrackerBase*> trackers;
		trackers.push_back(getSM("pf", am_type, ssm_type, ilm_type));
		trackers.push_back(getSM("ic", am_type, ssm_type, ilm_type));
		CascadeParams casc_params(casc_enable_feedback);
		return new CascadeTracker(trackers, &casc_params);
	} else if(!strcmp(sm_type, "pffc")){// PF + FCLK
		vector<TrackerBase*> trackers;
		trackers.push_back(getSM("pf", am_type, ssm_type, ilm_type));
		trackers.push_back(getSM("fc", am_type, ssm_type, ilm_type));
		CascadeParams casc_params(casc_enable_feedback);
		return new CascadeTracker(trackers, &casc_params);
	} else if(!strcmp(sm_type, "pfes")){// PF + FCLK
		vector<TrackerBase*> trackers;
		trackers.push_back(getSM("pf", am_type, ssm_type, ilm_type));
		trackers.push_back(getSM("esm", am_type, ssm_type, ilm_type));
		CascadeParams casc_params(casc_enable_feedback);
		return new CascadeTracker(trackers, &casc_params);
	} else if(!strcmp(sm_type, "pf3")){
		vector<TrackerBase*> trackers;
		if(!getPF3(trackers, am_type, ssm_type, ilm_type)){ return nullptr; }
		CascadeParams casc_params(casc_enable_feedback);
		return new CascadeTracker(trackers, &casc_params);
	} else if(!strcmp(sm_type, "pf3ic")){// PF + ICLK
		vector<TrackerBase*> trackers;
		trackers.resize(2);
		if(!getPF3(trackers, am_type, ssm_type, ilm_type)){ return nullptr; }
		trackers.push_back(getSM("ic", am_type, ssm_type, ilm_type));
		CascadeParams casc_params(casc_enable_feedback);
		return new CascadeTracker(trackers, &casc_params);
	} else if(!strcmp(sm_type, "pf3fc")){// PF + ICLK
		vector<TrackerBase*> trackers;
		if(!getPF3(trackers, am_type, ssm_type, ilm_type)){ return nullptr; }
		trackers.push_back(getSM("fc", am_type, ssm_type, ilm_type));
		CascadeParams casc_params(casc_enable_feedback);
		return new CascadeTracker(trackers, &casc_params);
	} else if(!strcmp(sm_type, "pf3es")){// PF + ICLK
		vector<TrackerBase*> trackers;
		if(!getPF3(trackers, am_type, ssm_type, ilm_type)){ return nullptr; }
		trackers.push_back(getSM("esm", am_type, ssm_type, ilm_type));
		CascadeParams casc_params(casc_enable_feedback);
		return new CascadeTracker(trackers, &casc_params);
	} else if(!strcmp(sm_type, "nnic")){// PF + ICLK
		vector<TrackerBase*> trackers;
		trackers.push_back(getSM("nn", am_type, ssm_type, ilm_type));
		trackers.push_back(getSM("ic", am_type, ssm_type, ilm_type));
		CascadeParams casc_params(casc_enable_feedback);
		return new CascadeTracker(trackers, &casc_params);
	} else if(!strcmp(sm_type, "nnfc")){// PF + ICLK
		vector<TrackerBase*> trackers;
		trackers.push_back(getSM("nn", am_type, ssm_type, ilm_type));
		trackers.push_back(getSM("fc", am_type, ssm_type, ilm_type));
		CascadeParams casc_params(casc_enable_feedback);
		return new CascadeTracker(trackers, &casc_params);
	} else if(!strcmp(sm_type, "nn3")){// 3 layers of NN
		vector<TrackerBase*> trackers;
		if(!getNN3(trackers, am_type, ssm_type, ilm_type)){ return nullptr; }
		CascadeParams casc_params(casc_enable_feedback);
		return new CascadeTracker(trackers, &casc_params);
	} else if(!strcmp(sm_type, "nn3ic")){// NN3 + ICLK
		vector<TrackerBase*> trackers;
		trackers.resize(2);
		if(!getNN3(trackers, am_type, ssm_type, ilm_type)){ return nullptr; }
		trackers.push_back(getSM("ic", am_type, ssm_type, ilm_type));
		CascadeParams casc_params(casc_enable_feedback);
		return new CascadeTracker(trackers, &casc_params);
	} else if(!strcmp(sm_type, "nn3fc")){// NN3 + ICLK
		vector<TrackerBase*> trackers;
		if(!getNN3(trackers, am_type, ssm_type, ilm_type)){ return nullptr; }
		trackers.push_back(getSM("fc", am_type, ssm_type, ilm_type));
		CascadeParams casc_params(casc_enable_feedback);
		return new CascadeTracker(trackers, &casc_params);
	} else if(!strcmp(sm_type, "nn3es")){// NN3 + ESM
		vector<TrackerBase*> trackers;
		if(!getNN3(trackers, am_type, ssm_type, ilm_type)){ return nullptr; }
		trackers.push_back(getSM("esm", am_type, ssm_type, ilm_type));
		CascadeParams casc_params(casc_enable_feedback);
		return new CascadeTracker(trackers, &casc_params);
	} else{
		return nullptr;
	}
}
//! some AMs can be used as pixel mappers by other AMs, e.g. SCV with NCC or SSIM
inline ImageBase *getPixMapperObj(const char *pix_mapper_type, ImgParams *img_params){
	if(!pix_mapper_type)
		return nullptr;
	if(!strcmp(pix_mapper_type, "ssd")){
		return new SSD(img_params);
	} else if(!strcmp(pix_mapper_type, "nssd")){
		NSSD::ParamType *nssd_params = new NSSD::ParamType(img_params, norm_pix_max, norm_pix_min, debug_mode);
		return new NSSD(nssd_params);
	} else if(!strcmp(pix_mapper_type, "zncc")){
		return new ZNCC(img_params);
	} else if(!strcmp(pix_mapper_type, "scv")){
		SCV::ParamType *scv_params = new SCV::ParamType(img_params, scv_use_bspl, scv_n_bins, scv_preseed, scv_pou,
			scv_weighted_mapping, scv_mapped_gradient, scv_approx_dist_feat, debug_mode);
		return new SCV(scv_params);
	} else if(!strcmp(pix_mapper_type, "lscv")){
		LSCV::ParamType *lscv_params = new LSCV::ParamType(img_params, lscv_sub_regions, lscv_sub_regions,
			lscv_spacing, lscv_spacing, scv_affine_mapping, scv_once_per_frame, scv_n_bins, scv_preseed,
			scv_weighted_mapping, lscv_show_subregions, debug_mode);
		return new LSCV(lscv_params);
	} else if(!strcmp(pix_mapper_type, "lrscv") || !strcmp(pix_mapper_type, "lrsc")){
		LRSCV::ParamType *lrscv_params = new LRSCV::ParamType(img_params, lscv_sub_regions, lscv_sub_regions,
			lscv_spacing, lscv_spacing, scv_affine_mapping, scv_once_per_frame, scv_n_bins, scv_preseed,
			scv_weighted_mapping, lscv_show_subregions, debug_mode);
		return new LRSCV(lrscv_params);
	} else if(!strcmp(pix_mapper_type, "rscv")){
		RSCV::ParamType *rscv_params = new RSCV::ParamType(img_params, scv_use_bspl, scv_n_bins, scv_preseed, scv_pou,
			scv_weighted_mapping, scv_mapped_gradient, scv_approx_dist_feat, debug_mode);
		return new RSCV(rscv_params);
	} else if(!strcmp(pix_mapper_type, "mczncc") || !strcmp(pix_mapper_type, "zncc3")){
		return new MCZNCC(img_params);
	} else if(!strcmp(pix_mapper_type, "mcscv") || !strcmp(pix_mapper_type, "scv3")){
		SCV::ParamType *scv_params = new SCV::ParamType(img_params, scv_use_bspl, scv_n_bins, scv_preseed, scv_pou,
			scv_weighted_mapping, scv_mapped_gradient, scv_approx_dist_feat, debug_mode);
		return new MCSCV(scv_params);
	} else if(!strcmp(pix_mapper_type, "mcrscv") || !strcmp(pix_mapper_type, "rscv3")){
		RSCV::ParamType *rscv_params = new RSCV::ParamType(img_params, scv_use_bspl, scv_n_bins, scv_preseed, scv_pou,
			scv_weighted_mapping, scv_mapped_gradient, scv_approx_dist_feat, debug_mode);
		return new MCRSCV(rscv_params);
	} else{
		throw std::invalid_argument("getPixMapperObj::Invalid pixel mapper type provided");
	}
}
// Third Party Trackers
inline TrackerBase *getTrackerObj(const char *tracker_type){
#ifndef DISABLE_THIRD_PARTY_TRACKERS
	// 3rd party trackers
	if(!strcmp(tracker_type, "dsst")){
		DSSTParams *dsst_params = new DSSTParams(dsst_padding, dsst_sigma, dsst_scale_sigma, dsst_lambda,
			dsst_learning_rate, dsst_number_scales, dsst_scale_step,
			dsst_resize_factor, dsst_is_scaling, dsst_bin_size);
		return new DSSTTracker(dsst_params);
	} else if(!strcmp(tracker_type, "kcf")){
		KCFParams *kcf_params = new KCFParams(
			kcf_padding,
			kcf_lambda,
			kcf_output_sigma_factor,
			kcf_interp_factor,
			kcf_kernel_sigma,
			kcf_number_scales,
			kcf_scale_step,
			kcf_scale_model_max_area,
			kcf_scale_sigma_factor,
			kcf_scale_learning_rate,
			kcf_enableScaling,
			kcf_resize_factor
			);
		return new KCFTracker(kcf_params);
	} else if(!strcmp(tracker_type, "cmt")){
		CMTParams * cmt_params = new CMTParams(cmt_estimate_scale, cmt_estimate_rotation,
			cmt_feat_detector, cmt_desc_extractor, cmt_resize_factor);
		return new cmt::CMT(cmt_params);
	} else if(!strcmp(tracker_type, "tld")){
		TLDParams * tld_params = new TLDParams(tld_tracker_enabled, tld_detector_enabled,
			tld_learning_enabled, tld_alternating);
		return new tld::TLD(tld_params);
	} else if(!strcmp(tracker_type, "rct")){
		RCTParams *rct_params = new RCTParams(rct_min_n_rect, rct_max_n_rect, rct_n_feat,
			rct_rad_outer_pos, rct_rad_search_win, rct_learning_rate);
		return new CompressiveTracker(rct_params);
	} else if(!strcmp(tracker_type, "strk")){
		struck::StruckParams strk_params(strk_config_path);
		return new struck::Struck(&strk_params);
	} else if(!strcmp(tracker_type, "mil")){
		MILParams mil_params(
			mil_algorithm,
			mil_num_classifiers,
			mil_overlap,
			mil_search_factor,
			mil_pos_radius_train,
			mil_neg_num_train,
			mil_num_features
			);
		return new MIL(&mil_params);
	} else if(!strcmp(tracker_type, "dft")){
		dft::DFTParams dft_params(dft_res_to_l, dft_p_to_l, dft_max_iter,
			dft_max_iter_single_level, dft_pyramid_smoothing_variance,
			dft_presmoothing_variance, dft_n_control_points_on_edge,
			dft_b_adaptative_choice_of_points, dft_b_normalize_descriptors,
			static_cast<OptimizationType>(dft_optimization_type));
		return new dft::DFT(&dft_params);
	} else if(!strcmp(tracker_type, "frg")){
		FRGParams frg_params(frg_n_bins, frg_search_margin,
			static_cast<FRGParams::HistComparisonMetric>(frg_hist_cmp_metric),
			frg_resize_factor, frg_show_window);
		return new FRG(&frg_params);
	}
#ifndef DISABLE_PFSL3
	else if(!strcmp(tracker_type, "pfsl3")){
		PFSL3Params *pfsl3_params = new PFSL3Params(pfsl3_p_x, pfsl3_p_y,
			pfsl3_state_std, pfsl3_rot, pfsl3_ncc_std, pfsl3_pca_std, pfsl3_ar_p,
			pfsl3_n, pfsl3_n_c, pfsl3_n_iter, pfsl3_sampling, pfsl3_capture,
			pfsl3_mean_check, pfsl3_outlier_flag, pfsl3_len,
			pfsl3_init_size, pfsl3_update_period, pfsl3_ff,
			pfsl3_basis_thr, pfsl3_max_num_basis, pfsl3_max_num_used_basis,
			pfsl3_show_weights, pfsl3_show_templates, pfsl3_debug_mode);
		return new PFSL3(pfsl3_params);
	}
#endif
#ifndef DISABLE_VISP
	else if(!strcmp(tracker_type, "visp")){
		ViSPParams::SMType vp_tracker_type = ViSPParams::SMType::FCLK;
		if(!strcmp(visp_sm, "fclk")){
			vp_tracker_type = ViSPParams::SMType::FCLK;
		} else if(!strcmp(visp_sm, "iclk")){
			vp_tracker_type = ViSPParams::SMType::ICLK;
		} else if(!strcmp(visp_sm, "falk")){
			vp_tracker_type = ViSPParams::SMType::FALK;
		} else if(!strcmp(visp_sm, "esm")){
			vp_tracker_type = ViSPParams::SMType::ESM;
		}
		ViSPParams::AMType vp_am_type = ViSPParams::AMType::SSD;
		if(!strcmp(visp_am, "ssd")){
			vp_am_type = ViSPParams::AMType::SSD;
		} else if(!strcmp(visp_am, "zncc")){
			vp_am_type = ViSPParams::AMType::ZNCC;
		} else if(!strcmp(visp_am, "mi")){
			vp_am_type = ViSPParams::AMType::MI;
		}

		ViSPParams::SSMType vp_stracker_type = ViSPParams::SSMType::Homography;
		if(!strcmp(visp_ssm, "8")){
			vp_stracker_type = ViSPParams::SSMType::Homography;
		} else if(!strcmp(visp_ssm, "l8")){
			vp_stracker_type = ViSPParams::SSMType::SL3;
		} else if(!strcmp(visp_ssm, "6")){
			vp_stracker_type = ViSPParams::SSMType::Affine;
		} else if(!strcmp(visp_ssm, "4")){
			vp_stracker_type = ViSPParams::SSMType::Similarity;
		} else if(!strcmp(visp_ssm, "3")){
			vp_stracker_type = ViSPParams::SSMType::Isometry;
		} else if(!strcmp(visp_ssm, "2")){
			vp_stracker_type = ViSPParams::SSMType::Translation;
		}

		ViSPParams *visp_params = new ViSPParams(
			vp_tracker_type, vp_am_type, vp_stracker_type,
			visp_max_iters, visp_res, visp_res, visp_lambda,
			visp_thresh_grad, visp_pyr_n_levels,
			visp_pyr_level_to_stop
			);
		return new ViSP(visp_params);
	}
#endif
#endif
#ifndef DISABLE_XVISION
	else if(strstr(tracker_type, "xv")){
		using_xv_tracker = true;
		XVParams *xv_params = new XVParams(show_xv_window, steps_per_frame, false, false);
		if(!strcmp(tracker_type, "xv1r")){
			return new XVSSDRotate(xv_params);
		} else if(!strcmp(tracker_type, "xv1s")){
			return new XVSSDScaling(xv_params);
		} else if(!strcmp(tracker_type, "xv2")){
			return new XVSSDTrans(xv_params);
		} else if(!strcmp(tracker_type, "xv3")){
			return new XVSSDRT(xv_params);
		} else if(!strcmp(tracker_type, "xv4")){
			return new XVSSDSE2(xv_params);
		} else if(!strcmp(tracker_type, "xv6")){
			return new XVSSDAffine(xv_params);
		} else if(!strcmp(tracker_type, "xv1p")){
			return new XVSSDPyramidRotate(xv_params, no_of_levels, scale);
		} else if(!strcmp(tracker_type, "xv2p")){
			return new XVSSDPyramidTrans(xv_params, no_of_levels, scale);
		} else if(!strcmp(tracker_type, "xv3p")){
			return new XVSSDPyramidRT(xv_params, no_of_levels, scale);
		} else if(!strcmp(tracker_type, "xv4p")){
			return new XVSSDPyramidSE2(xv_params, no_of_levels, scale);
		} else if(!strcmp(tracker_type, "xv6p")){
			return new XVSSDPyramidAffine(xv_params, no_of_levels, scale);
		} else if(!strcmp(tracker_type, "xve")){
			return new XVEdgeTracker(xv_params);
		} else if(!strcmp(tracker_type, "xvc")){
			return new XVColor(xv_params);
		} else if(!strcmp(tracker_type, "xvg")){
			return new XVSSDGrid(xv_params,
				xv_tracker_type, xvg_grid_size_x, xvg_grid_size_y, patch_size,
				xvg_reset_pos, reset_template, xvg_sel_reset_thresh, xvg_reset_wts,
				xvg_adjust_lines, xvg_update_wts, debug_mode);
		} else if(!strcmp(tracker_type, "xvgl")){
			return new XVSSDGridLine(xv_params,
				xv_tracker_type, xvg_grid_size_x, xvg_grid_size_y,
				patch_size, xvg_use_constant_slope, xvg_use_ls, xvg_inter_alpha_thresh,
				xvg_intra_alpha_thresh, xvg_reset_pos, reset_template, debug_mode);
		} else{
			stringstream err_msg;
			err_msg << "Invalid Xvision tracker type provided : " << tracker_type << "\n";
			throw std::invalid_argument(err_msg.str());
		}
	}
#endif
	return nullptr;
}
//template< class AMType, class SSMType >
//TrackerBase *getFESMObj(
//	typename AMType::ParamType *am_params = nullptr,
//	typename SSMType::ParamType *ssm_params = nullptr){
//	FESMParams *esm_params = new FESMParams(max_iters, epsilon,
//		sec_ord_hess, spi_enable, spi_thresh, debug_mode);
//	switch(static_cast<FESMParams::JacType>(jac_type)){
//	case FESMParams::JacType::Original:
//		switch(static_cast<FESMParams::HessType>(hess_type)){
//		case FESMParams::HessType::Original:
//			return new FESM < AMType, SSMType,
//				FESMParams::HessType::Original,
//				FESMParams::JacType::Original >(esm_params, am_params, ssm_params);
//		case FESMParams::HessType::SumOfStd:
//			return new FESM < AMType, SSMType,
//				FESMParams::HessType::SumOfStd,
//				FESMParams::JacType::Original >(esm_params, am_params, ssm_params);
//		case FESMParams::HessType::SumOfSelf:
//			return new FESM < AMType, SSMType,
//				FESMParams::HessType::SumOfSelf,
//				FESMParams::JacType::Original >(esm_params, am_params, ssm_params);
//		case FESMParams::HessType::InitialSelf:
//			return new FESM < AMType, SSMType,
//				FESMParams::HessType::InitialSelf,
//				FESMParams::JacType::Original >(esm_params, am_params, ssm_params);
//		case FESMParams::HessType::CurrentSelf:
//			return new FESM < AMType, SSMType,
//				FESMParams::HessType::CurrentSelf,
//				FESMParams::JacType::Original >(esm_params, am_params, ssm_params);
//		case FESMParams::HessType::Std:
//			return new FESM < AMType, SSMType,
//				FESMParams::HessType::CurrentSelf,
//				FESMParams::JacType::Original >(esm_params, am_params, ssm_params);
//		default:
//			throw std::invalid_argument("Invalid FESM Hessian type provided");
//		}
//	case FESMParams::JacType::DiffOfJacs:
//		switch(static_cast<FESMParams::HessType>(hess_type)){
//		case FESMParams::HessType::Original:
//			return new FESM < AMType, SSMType,
//				FESMParams::HessType::Original,
//				FESMParams::JacType::DiffOfJacs >(esm_params, am_params, ssm_params);
//		case FESMParams::HessType::SumOfStd:
//			return new FESM < AMType, SSMType,
//				FESMParams::HessType::SumOfStd,
//				FESMParams::JacType::DiffOfJacs >(esm_params, am_params, ssm_params);
//		case FESMParams::HessType::SumOfSelf:
//			return new FESM < AMType, SSMType,
//				FESMParams::HessType::SumOfSelf,
//				FESMParams::JacType::DiffOfJacs >(esm_params, am_params, ssm_params);
//		case FESMParams::HessType::InitialSelf:
//			return new FESM < AMType, SSMType,
//				FESMParams::HessType::InitialSelf,
//				FESMParams::JacType::DiffOfJacs >(esm_params, am_params, ssm_params);
//		case FESMParams::HessType::CurrentSelf:
//			return new FESM < AMType, SSMType,
//				FESMParams::HessType::CurrentSelf,
//				FESMParams::JacType::DiffOfJacs >(esm_params, am_params, ssm_params);
//		case FESMParams::HessType::Std:
//			return new FESM < AMType, SSMType,
//				FESMParams::HessType::CurrentSelf,
//				FESMParams::JacType::DiffOfJacs >(esm_params, am_params, ssm_params);
//		default:
//			throw std::invalid_argument("Invalid FESM Hessian type provided");
//		}
//	default:
//		throw std::invalid_argument("Invalid FESM Jacobian type provided");
//	}
//}

_MTF_END_NAMESPACE

#endif
