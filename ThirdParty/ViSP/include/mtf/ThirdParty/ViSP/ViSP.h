#ifndef MTF_VISP_H
#define MTF_VISP_H

#include "mtf/TrackerBase.h"
#include "opencv2/imgproc/imgproc.hpp"

#include <visp3/core/vpImage.h>
#include <visp3/tt/vpTemplateTrackerWarp.h>
#include <visp3/tt/vpTemplateTracker.h>

#define VISP_SM 0
#define VISP_AM 0
#define VISP_SSM 0
#define  VISP_MAX_ITERS 30
#define  VISP_SAMPLING_RES 50
#define  VISP_LAMBDA 0.001
#define  VISP_THRESH_GRAD 60
#define  VISP_PYR_N_LEVELS 0
#define  VISP_PYR_LEVEL_TO_STOP 1

struct ViSPParams{
	enum class SSMType{
		Homography, HomographySL3, Affine,
		Similarity, Isometry, Translation
	};
	enum class SMType{
		FCLK, ICLK, FALK, ESM
	};
	enum class AMType{
		SSD, ZNCC, MI
	};

	SMType sm_type;
	AMType am_type;
	SSMType ssm_type;

	int max_iters;
	int resx, resy;
	double lambda;
	double thresh_grad;
	int pyr_n_levels;
	int pyr_level_to_stop;

	ViSPParams(SMType _sm_type, AMType _am_type,
		SSMType _ssm_type,
		int _max_iters,
		int _resx,
		int _resy,
		double _lambda,
		double _thresh_grad,
		int _pyr_n_levels,
		int _pyr_level_to_stop);

	ViSPParams(const ViSPParams *params = nullptr);
};

class ViSP : public mtf::TrackerBase{
public:
	typedef ViSPParams ParamType;
	typedef ParamType::SMType SMType;
	typedef ParamType::AMType AMType;
	typedef ParamType::SSMType SSMType;

	ViSP(const ParamType *visp_params);
	virtual ~ViSP(){
		if(warp){ delete(tracker); }
		if(tracker){ delete(tracker); }
	}
	void initialize(const cv::Mat &corners) override;
	void update() override;
	const cv::Mat& getRegion() override{ return cv_corners_mat; }
	void setRegion(const cv::Mat& corners) override;
	void setImage(const cv::Mat &img) override;
	int inputType() const override{ return CV_8UC1; }
	void setOptimalSamplingRatio(const cv::Mat &corners);
	void updateCorners();

protected:
	const ParamType params;
	cv::Mat curr_img_uchar;
	vpImage<unsigned char> curr_img;
	vpTemplateTrackerWarp *warp;
	vpTemplateTracker *tracker;
};
#endif
