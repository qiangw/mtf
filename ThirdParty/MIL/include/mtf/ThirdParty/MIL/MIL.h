#ifndef MTF_MIL_H
#define MTF_MIL_H

#include "object_tracker.h"
#include "mtf/TrackerBase.h"

#define MIL_ALGORITHM cv::ObjectTrackerParams::CV_ONLINEMIL
#define MIL_NUM_CLASSIFIERS 100
#define MIL_OVERLAP 0.99f
#define MIL_SEARCH_FACTOR 2.0f
#define MIL_POS_RADIUS_TRAIN 4.0f
#define MIL_NEG_NUM_TRAIN 65
#define MIL_NUM_FEATURES 250

struct MILParams{
	int algorithm; // CV_ONLINEBOOSTING, CV_SEMIONLINEBOOSTING, CV_ONLINEMIL, CV_LINEMOD
	int num_classifiers; // the number of classifiers to use in a given boosting algorithm (OnlineBoosting, MIL)
	float overlap; // search region parameters to use in a given boosting algorithm (OnlineBoosting, MIL)
	float search_factor; // search region parameters to use in a given boosting algorithm (OnlineBoosting, MIL)

	// The following are specific to the MIL algorithm
	float pos_radius_train; // radius for gathering positive instances
	int neg_num_train; // # negative samples to use during training
	int num_features;
	MILParams(int _algorithm, int _num_classifiers, float _overlap, float _search_factor,
		float _pos_radius_train, int _neg_num_train, int _num_features);
	MILParams(const MILParams *params = nullptr);
};


class MIL : public mtf::TrackerBase{
public:
	typedef MILParams ParamType;
	const ParamType params;
	cv::ObjectTracker *tracker;
	cv::ObjectTrackerParams tracker_params;
	cv::Mat curr_img;
	cv::Rect curr_location;
	CvRect init_bb;

	MIL();
	MIL(const ParamType *mil_params = nullptr);
	~MIL(){}
	void setImage(const cv::Mat &img) override{ curr_img = img; }
	int inputType() const override{ return CV_8UC1; }
	void initialize(const cv::Mat& corners) override;
	void update() override;
	void updateCVCorners();
};

#endif 
