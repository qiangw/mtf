#include "mtf/ThirdParty/MIL/MIL.h"
#include "mtf/Utilities/miscUtils.h"

MILParams::MILParams(int _algorithm, int _num_classifiers,
	float _overlap, float _search_factor,
	float _pos_radius_train, int _neg_num_train,
	int _num_features) :
	algorithm(_algorithm),
	num_classifiers(_num_classifiers),
	overlap(_overlap),
	search_factor(_search_factor),
	pos_radius_train(_pos_radius_train),
	neg_num_train(_neg_num_train),
	num_features(_num_features){}
MILParams::MILParams(const MILParams *params) :
algorithm(MIL_ALGORITHM),
num_classifiers(MIL_NUM_CLASSIFIERS),
overlap(MIL_OVERLAP),
search_factor(MIL_SEARCH_FACTOR),
pos_radius_train(MIL_POS_RADIUS_TRAIN),
neg_num_train(MIL_NEG_NUM_TRAIN),
num_features(MIL_NUM_FEATURES){
	if(params){
		algorithm = params->algorithm;
		num_classifiers = params->num_classifiers;
		overlap = params->overlap;
		search_factor = params->search_factor;
		pos_radius_train = params->pos_radius_train;
		neg_num_train = params->neg_num_train;
		num_features = params->num_features;
	}
}
MIL::MIL() :
tracker(nullptr){
	name = "mil";
	cv_corners_mat.create(2, 4, CV_64FC1);
	tracker = new cv::ObjectTracker(tracker_params);
}

MIL::MIL(const ParamType *mil_params) :
params(mil_params),
tracker_params(params.algorithm, params.num_classifiers,
params.overlap, params.search_factor, params.pos_radius_train,
params.neg_num_train, params.num_features),
tracker(nullptr){
	name = "mil";
	printf("Using MIL tracker with:\n");
	printf("algorithm: %d\n", tracker_params.algorithm_);
	printf("num_classifiers: %d\n", tracker_params.num_classifiers_);
	printf("overlap: %f\n", tracker_params.overlap_);
	printf("search_factor: %f\n", tracker_params.search_factor_);
	printf("pos_radius_train: %f\n", tracker_params.pos_radius_train_);
	printf("neg_num_train: %d\n", tracker_params.neg_num_train_);
	printf("num_features: %d\n", tracker_params.num_features_);
	printf("\n");
	cv_corners_mat.create(2, 4, CV_64FC1);
	tracker = new cv::ObjectTracker(tracker_params);
}
void MIL::initialize(const cv::Mat& corners){
	curr_location = mtf::utils::getBestFitRectangle<int>(corners,
		curr_img.cols, curr_img.rows);
	printf("best_fit_rect: x: %d y: %d width: %d height: %d\n",
		curr_location.x, curr_location.y, curr_location.width, curr_location.height);
	init_bb = cvRect(curr_location.x, curr_location.y, curr_location.width, curr_location.height);
	printf("Using MIL tracker at: %d, %d with region of size %dx%d\n",
		curr_location.x, curr_location.y, curr_location.width, curr_location.height);
	if(!tracker->initialize(curr_img, curr_location)){
		throw std::runtime_error("MIL::Tracker could not be initialized successfully\n");
	}
	updateCVCorners();
}
void MIL::update(){
	if(!tracker->update(curr_img, curr_location)){
		throw std::runtime_error("MIL::Tracker could not be updated successfully\n");
	}
	updateCVCorners();
}
void MIL::updateCVCorners(){
	cv_corners_mat.at<double>(0, 0) = cv_corners_mat.at<double>(0, 3) = curr_location.x;
	cv_corners_mat.at<double>(1, 0) = cv_corners_mat.at<double>(1, 1) = curr_location.y;
	cv_corners_mat.at<double>(0, 1) = cv_corners_mat.at<double>(0, 2) = curr_location.x + curr_location.width;
	cv_corners_mat.at<double>(1, 2) = cv_corners_mat.at<double>(1, 3) = curr_location.y + curr_location.height;
}

